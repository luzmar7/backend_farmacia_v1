from rest_framework import serializers

from ....pedir.models import (
  Unidad,
  Cargo,
  Personal,
  Pedido_U,
  producto_pedido,
  Farmaceutica,
  Pedido_E,
  producto_pedidoE,
  Personal_PROMES,
  T_Especialidad,
)

from ....almacena.api.v1.serializers import AlmacenSerializer, ProductoSerializer
from ....Usuarios_Login.serializers import UserSerializer
###################### ESPECIALIDAD ##############################
class EspecialidadSerializer(serializers.ModelSerializer):
  class Meta:
    model = T_Especialidad
    fields = '__all__'

###################### UNIDAD ##############################
class UnidadSerializer(serializers.ModelSerializer):
  class Meta:
    model = Unidad
    fields = '__all__'

###################### CARGO ##############################
class CargoSerializer(serializers.ModelSerializer):
  class Meta:
    model = Cargo
    fields = '__all__'

###################### PERSONAL ##############################
class PersonalSerializer(serializers.ModelSerializer):
  unidad = UnidadSerializer(read_only=True)
  cargo = CargoSerializer(read_only=True)
  class Meta:
    model = Personal
    fields = '__all__'

class PersonalSerializer2(serializers.ModelSerializer):
  class Meta:
    model = Personal
    fields = (
      'nro_item',
      'nombre',
      'apellidos',
      'ci',
    ) 



class PersonalSerializer2(serializers.ModelSerializer):
  class Meta:
    model = Personal
    fields = (
      'nro_item',
      'nombre',
      'apellidos',
      'ci',
    )

###################### FARMACEUTICA ##############################
class FarmaceuticaSerializer(serializers.ModelSerializer):
  personal = PersonalSerializer2(read_only=True)
  class Meta:
    model = Farmaceutica
    fields = '__all__' 

############################### PERSONAL PROMES #########################
#----------------------------- Reportes Pedidos -------------------
class Personal_PROMESSerializer(serializers.ModelSerializer):
  usuario=UserSerializer(read_only=True)
  unidad = UnidadSerializer(read_only=True)
  especialidad = EspecialidadSerializer(read_only=True)
  class Meta:
    model = Personal
    fields = '__all__'
    
###################### PEDIDO UNIDAD ##############################
class PedidoUnidadSerializer(serializers.ModelSerializer):
  class Meta:
    model = Pedido_U
    fields = '__all__'


class PedidoUnidadSerializer2(serializers.ModelSerializer):
  localizacion = AlmacenSerializer(read_only=True)
  unidad = UnidadSerializer(read_only=True)
  personal = Personal_PROMESSerializer(read_only=True)
  farmaceutica = Personal_PROMESSerializer(read_only=True)
  class Meta:
    model = Pedido_U
    fields = '__all__'

class PedidoEstadoSerializer(serializers.ModelSerializer):
  class Meta:
    model = Pedido_U
    fields = (
      'estado',
      'farmaceutica',
      'fecha_entrega',
      'hora_entrega',
    )

###################### PRODUCTO PEDIDO ##############################
class ProductoPedidoSerializer(serializers.ModelSerializer):
  class Meta:
    model = producto_pedido
    fields = '__all__'

class ProductoPedidoSerializer2(serializers.ModelSerializer):
  codigo = ProductoSerializer(read_only=True)

  class Meta:
    model = producto_pedido
    fields = '__all__'

class ProductoPedidoSerializer3(serializers.ModelSerializer):
  class Meta:
    model = producto_pedido
    fields = (
      'cantidad_entregada',
      'lote_salida',
      'fecha_expiracion',
    ) 

class ProductoPedidoSerializer4(serializers.ModelSerializer):
  class Meta:
    model = producto_pedido
    fields = (
      'lote_salida',
    )

class ProductoPedidoSerializerA(serializers.ModelSerializer):
  codigo = ProductoSerializer(read_only=True)
  id_pedido = PedidoUnidadSerializer(read_only=True)
  class Meta:
    model = producto_pedido
    fields = '__all__'
###################### PEDIDO SSU ##############################
class PedidoExternoSerializer(serializers.ModelSerializer):
  class Meta:
    model = Pedido_E
    fields = '__all__'


class PedidoExternoSerializer2(serializers.ModelSerializer):
  localizacion = AlmacenSerializer(read_only=True)
  personal = Personal_PROMESSerializer(read_only=True)
  class Meta:
    model = Pedido_E
    fields = '__all__'

class PedidoExternoEstadoSerializer(serializers.ModelSerializer):
  class Meta:
    model = Pedido_E
    fields = (
      'estado',
      'personal_externo',
      'fecha_recibida',
      'hora_recibida',
    ) 


###################### PRODUCTO PEDIDO SSU (EXTERNO) ##############################
class ProductoPedidoESerializer(serializers.ModelSerializer):
  class Meta:
    model = producto_pedidoE
    fields = '__all__'

class ProductoPedidoESerializer2(serializers.ModelSerializer):
  codigo = ProductoSerializer(read_only=True)
  class Meta:
    model = producto_pedidoE
    fields = '__all__'

class ProductoPedidoESerializer3(serializers.ModelSerializer):
  class Meta:
    model = producto_pedidoE
    fields = (
      'cantidad_recibida',
      'lote_entrada',
      'fecha_expiracion',
      'cantidad_farmacia',
      'cantidad_almacen',
    ) 

class ProductoPedidoSerializer4(serializers.ModelSerializer):
  class Meta:
    model = producto_pedidoE
    fields = (
      'lote_entrada',
    )

class ProductoPedidoESerializerA(serializers.ModelSerializer):
  codigo = ProductoSerializer(read_only=True)
  id_pedido = PedidoExternoSerializer(read_only=True)
  class Meta:
    model = producto_pedidoE
    fields = '__all__'
    
############################### REPORTES #########################
#----------------------------- Reportes Pedidos -------------------
class PedidoMesSumSerializer(serializers.ModelSerializer):
  sum = serializers.IntegerField()
  class Meta:
    model = producto_pedido
    fields = ('codigo_id','sum')



# #----------------------------- Reportes Medicamentos -------------------
# class RecetaMedicamentoMesSumSerializer(serializers.ModelSerializer):
#   sum = serializers.IntegerField()
#   class Meta:
#     model = medicamento_receta
#     fields = ('codigo_id','sum')
