from django.urls import path
from .views import (
  ListaUnidades,
  ListaCargos,
  ListaPersonal,
  PersonalBusca,
  PedidoGuardar,
  ProductoPedidoGuardar,
  ListaPedido,
  ProductoPedidoBusca,
  PedidosPorEntregar,
  PedidosPorEntregarF,

  PedidoEstadoEdita,
  PedidosEntregadosF,
  ProductoPedidoCantidadEdita,
  PedidosPorEntregarA,

  PedidosEntregadosA,
  ListaPedidoFA,
  PedidosPorEntregarAU,
  PedidosPorEntregarAF,
  ProductoPedidoEdita,

  PedidoExternoGuardar,
  ProductoPedidoEGuardar,
  ListaPedidoSSU,

  ProductoPedidoSSUBusca,
  PedidoSSUPorRecibir,
  ProductoPedidoECantidadEdita,
  PedidoSSUEstadoEdita,
  PedidoSSURecibido,
  BindCardPedido,
  BindCardPedidoBusca,
  BindCardPedidoEBusca,
  ListaPedidoFecha,
  ListaPedidoCodigo,

  ListaPedidoPorEntregarF,
  PedidosPorEntregarFechaF,
  PedidosPorEntregarCodigoF,

  PedidosEntregadosFechaF,
  ListaPedidoEntregadosF,
  PedidosEntregadosCodigoF,

  PedidosEmitidosFechaA,
  PedidosEmitidosCodigoA,
  ListaPedidoEmitidosA,

  ListaPedidoExterno,
  ListaPedidoFechaSSU,
  ListaPedidoCodigoSSU,

  PedidoSSUPorRecibirFecha,
  ListaPedidoSSUPorRecibir,
  PedidoSSUPorRecibirCodigo,
  PedidoSSURecibidoFecha,
  ListaPedidoSSURecibido,
  PedidoSSURecibidoCodigo,
  PedidosPorEntregarFechaAU,
  PedidosPorEntregarCodigoAU,
  ListaPedidoPorEntregarAU,

  ListaPedidoEntregadoAU,
  PedidosEntregadoFechaAU,

  PedidosEntregadoCodigoAU,
  PedidosEntregadoAF,

  PedidosPorEntregarFechaAF,
  ListaPedidosPorEntregarAF,
  PedidosPorEntregarCodigoAF,
  PedidosEntregadoFechaAF,
  ListaPedidosEntregadoAF,
  PedidosEntregadoCodigoAF,

  BindCardPedidoBuscaFecha,
  BindCardPedidoEBuscaFecha,

  PedidoMesSum,
  PedidoTrimSum,
  PedidoTrimSumTotalMedicamento,

  PedidoSemSum,
  PedidoSemSumTotalMedicamento,

  PedidoAnualSum,
  PedidoAnualSumTotalMedicamento,
  PedidoTrimSumTotalInsumo,
  PedidoSemSumTotalInsumo,
  PedidoAnualSumTotalInsumo,
  PedidoTrimSumTotalReactivo,
  PedidoSemSumTotalReactivo,
  PedidoAnualSumTotalReactivo,

  PedidoTrimSumTotal,
  PedidoSemSumTotal,
  PedidoAnualSumTotal,
  ProductoPedidoBuscaNoEntregado,
  ListaPersonalusuario,

  PedidoUnidad,
  ListaPedidoUnidadFecha,
  ListaPedidoUnidad,
  ProductoPedidoSSUBusca2,
  ProductoPedidoSSUBusca3,
)

app_name = 'pedir'
urlpatterns = [
  #UNIDADES
  path('ListaUnidades/', ListaUnidades.as_view(), name="ListaUnidades"),

  #CARGOS
  path('ListaCargos/', ListaCargos.as_view(), name="ListaCargos"),
  
  #PERSONAL
  path('ListaPersonal/', ListaPersonal.as_view(), name="ListaPersonal"),
  path('PersonalBusca/<int:buscaU>/', PersonalBusca.as_view(), name="PersonalBusca"),

  #PEDIDO UNIDAD (Enfermeria, Laboratorio, Odontologia)
  path('PedidoGuardar/', PedidoGuardar.as_view(), name="PedidoGuardar"), 
  path('ListaPedido/', ListaPedido.as_view(), name="ListaPedido"),
  path('ListaPedidoFecha/<str:busca>/', ListaPedidoFecha.as_view(), name="ListaPedidoFecha"),
  path('ListaPedidoCodigo/<int:busca>/', ListaPedidoCodigo.as_view(), name="ListaPedidoCodigo"),
  path('PedidoEstadoEdita/<int:pk>/', PedidoEstadoEdita.as_view(), name="PedidoEstadoEdita"),
  
  #----PEDIDOS POR UNIDAD (ENFERMERIA, LABORATORIO, ODONTOLOGIA)-------

  path('PedidoUnidad/<int:unidad>/', PedidoUnidad.as_view(), name="PedidoUnidad"),
  path('ListaPedidoUnidadFecha/<int:unidad>/<str:busca>/', ListaPedidoUnidadFecha.as_view(), name="ListaPedidoUnidadFecha"),
  path('ListaPedidoUnidad/<int:unidad>/', ListaPedidoUnidad.as_view(), name="ListaPedidoUnidad"),
  
  #PEDIDOS FARMACIA 
  path('ListaPedidoPorEntregarF/', ListaPedidoPorEntregarF.as_view(), name="ListaPedidoPorEntregarF"),
  path('PedidosPorEntregar/', PedidosPorEntregar.as_view(), name="PedidosPorEntregar"),
  path('PedidosPorEntregarF/', PedidosPorEntregarF.as_view(), name="PedidosPorEntregarF"),

  path('PedidosPorEntregarFechaF/<str:busca>/', PedidosPorEntregarFechaF.as_view(), name="PedidosPorEntregarFechaF"),
  path('PedidosPorEntregarCodigoF/<str:busca>/', PedidosPorEntregarCodigoF.as_view(), name="PedidosPorEntregarCodigoF"),

  path('ListaPedidoEntregadosF/', ListaPedidoEntregadosF.as_view(), name="ListaPedidoEntregadosF"),
  path('PedidosEntregadosF/', PedidosEntregadosF.as_view(), name="PedidosEntregadosF"),
  path('PedidosEntregadosFechaF/<str:busca>/',  PedidosEntregadosFechaF.as_view(), name="PedidosEntregadosFechaF"),
  path('PedidosEntregadosCodigoF/<str:busca>/', PedidosEntregadosCodigoF.as_view(), name="PedidosEntregadosCodigoF"),

  path('ListaPedidoEmitidosA/', ListaPedidoEmitidosA.as_view(), name="ListaPedidoEmitidosA"),
  path('ListaPedidoFA/', ListaPedidoFA.as_view(), name="ListaPedidoFA"),
  path('PedidosEmitidosFechaA/<str:busca>/',  PedidosEmitidosFechaA.as_view(), name="PedidosEmitidosFechaA"),
  path('PedidosEmitidosCodigoA/<str:busca>/', PedidosEmitidosCodigoA.as_view(), name="PedidosEmitidosCodigoA"),

  #PEDIDOS ALMACEN
  path('PedidosPorEntregarA/', PedidosPorEntregarA.as_view(), name="PedidosPorEntregarA"),

  path('ListaPedidoPorEntregarAU/', ListaPedidoPorEntregarAU.as_view(), name="ListaPedidoPorEntregarAU"),
  path('PedidosPorEntregarAU/', PedidosPorEntregarAU.as_view(), name="PedidosPorEntregarAU"),
  path('PedidosPorEntregarFechaAU/<str:busca>/', PedidosPorEntregarFechaAU.as_view(), name="PedidosPorEntregarFechaAU"),
  path('PedidosPorEntregarCodigoAU/<int:busca>/', PedidosPorEntregarCodigoAU.as_view(), name="PedidosPorEntregarCodigoAU"),



  path('PedidosEntregadosA/', PedidosEntregadosA.as_view(), name="PedidosEntregadosA"),
  path('ListaPedidoEntregadoAU/', ListaPedidoEntregadoAU.as_view(), name="ListaPedidoEntregadoAU"),
  path('PedidosEntregadoFechaAU/<str:busca>/', PedidosEntregadoFechaAU.as_view(), name="PedidosEntregadoFechaAU"),

  path('PedidosEntregadoCodigoAU/<int:busca>/', PedidosEntregadoCodigoAU.as_view(), name="PedidosEntregadoCodigoAU"),


  path('ListaPedidosPorEntregarAF/', ListaPedidosPorEntregarAF.as_view(), name="ListaPedidosPorEntregarAF"),
  path('PedidosPorEntregarAF/', PedidosPorEntregarAF.as_view(), name="PedidosPorEntregarAF"),
  path('PedidosPorEntregarFechaAF/<str:busca>/', PedidosPorEntregarFechaAF.as_view(), name="PedidosPorEntregarFechaAF"),

  path('PedidosPorEntregarCodigoAF/<int:busca>/', PedidosPorEntregarCodigoAF.as_view(), name="PedidosPorEntregarCodigoAF"),

  path('ListaPedidosEntregadoAF/', ListaPedidosEntregadoAF.as_view(), name="ListaPedidosEntregadoAF"),
  path('PedidosEntregadoAF/', PedidosEntregadoAF.as_view(), name="PedidosEntregadoAF"),
  path('PedidosEntregadoFechaAF/<str:busca>/', PedidosEntregadoFechaAF.as_view(), name="PedidosEntregadoFechaAF"),
  path('PedidosEntregadoCodigoAF/<int:busca>/', PedidosEntregadoCodigoAF.as_view(), name="PedidosEntregadoCodigoAF"),

  #PRODUCTO-PEDIDO 
  path('ProductoPedidoGuardar/', ProductoPedidoGuardar.as_view(), name="ProductoPedidoGuardar"),
  path('ProductoPedidoBusca/<int:busca>/', ProductoPedidoBusca.as_view(), name="ProductoPedidoBusca"),
  path('ProductoPedidoBuscaNoEntregado/<int:busca>/', ProductoPedidoBuscaNoEntregado.as_view(), name="ProductoPedidoBuscaNoEntregado"),

  path('ProductoPedidoCantidadEdita/<int:pk>/', ProductoPedidoCantidadEdita.as_view(), name="ProductoPedidoCantidadEdita"),

  path('ProductoPedidoEdita/<int:pk>/', ProductoPedidoEdita.as_view(), name="ProductoPedidoEdita"),

  #PEDIDO SSU (EXTERNO)
  path('PedidoExternoGuardar/', PedidoExternoGuardar.as_view(), name="PedidoExternoGuardar"),
  
  path('ListaPedidoExterno/', ListaPedidoExterno.as_view(), name="ListaPedidoExterno"),
  path('ListaPedidoSSU/', ListaPedidoSSU.as_view(), name="ListaPedidoSSU"),
  path('ListaPedidoFechaSSU/<str:busca>/', ListaPedidoFechaSSU.as_view(), name="ListaPedidoFechaSSU"),
  path('ListaPedidoCodigoSSU/<int:busca>/', ListaPedidoCodigoSSU.as_view(), name="ListaPedidoCodigoSSU"),

  path('ListaPedidoSSUPorRecibir/', ListaPedidoSSUPorRecibir.as_view(), name="ListaPedidoSSUPorRecibir"),
  path('PedidoSSUPorRecibir/', PedidoSSUPorRecibir.as_view(), name="PedidoSSUPorRecibir"),
  path('PedidoSSUPorRecibirFecha/<str:busca>/', PedidoSSUPorRecibirFecha.as_view(), name="PedidoSSUPorRecibirFecha"),
 
  path('PedidoSSUPorRecibirCodigo/<int:busca>/', PedidoSSUPorRecibirCodigo.as_view(), name="PedidoSSUPorRecibirCodigo"),

  path('ListaPedidoSSURecibido/', ListaPedidoSSURecibido.as_view(), name="ListaPedidoSSURecibido"),
  path('PedidoSSURecibido/', PedidoSSURecibido.as_view(), name="PedidoSSURecibido"),
  path('PedidoSSURecibidoFecha/<str:busca>/', PedidoSSURecibidoFecha.as_view(), name="PedidoSSURecibidoFecha"),
  path('PedidoSSURecibidoCodigo/<int:busca>/', PedidoSSURecibidoCodigo.as_view(), name="PedidoSSURecibidoCodigo"),

  path('PedidoSSUEstadoEdita/<int:pk>/', PedidoSSUEstadoEdita.as_view(), name="PedidoSSUEstadoEdita"),

  #PRODUCTO - PEDIDO SSU (EXTERNO)
  path('ProductoPedidoEGuardar/', ProductoPedidoEGuardar.as_view(), name="ProductoPedidoEGuardar"),
  path('ProductoPedidoSSUBusca/<int:busca>/', ProductoPedidoSSUBusca.as_view(), name="ProductoPedidoSSUBusca"),
  path('ProductoPedidoSSUBusca2/<int:busca>/', ProductoPedidoSSUBusca2.as_view(), name="ProductoPedidoSSUBusca2"),
  
  path('ProductoPedidoSSUBusca3/<int:busca>/', ProductoPedidoSSUBusca3.as_view(), name="ProductoPedidoSSUBusca3"),

  path('ProductoPedidoECantidadEdita/<int:pk>/', ProductoPedidoECantidadEdita.as_view(), name="ProductoPedidoECantidadEdita"),

################### BIND CARD #######################
  path('BindCardPedido/',BindCardPedido.as_view(), name="BindCardPedido"),
  path('BindCardPedidoBusca/<str:busca>/',BindCardPedidoBusca.as_view(), name="BindCardPedidoBusca"),
  path('BindCardPedidoBuscaFecha/<str:codigo>/<str:fecha>/',BindCardPedidoBuscaFecha.as_view(), name="BindCardPedidoBuscaFecha"),

  path('BindCardPedidoEBusca/<str:busca>/',BindCardPedidoEBusca.as_view(), name="BindCardPedidoEBusca"),
  path('BindCardPedidoEBuscaFecha/<str:codigo>/<str:fecha>/',BindCardPedidoEBuscaFecha.as_view(), name="BindCardPedidoEBuscaFecha"),

##################### REPORTES ######################
  path('PedidoMesSum/<str:mes>/<str:anio>/<str:codigo>/',PedidoMesSum.as_view(), name="PedidoMesSum"),
  path('PedidoTrimSum/<str:mes1>/<str:mes2>/<str:mes3>/<str:anio>/<str:codigo>/',PedidoTrimSum.as_view(), name="PedidoTrimSum"),
  path('PedidoTrimSumTotalMedicamento/<str:mes1>/<str:mes2>/<str:mes3>/<str:anio>/', PedidoTrimSumTotalMedicamento.as_view(), name="PedidoTrimSumTotalMedicamento"),

  path('PedidoSemSum/<str:mes1>/<str:mes2>/<str:mes3>/<str:mes4>/<str:mes5>/<str:mes6>/<str:anio>/<str:codigo>/', PedidoSemSum.as_view(), name="PedidoSemSum"),
  path('PedidoSemSumTotalMedicamento/<str:mes1>/<str:mes2>/<str:mes3>/<str:mes4>/<str:mes5>/<str:mes6>/<str:anio>/', PedidoSemSumTotalMedicamento.as_view(), name="PedidoSemSumTotalMedicamento"),

  path('PedidoAnualSum/<str:anio>/<str:codigo>/', PedidoAnualSum.as_view(), name="PedidoAnualSum"),
  path('PedidoAnualSumTotalMedicamento/<str:anio>/', PedidoAnualSumTotalMedicamento.as_view(), name="PedidoAnualSumTotalMedicamento"),

  path('PedidoTrimSumTotalInsumo/<str:mes1>/<str:mes2>/<str:mes3>/<str:anio>/',PedidoTrimSumTotalInsumo.as_view(), name="PedidoTrimSumTotalInsumo"),
  path('PedidoSemSumTotalInsumo/<str:mes1>/<str:mes2>/<str:mes3>/<str:mes4>/<str:mes5>/<str:mes6>/<str:anio>/', PedidoSemSumTotalInsumo.as_view(), name="PedidoSemSumTotalInsumo"),
  path('PedidoAnualSumTotalInsumo/<str:anio>/', PedidoAnualSumTotalInsumo.as_view(), name="PedidoAnualSumTotalInsumo"),

  path('PedidoTrimSumTotalReactivo/<str:mes1>/<str:mes2>/<str:mes3>/<str:anio>/', PedidoTrimSumTotalReactivo.as_view(), name="PedidoTrimSumTotalReactivo"),
  path('PedidoSemSumTotalReactivo/<str:mes1>/<str:mes2>/<str:mes3>/<str:mes4>/<str:mes5>/<str:mes6>/<str:anio>/', PedidoSemSumTotalReactivo.as_view(), name="PedidoSemSumTotalReactivo"),
  path('PedidoAnualSumTotalReactivo/<str:anio>/', PedidoAnualSumTotalReactivo.as_view(), name="PedidoAnualSumTotalReactivo"),

  path('PedidoTrimSumTotal/<str:mes1>/<str:mes2>/<str:mes3>/<str:anio>/', PedidoTrimSumTotal.as_view(), name="PedidoTrimSumTotal"),
  path('PedidoSemSumTotal/<str:mes1>/<str:mes2>/<str:mes3>/<str:mes4>/<str:mes5>/<str:mes6>/<str:anio>/', PedidoSemSumTotal.as_view(), name="PedidoSemSumTotal"),
  path('PedidoAnualSumTotal/<str:anio>/', PedidoAnualSumTotal.as_view(), name="PedidoAnualSumTotal"),
######### USUARIOS #########
  path('ListaPersonalusuario/<str:usr>/', ListaPersonalusuario.as_view(),name="ListaPersonalusuario"),
]