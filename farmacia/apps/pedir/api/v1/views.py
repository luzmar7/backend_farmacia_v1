from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import HttpResponse
from django.http import Http404  
from django.shortcuts import get_object_or_404
from rest_framework import status
from ....pedir.models import (
  Unidad,
  Cargo,
  Personal,
  Pedido_U,
  producto_pedido,
  Pedido_E,
  producto_pedidoE,
  Personal_PROMES,
)

from ....almacena.models import Medicamento, Insumo_hospitalario, Reactivo
from .serializers import(
  UnidadSerializer,
  CargoSerializer,
  PersonalSerializer,
  PedidoUnidadSerializer,
  PedidoUnidadSerializer2,
  ProductoPedidoSerializer,
  ProductoPedidoSerializer2,
  ProductoPedidoSerializer3,
  PedidoEstadoSerializer,
  ProductoPedidoSerializer4,
  PedidoExternoSerializer,

  ProductoPedidoESerializer,
  PedidoExternoSerializer2,
  ProductoPedidoESerializer2,
  ProductoPedidoESerializer3,
  PedidoExternoEstadoSerializer,
  ProductoPedidoSerializerA,
  ProductoPedidoESerializerA,
  PedidoMesSumSerializer,

  Personal_PROMESSerializer,
)

from django.db.models import Q
from django.db.models import Avg, Count, Min, Sum

################## Unidad ######################
class ListaUnidades(APIView):
  """LISTA TODAS LAS UNIDADES"""
  def get(self, request):
    unidad = Unidad.objects.all().order_by('nombre_unidad')
    data = UnidadSerializer(unidad, many=True).data
    return Response(data)

#--------------- Personal Busca (ID UNIDAD) -----------------------------------
class PersonalBusca(APIView):
  def get(self, request,buscaU, format=None):
    snippets = Personal.objects.all().order_by('nombres').filter(unidad=buscaU)
    serializer = Personal_PROMESSerializer(snippets, many=True)
    return Response(serializer.data)

################## Cargo ######################
class ListaCargos(APIView):
  """LISTA TODOS LOS CARGOS"""
  def get(self, request):
    cargo = Cargo.objects.all().order_by('descripcion')
    data = CargoSerializer(cargo, many=True).data
    return Response(data)

################## Personal ######################
class ListaPersonal(APIView):
  """LISTA TODO EL PERSONAL"""
  def get(self, request):
    personal = Personal.objects.all().order_by('nombre')
    data = PersonalSerializer(personal, many=True).data
    return Response(data)

################## PEDIDO UNIDAD (Enfermeria, Laboratorio, Odontologia) ######################
#---------------- Pedido Guardar ----------------------
class PedidoGuardar(APIView):
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Pedido_U.objects.all().order_by('id_pedido')
    serializer = PedidoUnidadSerializer(snippets, many=True)
    return Response(serializer.data)
  #CREA UN NUEVO REGISTRO
  def post(self, request, format=None):
    serializer = PedidoUnidadSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


#---------------- Pedido (UNIDAD) ----------------------
class PedidoUnidad(APIView):
  #LISTA TODOS LOS REGISTROS
  def get(self, request, unidad, format=None):
    snippets = Pedido_U.objects.all().order_by('id_pedido').filter(unidad__id=unidad)
    serializer = PedidoUnidadSerializer(snippets, many=True)
    return Response(serializer.data)
  #CREA UN NUEVO REGISTRO
  def post(self, request, format=None):
    serializer = PedidoUnidadSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#-------------- Pedido Lista ----------------
class ListaPedido(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido')
    serializer = PedidoUnidadSerializer2(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista (UNIDAD)----------------
class ListaPedidoUnidad(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, unidad, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(unidad__id=unidad)
    serializer = PedidoUnidadSerializer2(snippets, many=True)
    return Response(serializer.data)


#-------------- Pedido Lista (fecha) ----------------
class ListaPedidoFecha(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(fecha_emision=busca)
    serializer = PedidoUnidadSerializer2(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista (fecha) ----------------
class ListaPedidoUnidadFecha(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, unidad, busca, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(unidad__id=unidad).filter(fecha_emision=busca)
    serializer = PedidoUnidadSerializer2(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista (Codigo Pedido) ----------------
class ListaPedidoCodigo(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(id_pedido=busca)
    serializer = PedidoUnidadSerializer2(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Por Entregar  ----------------
class PedidosPorEntregar(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets =Pedido_U.objects.all().order_by('id_pedido').filter(estado='Por Entregar')
    serializer = PedidoUnidadSerializer2(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Por Entregar FARMACIA  ----------------

class PedidosPorEntregarF(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(estado='Por Entregar').filter(localizacion__id=1)
    serializer = PedidoUnidadSerializer2(snippets, many=True)
    return Response(serializer.data)
#---------------- Pedido Lista Por Entregar FARMACIA ----------------------
class ListaPedidoPorEntregarF(APIView):
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Pedido_U.objects.all().order_by('-id_pedido').filter(estado='Por Entregar').filter(localizacion__id=1)
    serializer = PedidoUnidadSerializer(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Por Entregar (FECHA) FARMACIA  ----------------
class PedidosPorEntregarFechaF(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(fecha_emision=busca).filter(estado='Por Entregar').filter(localizacion__id=1)
    serializer = PedidoUnidadSerializer2(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Por Entregar (Codigo Pedido) FARMACIA----------------
class PedidosPorEntregarCodigoF(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(id_pedido=busca).filter(estado='Por Entregar').filter(localizacion__id=1)
    serializer = PedidoUnidadSerializer2(snippets, many=True)
    return Response(serializer.data)


#---------------- Pedido Lista Entregados FARMACIA ----------------------
class ListaPedidoEntregadosF(APIView):
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Pedido_U.objects.all().order_by('-id_pedido').filter(estado='Entregado').filter(localizacion__id=1)
    serializer = PedidoUnidadSerializer(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Entregados FARMACIA  ----------------
class PedidosEntregadosF(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(estado='Entregado').filter(localizacion__id=1).exclude(unidad__id=1)
    serializer = PedidoUnidadSerializer2(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Entregados (FECHA) FARMACIA  ----------------
class PedidosEntregadosFechaF(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(fecha_entrega=busca).filter(estado='Entregado').filter(localizacion__id=1)
    serializer = PedidoUnidadSerializer2(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Entregados (Codigo Pedido) FARMACIA----------------
class PedidosEntregadosCodigoF(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(id_pedido=busca).filter(estado='Entregado').filter(localizacion__id=1)
    serializer = PedidoUnidadSerializer2(snippets, many=True)
    return Response(serializer.data)



#################### PEDIDOS EN ALMACEN ################################################
#---------------- Pedido LISTA FARMACIA A ALMACEN  ----------------------
class ListaPedidoEmitidosA(APIView):
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Pedido_U.objects.all().order_by('-id_pedido').filter(unidad__id=1).filter(localizacion__id=2)
    serializer = PedidoUnidadSerializer(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Emitidos de FARMACIA A ALMACEN ----------------
class ListaPedidoFA(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(unidad__id=1).filter(localizacion__id=2)
    serializer = PedidoUnidadSerializer2(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Emitidos (FECHA) FARMACIA A ALMACEN ----------------
class PedidosEmitidosFechaA(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(unidad__id=1).filter(fecha_emision=busca).filter(localizacion__id=2)
    serializer = PedidoUnidadSerializer2(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Emitidos (Codigo Pedido) FARMACIA A FARMACIA----------------
class PedidosEmitidosCodigoA(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(id_pedido=busca).filter(localizacion__id=2)
    serializer = PedidoUnidadSerializer2(snippets, many=True)
    return Response(serializer.data)


#-------------- Pedido Lista Por Entregar ALMACEN  ----------------
class PedidosPorEntregarA(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets =Pedido_U.objects.all().order_by('id_pedido').filter(estado='Por Entregar').filter(localizacion__id=2)
    serializer = PedidoUnidadSerializer2(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Por Entregar ALMACEN (TODAS LAS UNIDADES - MENOS FARMACIA) ----------------
class ListaPedidoPorEntregarAU(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(estado='Por Entregar').filter(localizacion__id=2).exclude(unidad__id=1)
    serializer = PedidoUnidadSerializer(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Por Entregar ALMACEN (TODAS LAS UNIDADES - MENOS FARMACIA) ----------------
class PedidosPorEntregarAU(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(estado='Por Entregar').filter(localizacion__id=2).exclude(unidad__id=1)
    serializer = PedidoUnidadSerializer2(snippets, many=True)
    return Response(serializer.data)



#-------------- Pedido Lista Por Entregar ALMACEN (FECHA - TODAS LAS UNIDADES - MENOS FARMACIA) ----------------
class PedidosPorEntregarFechaAU(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(estado='Por Entregar').filter(fecha_emision=busca).filter(localizacion__id=2).exclude(unidad__id=1)
    serializer = PedidoUnidadSerializer2(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Por Entregar ALMACEN (CODIGO - TODAS LAS UNIDADES - MENOS FARMACIA) ----------------
class PedidosPorEntregarCodigoAU(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(estado='Por Entregar').filter(id_pedido=busca).filter(localizacion__id=2).exclude(unidad__id=1)
    serializer = PedidoUnidadSerializer2(snippets, many=True)
    return Response(serializer.data)



#-------------- Pedido LISTA Por Entregar ALMACEN A FARMACIA (FARMACIA - MENOS TODAS LAS UNIDADES) ----------------
class ListaPedidosPorEntregarAF(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(estado='Por Entregar').filter(localizacion__id=2).filter(unidad__id=1)
    serializer = PedidoUnidadSerializer(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Por Entregar ALMACEN A FARMACIA (FARMACIA - MENOS TODAS LAS UNIDADES) ----------------
class PedidosPorEntregarAF(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(estado='Por Entregar').filter(localizacion__id=2).filter(unidad__id=1)
    serializer = PedidoUnidadSerializer2(snippets, many=True)
    return Response(serializer.data)


#-------------- Pedido Lista Por Entregar ALMACEN A FARMACIA (FECHA) ----------------
class PedidosPorEntregarFechaAF(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(estado='Por Entregar').filter(fecha_emision=busca).filter(localizacion__id=2).filter(unidad__id=1)
    serializer = PedidoUnidadSerializer2(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Por Entregar ALMACEN A FARMACIA (CODIGO) ----------------
class PedidosPorEntregarCodigoAF(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(estado='Por Entregar').filter(id_pedido=busca).filter(localizacion__id=2).filter(unidad__id=1)
    serializer = PedidoUnidadSerializer2(snippets, many=True)
    return Response(serializer.data)



#-------------- Pedido Lista Entregado ALMACEN (TODAS LAS UNIDADES - MENOS FARMACIA) ----------------
class ListaPedidoEntregadoAU(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(estado='Entregado').filter(localizacion__id=2).exclude(unidad__id=1)
    serializer = PedidoUnidadSerializer(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Entregados ALMACEN  ----------------
class PedidosEntregadosA(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(estado='Entregado').filter(localizacion__id=2).exclude(unidad__id=1)
    serializer = PedidoUnidadSerializer2(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Entregado ALMACEN (FECHA - TODAS LAS UNIDADES - MENOS FARMACIA) ----------------
class PedidosEntregadoFechaAU(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(estado='Entregado').filter(fecha_entrega=busca).filter(localizacion__id=2).exclude(unidad__id=1)
    serializer = PedidoUnidadSerializer2(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Entregado ALMACEN (CODIGO - TODAS LAS UNIDADES - MENOS FARMACIA) ----------------
class PedidosEntregadoCodigoAU(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(estado='Entregado').filter(id_pedido=busca).filter(localizacion__id=2).exclude(unidad__id=1)
    serializer = PedidoUnidadSerializer2(snippets, many=True)
    return Response(serializer.data)


#-------------- Pedido LISTA ENTREGADOS ALMACEN (FARMACIA - MENOS TODAS LAS UNIDADES) ----------------
class ListaPedidosEntregadoAF(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(estado='Entregado').filter(localizacion__id=2).filter(unidad__id=1)
    serializer = PedidoUnidadSerializer(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Entregados ALMACEN (FARMACIA - MENOS TODAS LAS UNIDADES) ----------------
class PedidosEntregadoAF(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(estado='Entregado').filter(localizacion__id=2).filter(unidad__id=1)
    serializer = PedidoUnidadSerializer2(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Entregados ALMACEN A FARMACIA (FECHA) ----------------
class PedidosEntregadoFechaAF(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(estado='Entregado').filter(fecha_entrega=busca).filter(localizacion__id=2).filter(unidad__id=1)
    serializer = PedidoUnidadSerializer2(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Entregados ALMACEN A FARMACIA (CODIGO) ----------------
class PedidosEntregadoCodigoAF(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets =Pedido_U.objects.all().order_by('-id_pedido').filter(estado='Entregado').filter(id_pedido=busca).filter(localizacion__id=2).filter(unidad__id=1)
    serializer = PedidoUnidadSerializer2(snippets, many=True)
    return Response(serializer.data)


#---------- ACTUALIZA ESTADO DE PEDIDO--------------
class PedidoEstadoEdita(APIView):
  def get_object(self, pk):
    try:
      return Pedido_U.objects.get(id_pedido=pk)
    except Pedido_U.DoesNotExist:
      raise Http404
  def get(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = PedidoEstadoSerializer(snippet)
    return Response(serializer.data)

  def put(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = PedidoEstadoSerializer(snippet, data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

################## PRODUCTO-PEDIDO ######################
#---------------- Producto-Pedido Guardar ----------------------
class ProductoPedidoGuardar(APIView):
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = producto_pedido.objects.all()
    serializer = ProductoPedidoSerializer(snippets, many=True)
    return Response(serializer.data)
  #CREA UN NUEVO REGISTRO
  def post(self, request, format=None):
    serializer = ProductoPedidoSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
  
#---------------- Producto-Pedido Busca (ID PEDIDO) ----------------------
class ProductoPedidoBusca(APIView):
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets = producto_pedido.objects.all().filter(id_pedido=busca)
    serializer = ProductoPedidoSerializer2(snippets, many=True)
    return Response(serializer.data)

#---------------- Producto-Pedido Busca (ID PEDIDO) NO ENTREGADO ----------------------
class ProductoPedidoBuscaNoEntregado(APIView):
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets = producto_pedido.objects.all().filter(id_pedido=busca).filter(cantidad_entregada=None)
    serializer = ProductoPedidoSerializer2(snippets, many=True)
    return Response(serializer.data)



#-------------- Producto-Pedido Busca BUSCA (ID)----------------
class ProductoPedidoCantidadEdita(APIView):
  def get_object(self, pk):
    try:
      return producto_pedido.objects.get(id=pk)
    except producto_pedido.DoesNotExist:
      raise Http404
  def get(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = ProductoPedidoSerializer3(snippet)
    return Response(serializer.data)

  def put(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = ProductoPedidoSerializer3(snippet, data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

###--- ACTUALIZA PRODUCTO-PEDIDO ------------------------------
class ProductoPedidoEdita(APIView):
  def get_object(self, pk):
    try:
      return producto_pedido.objects.get(id=pk)
    except producto_pedido.DoesNotExist:
      raise Http404

  def get(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = ProductoPedidoSerializer4(snippet)
    return Response(serializer.data)

  def put(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = ProductoPedidoSerializer4(snippet, data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
################## PEDIDO SSU (Pedido Externo) ######################
#---------------- Pedido Guardar ----------------------
class PedidoExternoGuardar(APIView):
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Pedido_E.objects.all().order_by('id_pedido')
    serializer = PedidoExternoSerializer(snippets, many=True)
    return Response(serializer.data)
  #CREA UN NUEVO REGISTRO
  def post(self, request, format=None):
    serializer = PedidoExternoSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



#---------- ACTUALIZA ESTADO DE PEDIDO SSU (EXTERNO)--------------
class PedidoSSUEstadoEdita(APIView):
  def get_object(self, pk):
    try:
      return Pedido_E.objects.get(id_pedido=pk)
    except Pedido_E.DoesNotExist:
      raise Http404
  def get(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = PedidoExternoEstadoSerializer(snippet)
    return Response(serializer.data)

  def put(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = PedidoExternoEstadoSerializer(snippet, data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#---------------- Pedido LISTA SEGURO SOCIAL UNIVERSITARIO   ----------------------
class ListaPedidoExterno(APIView):
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Pedido_E.objects.all().order_by('-id_pedido')
    serializer = PedidoExternoSerializer(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista de SEGURO SOCIAL UNIVERSITARIO ----------------
class ListaPedidoSSU(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets =Pedido_E.objects.all().order_by('-id_pedido')
    serializer = PedidoExternoSerializer2(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Emitidos (FECHA) SEGURO SOCIAL UNIVERSITARIO ----------------
class ListaPedidoFechaSSU(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets =Pedido_E.objects.all().order_by('-id_pedido').filter(fecha_emision=busca)
    serializer = PedidoExternoSerializer2(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Emitidos (ID PEDIDO) SEGURO SOCIAL UNIVERSITARIO ----------------
class ListaPedidoCodigoSSU(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets =Pedido_E.objects.all().order_by('-id_pedido').filter(id_pedido=busca)
    serializer = PedidoExternoSerializer2(snippets, many=True)
    return Response(serializer.data)


#---------------- Pedido (LISTA POR RECIBIR) SEGURO SOCIAL UNIVERSITARIO   ----------------------
class ListaPedidoSSUPorRecibir(APIView):
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Pedido_E.objects.all().order_by('-id_pedido').filter(estado='Por Recibir')
    serializer = PedidoExternoSerializer(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Por Recibir SEGURO SOCIAL UNIVERSITARIO ----------------
class PedidoSSUPorRecibir(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets =Pedido_E.objects.all().order_by('-id_pedido').filter(estado='Por Recibir')
    serializer = PedidoExternoSerializer2(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Por Recibir (FECHA) SEGURO SOCIAL UNIVERSITARIO ----------------
class PedidoSSUPorRecibirFecha(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request,busca, format=None):
    snippets =Pedido_E.objects.all().order_by('-id_pedido').filter(estado='Por Recibir').filter(fecha_emision=busca)
    serializer = PedidoExternoSerializer2(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Por Recibir (ID PEDIDO) SEGURO SOCIAL UNIVERSITARIO ----------------
class PedidoSSUPorRecibirCodigo(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets =Pedido_E.objects.all().order_by('-id_pedido').filter(estado='Por Recibir').filter(id_pedido=busca)
    serializer = PedidoExternoSerializer2(snippets, many=True)
    return Response(serializer.data)

#---------------- Pedido (LISTA RECIBIDO) SEGURO SOCIAL UNIVERSITARIO   ----------------------
class ListaPedidoSSURecibido(APIView):
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Pedido_E.objects.all().order_by('-id_pedido').filter(estado='Recibido')
    serializer = PedidoExternoSerializer(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Recibido SEGURO SOCIAL UNIVERSITARIO ----------------
class PedidoSSURecibido(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets =Pedido_E.objects.all().order_by('-id_pedido').filter(estado='Recibido')
    serializer = PedidoExternoSerializer2(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Recibido (FECHA) SEGURO SOCIAL UNIVERSITARIO ----------------
class PedidoSSURecibidoFecha(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request,busca, format=None):
    snippets =Pedido_E.objects.all().order_by('-id_pedido').filter(estado='Recibido').filter(fecha_recibida=busca)
    serializer = PedidoExternoSerializer2(snippets, many=True)
    return Response(serializer.data)

#-------------- Pedido Lista Recibido (ID PEDIDO) SEGURO SOCIAL UNIVERSITARIO ----------------
class PedidoSSURecibidoCodigo(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets =Pedido_E.objects.all().order_by('-id_pedido').filter(estado='Recibido').filter(id_pedido=busca)
    serializer = PedidoExternoSerializer2(snippets, many=True)
    return Response(serializer.data)

################## PRODUCTO-PEDIDO SSU ######################
#---------------- Producto-Pedido Guardar ----------------------
class ProductoPedidoEGuardar(APIView):
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = producto_pedidoE.objects.all()
    serializer = ProductoPedidoESerializer(snippets, many=True)
    return Response(serializer.data)
  #CREA UN NUEVO REGISTRO
  def post(self, request, format=None):
    serializer = ProductoPedidoESerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


#---------------- Producto-PedidoE SSU (EXTERNO) Busca (ID PEDIDO) ----------------------
class ProductoPedidoSSUBusca(APIView):
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets = producto_pedidoE.objects.all().filter(id_pedido=busca)
    serializer = ProductoPedidoESerializer2(snippets, many=True)
    return Response(serializer.data)

#---------------- Producto-PedidoE SSU (EXTERNO) Busca (ID PEDIDO) CANTIDAD RECIBIDA IGUAL NULL ----------------------
class ProductoPedidoSSUBusca2(APIView):
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets = producto_pedidoE.objects.all().filter(cantidad_recibida=None).filter(id_pedido=busca)
    serializer = ProductoPedidoESerializer2(snippets, many=True)
    return Response(serializer.data)

#---------------- Producto-PedidoE SSU (EXTERNO) Busca (ID PEDIDO) CANTIDAD RECIBIDA DISTINTO NULL ----------------------
class ProductoPedidoSSUBusca3(APIView):
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets = producto_pedidoE.objects.all().exclude(cantidad_recibida=None).filter(id_pedido=busca)
    serializer = ProductoPedidoESerializer2(snippets, many=True)
    return Response(serializer.data)

#-------------- Producto-Pedido SSU(EXTERNO) Busca BUSCA (ID)----------------
class ProductoPedidoECantidadEdita(APIView):
  def get_object(self, pk):
    try:
      return producto_pedidoE.objects.get(id=pk)
    except producto_pedidoE.DoesNotExist:
      raise Http404
  def get(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = ProductoPedidoESerializer3(snippet)
    return Response(serializer.data)

  def put(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = ProductoPedidoESerializer3(snippet, data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#################### BIND CARD PEDIDO ##################################
#--------------------- BINDCARD PEDIDO EN FARMACIA-------------------        
class BindCardPedido(APIView):
  def get(self, request, format=None):
    snippets= producto_pedido.objects.filter(id_pedido__estado="Entregado") #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = ProductoPedidoSerializer2(snippets, many=True)
    return Response(serializer.data)

class BindCardPedidoBusca(APIView):
  def get(self, request,busca, format=None):
    snippets= producto_pedido.objects.filter(id_pedido__estado="Entregado").filter(id_pedido__localizacion=1).filter(codigo=busca) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = ProductoPedidoSerializerA(snippets, many=True)
    return Response(serializer.data)

class BindCardPedidoBuscaFecha(APIView):
  def get(self, request, codigo, fecha, format=None):
    snippets= producto_pedido.objects.filter(id_pedido__estado="Entregado").filter(id_pedido__localizacion=1).filter(codigo=codigo).filter(id_pedido__fecha_entrega=fecha) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = ProductoPedidoSerializerA(snippets, many=True)
    return Response(serializer.data)

class BindCardPedidoEBusca(APIView):
  def get(self, request,busca, format=None):
    snippets= producto_pedidoE.objects.filter(id_pedido__estado="Recibido").filter(id_pedido__localizacion=1).filter(codigo=busca) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = ProductoPedidoESerializerA(snippets, many=True)
    return Response(serializer.data)

class BindCardPedidoEBuscaFecha(APIView):
  def get(self, request, codigo, fecha, format=None):
    snippets= producto_pedidoE.objects.filter(id_pedido__estado="Recibido").filter(id_pedido__localizacion=1).filter(codigo=codigo).filter(id_pedido__fecha_recibida=fecha) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = ProductoPedidoESerializerA(snippets, many=True)
    return Response(serializer.data)

###################### REPORTES ###########################
#----------------------- PEDIDOS SUM SALIDAS---------------

class PedidoMesSum(APIView):
  def get(self, request, codigo, mes, anio, format=None):
    snippets= producto_pedido.objects.values('codigo_id').filter(id_pedido__fecha_entrega__month=mes).filter(id_pedido__fecha_entrega__year=anio).filter(codigo=codigo).exclude(id_pedido__unidad__id=1).annotate(sum=Sum('cantidad_entregada'))
    serializer = PedidoMesSumSerializer(snippets, many=True)  
    return Response(serializer.data)

#----------------------- PEDIDOS SUM TRIMESTRAL SALIDAS---------------
class PedidoTrimSum(APIView):
  def get(self, request, codigo, mes1, mes2, mes3, anio, format=None):
    snippets= producto_pedido.objects.values('codigo_id').filter(Q(id_pedido__fecha_entrega__month=mes1) | Q(id_pedido__fecha_entrega__month=mes2) | Q(id_pedido__fecha_entrega__month=mes3)).filter(id_pedido__fecha_entrega__year=anio).filter(codigo=codigo).exclude(id_pedido__unidad__id=1).annotate(sum=Sum('cantidad_entregada'))
    serializer = PedidoMesSumSerializer(snippets, many=True)   
    return Response(serializer.data)

#----------------------- PEDIDOS SUM SEMESTRAL SALIDAS---------------
class PedidoSemSum(APIView):
  def get(self, request, codigo, mes1, mes2, mes3, mes4, mes5, mes6, anio, format=None):
    snippets= producto_pedido.objects.values('codigo_id').filter(Q(id_pedido__fecha_entrega__month=mes1) | Q(id_pedido__fecha_entrega__month=mes2) | Q(id_pedido__fecha_entrega__month=mes3) | Q(id_pedido__fecha_entrega__month=mes4) | Q(id_pedido__fecha_entrega__month=mes5) | Q(id_pedido__fecha_entrega__month=mes6)).filter(id_pedido__fecha_entrega__year=anio).filter(codigo=codigo).exclude(id_pedido__unidad__id=1).annotate(sum=Sum('cantidad_entregada'))
    serializer = PedidoMesSumSerializer(snippets, many=True)   
    return Response(serializer.data)

#----------------------- PEDIDOS SUM TRIMESTRAL TOTAL SALIDAS---------------
class PedidoTrimSumTotal(APIView):
  def get(self, request, mes1, mes2, mes3, anio, format=None):
    snippets= producto_pedido.objects.all().filter(codigo__habilitado=True).filter(Q(id_pedido__fecha_entrega__month=mes1) | Q(id_pedido__fecha_entrega__month=mes2) | Q(id_pedido__fecha_entrega__month=mes3)).filter(id_pedido__fecha_entrega__year=anio).exclude(id_pedido__unidad__id=1).aggregate(sum=Sum('cantidad_entregada')) 
    return Response(snippets)

#----------------------- PEDIDOS SUM SEMESTRAL TOTAL SALIDAS---------------
class PedidoSemSumTotal(APIView):
  def get(self, request, mes1, mes2, mes3, mes4, mes5, mes6, anio, format=None):
    snippets= producto_pedido.objects.all().filter(codigo__habilitado=True).filter(Q(id_pedido__fecha_entrega__month=mes1) | Q(id_pedido__fecha_entrega__month=mes2) | Q(id_pedido__fecha_entrega__month=mes3) | Q(id_pedido__fecha_entrega__month=mes4) | Q(id_pedido__fecha_entrega__month=mes5) | Q(id_pedido__fecha_entrega__month=mes6)).filter(id_pedido__fecha_entrega__year=anio).exclude(id_pedido__unidad__id=1).aggregate(sum=Sum('cantidad_entregada')) 
    return Response(snippets)

#----------------------- PEDIDOS SUM ANUAL TOTAL SALIDAS---------------
class PedidoAnualSumTotal(APIView):
  def get(self, request, anio, format=None):
    snippets= producto_pedido.objects.all().filter(codigo__habilitado=True).filter(id_pedido__fecha_entrega__year=anio).exclude(id_pedido__unidad__id=1).aggregate(sum=Sum('cantidad_entregada')) 
    return Response(snippets)


#----------------------- PEDIDOS SUM TRIMESTRAL TOTAL SALIDAS MEDICAMENTOS---------------
class PedidoTrimSumTotalMedicamento(APIView):
  def get(self, request, mes1, mes2, mes3, anio, format=None):
    result = Medicamento.objects.all()
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets= producto_pedido.objects.all().filter(codigo__in=lista).filter(codigo__habilitado=True).filter(Q(id_pedido__fecha_entrega__month=mes1) | Q(id_pedido__fecha_entrega__month=mes2) | Q(id_pedido__fecha_entrega__month=mes3)).filter(id_pedido__fecha_entrega__year=anio).exclude(id_pedido__unidad__id=1).aggregate(sum=Sum('cantidad_entregada')) 
    return Response(snippets)

class PedidoSemSumTotalMedicamento(APIView):
  def get(self, request, mes1, mes2, mes3, mes4, mes5, mes6, anio, format=None):
    result = Medicamento.objects.all()
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets= producto_pedido.objects.all().filter(codigo__in=lista).filter(codigo__habilitado=True).filter(Q(id_pedido__fecha_entrega__month=mes1) | Q(id_pedido__fecha_entrega__month=mes2) | Q(id_pedido__fecha_entrega__month=mes3) | Q(id_pedido__fecha_entrega__month=mes4) | Q(id_pedido__fecha_entrega__month=mes5) | Q(id_pedido__fecha_entrega__month=mes6)).filter(id_pedido__fecha_entrega__year=anio).exclude(id_pedido__unidad__id=1).aggregate(sum=Sum('cantidad_entregada')) 
    return Response(snippets)


class PedidoAnualSum(APIView):
  def get(self, request, codigo, anio, format=None):
    snippets= producto_pedido.objects.values('codigo_id').filter(id_pedido__estado="Entregado").filter(id_pedido__fecha_entrega__year=anio).filter(codigo=codigo).exclude(id_pedido__unidad__id=1).annotate(sum=Sum('cantidad_entregada'))
    serializer = PedidoMesSumSerializer(snippets, many=True)   
    return Response(serializer.data)

class PedidoAnualSumTotalMedicamento(APIView):
  def get(self, request, anio, format=None):
    result = Medicamento.objects.all()
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets= producto_pedido.objects.all().filter(codigo__in=lista).filter(codigo__habilitado=True).filter(id_pedido__estado="Entregado").filter(id_pedido__fecha_entrega__year=anio).exclude(id_pedido__unidad__id=1).aggregate(sum=Sum('cantidad_entregada')) 
    return Response(snippets)

#----------------------- PEDIDOS SUM TRIMESTRAL TOTAL SALIDAS INSUMOS---------------
class PedidoTrimSumTotalInsumo(APIView):
  def get(self, request, mes1, mes2, mes3, anio, format=None):
    result = Insumo_hospitalario.objects.all()
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets= producto_pedido.objects.all().filter(codigo__in=lista).filter(codigo__habilitado=True).filter(Q(id_pedido__fecha_entrega__month=mes1) | Q(id_pedido__fecha_entrega__month=mes2) | Q(id_pedido__fecha_entrega__month=mes3)).filter(id_pedido__fecha_entrega__year=anio).exclude(id_pedido__unidad__id=1).aggregate(sum=Sum('cantidad_entregada')) 
    return Response(snippets)

#----------------------- PEDIDOS SUM SEMESTRAL TOTAL SALIDAS INSUMOS---------------
class PedidoSemSumTotalInsumo(APIView):
  def get(self, request, mes1, mes2, mes3, mes4, mes5, mes6, anio, format=None):
    result = Insumo_hospitalario.objects.all()
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets= producto_pedido.objects.all().filter(codigo__in=lista).filter(codigo__habilitado=True).filter(Q(id_pedido__fecha_entrega__month=mes1) | Q(id_pedido__fecha_entrega__month=mes2) | Q(id_pedido__fecha_entrega__month=mes3) | Q(id_pedido__fecha_entrega__month=mes4) | Q(id_pedido__fecha_entrega__month=mes5) | Q(id_pedido__fecha_entrega__month=mes6)).filter(id_pedido__fecha_entrega__year=anio).exclude(id_pedido__unidad__id=1).aggregate(sum=Sum('cantidad_entregada')) 
    return Response(snippets)
#----------------------- PEDIDOS SUM ANUAL TOTAL SALIDAS INSUMOS---------------

class PedidoAnualSumTotalInsumo(APIView):
  def get(self, request, anio, format=None):
    result = Insumo_hospitalario.objects.all()
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets= producto_pedido.objects.all().filter(codigo__in=lista).filter(codigo__habilitado=True).filter(id_pedido__estado="Entregado").filter(id_pedido__fecha_entrega__year=anio).exclude(id_pedido__unidad__id=1).aggregate(sum=Sum('cantidad_entregada')) 
    return Response(snippets)

#----------------------- PEDIDOS SUM TRIMESTRAL TOTAL SALIDAS REACTIVOS---------------
class PedidoTrimSumTotalReactivo(APIView):
  def get(self, request, mes1, mes2, mes3, anio, format=None):
    result = Reactivo.objects.all()
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets= producto_pedido.objects.all().filter(codigo__in=lista).filter(codigo__habilitado=True).filter(Q(id_pedido__fecha_entrega__month=mes1) | Q(id_pedido__fecha_entrega__month=mes2) | Q(id_pedido__fecha_entrega__month=mes3)).filter(id_pedido__fecha_entrega__year=anio).exclude(id_pedido__unidad__id=1).aggregate(sum=Sum('cantidad_entregada')) 
    return Response(snippets)

#----------------------- PEDIDOS SUM SEMESTRAL TOTAL SALIDAS REACTIVOS---------------
class PedidoSemSumTotalReactivo(APIView):
  def get(self, request, mes1, mes2, mes3, mes4, mes5, mes6, anio, format=None):
    result = Reactivo.objects.all()
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets= producto_pedido.objects.all().filter(codigo__in=lista).filter(codigo__habilitado=True).filter(Q(id_pedido__fecha_entrega__month=mes1) | Q(id_pedido__fecha_entrega__month=mes2) | Q(id_pedido__fecha_entrega__month=mes3) | Q(id_pedido__fecha_entrega__month=mes4) | Q(id_pedido__fecha_entrega__month=mes5) | Q(id_pedido__fecha_entrega__month=mes6)).filter(id_pedido__fecha_entrega__year=anio).exclude(id_pedido__unidad__id=1).aggregate(sum=Sum('cantidad_entregada')) 
    return Response(snippets)

#----------------------- PEDIDOS SUM ANUAL TOTAL SALIDAS REACTIVOS---------------
class PedidoAnualSumTotalReactivo(APIView):
  def get(self, request, anio, format=None):
    result = Reactivo.objects.all()
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets= producto_pedido.objects.all().filter(codigo__in=lista).filter(codigo__habilitado=True).filter(id_pedido__estado="Entregado").filter(id_pedido__fecha_entrega__year=anio).exclude(id_pedido__unidad__id=1).aggregate(sum=Sum('cantidad_entregada')) 
    return Response(snippets)

    #desde aqui se hace con el login

class ListaPersonalusuario(APIView):
  def get(self, request, usr, format=None):
    Formulario = Personal.objects.all().filter(usuario__username=usr)
    serializer = Personal_PROMESSerializer(Formulario, many=True)
    return Response(serializer.data)