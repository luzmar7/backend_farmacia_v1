from django.contrib import admin
from django.urls import path, include

app_name = 'pedir'
urlpatterns = [
    path('v1/',include("apps.pedir.api.v1.urls", namespace="v1")),
]
