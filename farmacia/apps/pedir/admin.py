from django.contrib import admin

from apps.pedir.models import (
  Unidad,
  Cargo,
  Personal,
  Pedido_U,
  producto_pedido,
  Farmaceutica,

  Pedido_E,
  producto_pedidoE,
  # Personal_PROMES,
  T_Especialidad,
)

@admin.register(Unidad)
class UnidadAdmin(admin.ModelAdmin):
  list_display = ["id", "nombre_unidad"]
  
@admin.register(Cargo)
class CargoAdmin(admin.ModelAdmin):
  list_display = ["id", "descripcion"]

@admin.register(T_Especialidad)
class T_EspecialidadAdmin(admin.ModelAdmin):
  list_display = ["id", "especialidad"]

@admin.register(Personal)
class PersonalAdmin(admin.ModelAdmin):
  list_display = ["ci","primer_apellido","segundo_apellido","nombres","genero","sector","responsable","cargo","carga_horaria","estado","unidad","especialidad","usuario", "foto_perfil"]

@admin.register(Pedido_U)
class Pedido_UAdmin(admin.ModelAdmin):
  list_display = ["id_pedido", "fecha_emision", "fecha_entrega", "hora_emision", "hora_entrega","unidad", "localizacion", "personal", "farmaceutica", "estado"]

@admin.register(producto_pedido)
class producto_pedidoAdmin(admin.ModelAdmin):
  list_display = ["id" ,"id_pedido", "codigo", "cantidad_solicitada", "cantidad_entregada", "lote_salida", "fecha_expiracion", "unidadProd"]

@admin.register(Pedido_E)
class Pedido_EAdmin(admin.ModelAdmin):
  list_display = ["id_pedido", "fecha_emision", "fecha_recibida", "hora_emision", "hora_recibida", "unidad", "localizacion", "personal", "personal_externo", "estado"]

@admin.register(producto_pedidoE)
class producto_pedidoEAdmin(admin.ModelAdmin):
  list_display = ["id" ,"id_pedido", "codigo", "cantidad_solicitada", "cantidad_recibida", "lote_entrada", "fecha_expiracion", "cantidad_farmacia","cantidad_almacen", "unidadProd"]

@admin.register(Farmaceutica)
class FarmaceuticaAdmin(admin.ModelAdmin):
  list_display = ["personal_id", "personal"]

# @admin.register(Personal_PROMES)
# class Personal_PROMESAdmin(admin.ModelAdmin):
#   list_display = ["nro_item","paterno","materno","nombres","genero","sector","responsable","cargo","cargaHoraria","estado","unidad","usuario", "perfil"]
