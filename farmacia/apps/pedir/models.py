from django.db import models
from ..almacena.models import Producto_farmaceutico, Lote, Almacen
import datetime
from django.contrib.auth import get_user_model

User = get_user_model()

class Unidad(models.Model):
  nombre_unidad = models.CharField(
    max_length=50,
    verbose_name="Nombre Unidad"
  )
  class Meta:
    verbose_name =("Unidad")
    verbose_name_plural =("Unidades")
  def __str__(self):
    return self.nombre_unidad

class Cargo(models.Model):
  descripcion = models.CharField(
    max_length=50,
    verbose_name="Cargo"
  )    
  class Meta:
    verbose_name =("Cargo")
    verbose_name_plural =("Cargos")
  def __str__(self):
    return self.descripcion

class T_Especialidad(models.Model):
  especialidad = models.CharField(max_length = 30)
  def __str__(self):
    return self.especialidad

''' MODELO PERSONAL V1 MODIFICADO CON EL EXCEL'''   
class Personal(models.Model):
  ci = models.CharField(primary_key= True, max_length = 15)
  nombres = models.CharField(max_length = 30)
  primer_apellido = models.CharField(max_length = 30,blank=True)
  segundo_apellido = models.CharField(max_length = 30,blank=True)
  Genero_CHOICES= (
  ('Femenino', 'Femenino'),
  ('Masculino', 'Masculino')
  )
  genero= models.CharField(max_length=20, choices=Genero_CHOICES, default='',blank=True)
  item=models.CharField(max_length=20,blank=True)
  sector = models.CharField(max_length=50)
  responsable=models.BooleanField(default=False)
  cargo = models.CharField(max_length=50)
  carga_horaria = models.CharField(max_length=20)
  Estado_CHOICES= (
  ('Activo', 'Activo'),
  ('Inactivo', 'Inactivo')
  )
  estado= models.CharField(max_length=15, choices=Estado_CHOICES, default='Inactivo',blank=True)
  foto_perfil=models.ImageField(upload_to='perfiles', blank=True, null=True)
  unidad=models.ForeignKey(Unidad,on_delete=models.CASCADE)
  usuario=models.OneToOneField(User,on_delete=models.CASCADE) # si se elimina el usuario se eliminara todo lo que se guardo con el
  especialidad = models.ForeignKey(T_Especialidad, on_delete = models.SET_NULL,blank = True, null = True)
  def _str_(self):
    return '%s %s' % (self.nombres, self.primer_apellido)
  class Meta:
    verbose_name_plural = ("Personal PROMES")

class Farmaceutica(models.Model):
  personal = models.OneToOneField(
    Personal,
    verbose_name ="Farmaceutica",
    on_delete = models.CASCADE, 
    primary_key =True, 
    unique = True 
  )
  class Meta:
    verbose_name =("Farmaceutica")
    verbose_name_plural =("Farmaceuticas")
  def __str__(self):
      return '%s %s' % (self.personal.nombre, self.personal.apellidos)
      
''' MODELO PERSONAL V1 MODIFICADO CON EL EXCEL'''
class Personal_PROMES(models.Model):
  Genero= (('F','Femenino'), ('M','Masculino'))
  nro_item = models.IntegerField(
    primary_key=True,
    null=False,
    verbose_name="Nro Item"
  )
  paterno = models.CharField(
    max_length=100,
    verbose_name="Apellido Paterno" 
  )
  materno = models.CharField(
    max_length=100,
    verbose_name="Apellido Materno"
  )
  nombres = models.CharField(
    max_length=100,
    verbose_name="Nombres"
  )
  genero = models.CharField(
    max_length = 1, 
    choices = Genero,
    default = 'M'
  )
  sector = models.CharField(
    max_length=50,
    verbose_name="Sector"
  )
  responsable=models.BooleanField(
    default=False,
    verbose_name="Responsable"
  )
  cargo = models.CharField(
    max_length=100,
    verbose_name="Cargo"
  )
  cargaHoraria = models.CharField(
    max_length=100,
    verbose_name="Carga Horaria"
  )
  ESTADO_CHOICES= (
  ('Activo', 'Activo'),
  ('Inactivo', 'Inactivo')
  )
  estado= models.CharField(
    max_length=20, 
    choices=ESTADO_CHOICES, 
    default='',
    blank=True,
    verbose_name="Activo/Inactivo"
  )
  unidad=models.ForeignKey(
    Unidad,
    on_delete=models.CASCADE,
    verbose_name="Unidad"
  )
  usuario=models.ForeignKey(
    User,
    on_delete=models.CASCADE,
    verbose_name="Usuario"
  )
  perfil=models.ImageField(
    upload_to='perfiles', 
    blank=True, 
    null=True
  )
  class Meta:
    verbose_name_plural = ("Personal PROMES")  
  def __str__(self):
      return '%s %s' % (self.nombres, self.paterno)
  

class Pedido_U(models.Model):
  ESTADO = (
    ('Por Entregar','Por Entregar'),
    ('Entregado','Entregado'),
    ('No Entregado','No Entregado'),
    ('Anulada','Anulada')
  )
  id_pedido = models.AutoField(
    primary_key=True, 
    null=False,
    verbose_name="Id Pedido",
  )
  fecha_emision = models.DateField(
    default=datetime.date.today,
    verbose_name="Fecha de Emision",
  )
  fecha_entrega = models.DateField(
    verbose_name="Fecha de Entrega",
    null=True,
    blank = True,
  )
  hora_emision = models.TimeField(
    verbose_name= "Hora Emision",
  )
  hora_entrega = models.TimeField(
    verbose_name="Hora Entrega",
    null=True, 
    blank=True
  )
  localizacion = models.ForeignKey(
    Almacen, 
    on_delete=models.CASCADE,
    verbose_name="Localizacion"
  )
  unidad = models.ForeignKey(
    Unidad, 
    on_delete=models.CASCADE,
    verbose_name="Id Unidad"
  )
  personal = models.ForeignKey(
    Personal, 
    on_delete=models.CASCADE,
    related_name="Personal1"
  )
  farmaceutica = models.ForeignKey(
    Personal,
    on_delete=models.CASCADE,
    related_name="Personal2",   
  )
  estado = models.CharField(
    max_length = 30, 
    choices = ESTADO, 
    default = 'Por Entregar',
    verbose_name="Estado"
  )
  class Meta:
    verbose_name = ("Pedido Unidad")
    verbose_name_plural = ("Pedido Unidades")
  def __int__(self):
    return self.id_pedido

class producto_pedido(models.Model):
  UNIDAD_PROD = (
    ('PIEZA','PIEZA'),
    ('FRASCO','FRASCO'),
    ('CAJA','CAJA'),
  )
  id_pedido = models.ForeignKey(
    Pedido_U, 
    on_delete=models.CASCADE,
    verbose_name="Id Pedido"
  )
  codigo = models.ForeignKey(
    Producto_farmaceutico,
    on_delete=models.CASCADE,
    verbose_name="Codigo Producto"
  )
  cantidad_solicitada = models.PositiveIntegerField(
    verbose_name="Cantidad Solicitada"
  )
  cantidad_entregada = models.PositiveIntegerField(
    verbose_name="Cantidad Entregada",
    null=True,
    blank = True,
  )
  lote_salida = models.CharField(
    verbose_name="lote salida",
    max_length=20,
    null=True,
    blank = True,
  )
  fecha_expiracion = models.DateField(
    verbose_name="Fecha de expiración",
    null=True,
    blank = True,
  ) 
  unidadProd = models.CharField(
    max_length = 30, 
    choices = UNIDAD_PROD, 
    verbose_name="Unidad Producto"
  )
  class Meta:
    verbose_name = ("Producto Pedido")
    verbose_name_plural = ("Productos Pedidos")
  def __int__(self):
    return self.id_pedido

#PEDIDO AL SEGURO SOCIAL UNIVERSITARIO 
class Pedido_E(models.Model):
  ESTADO = (
    ('Por Recibir','Por Recibir'),
    ('Recibido','Recibido'),
    ('No Recibido','No Recibido'),
    ('Anulada','Anulada')
  )
  id_pedido = models.AutoField(
    primary_key=True, 
    null=False,
    verbose_name="Id Pedido",
  )
  fecha_emision = models.DateField(
    default=datetime.date.today,
    verbose_name="Fecha de Emision",
  )
  fecha_recibida = models.DateField(
    verbose_name="Fecha de recepcion",
    null=True,
    blank = True,
  )
  hora_emision = models.TimeField(
    verbose_name= "Hora de Emision",
  )
  hora_recibida = models.TimeField(
    verbose_name="Hora de recepcion",
    null=True, 
    blank=True
  )
  localizacion = models.ForeignKey(
    Almacen, 
    on_delete=models.CASCADE,
    verbose_name="Localizacion"
  )
  unidad = models.CharField(
    max_length = 30, 
    verbose_name="Externo"
  )
  personal = models.ForeignKey(
    Personal, 
    on_delete=models.CASCADE,
    verbose_name="Personal"
  )
  personal_externo = models.CharField(
    max_length = 30, 
    null=True,
    blank = True,
    verbose_name="Entregado por"
  )
  estado = models.CharField(
    max_length = 30, 
    choices = ESTADO, 
    default = 'Por Recibir',
    verbose_name="Estado"
  )
  class Meta:
    verbose_name = ("Pedido SSU")
    verbose_name_plural = ("Pedidos SSU")
  def __int__(self):
    return self.id_pedido

class producto_pedidoE(models.Model):
  UNIDAD_PROD = (
    ('PIEZA','PIEZA'),
    ('FRASCO','FRASCO'),
    ('CAJA','CAJA'),
  )
  id_pedido = models.ForeignKey(
    Pedido_E, 
    on_delete=models.CASCADE,
    verbose_name="Id Pedido"
  )
  codigo = models.ForeignKey(
    Producto_farmaceutico,
    on_delete=models.CASCADE,
    verbose_name="Codigo Producto"
  )
  cantidad_solicitada = models.PositiveIntegerField(
    verbose_name="Cantidad Solicitada"
  )
  cantidad_recibida = models.PositiveIntegerField(
    verbose_name="Cantidad recibida",
    null=True,
    blank = True,
  )
  lote_entrada = models.CharField(
    verbose_name="lote entrada",
    max_length=20,
    null=True,
    blank = True,
  )
  fecha_expiracion = models.DateField(
    verbose_name="Fecha de expiración",
    null=True,
    blank = True,
  ) 
  cantidad_farmacia = models.PositiveIntegerField(
    verbose_name="Cantidad Farmacia",
    null=True,
    blank = True,
  ) 
  cantidad_almacen = models.PositiveIntegerField(
    verbose_name="Cantidad Almacen",
    null=True,
    blank = True,
  ) 
  unidadProd = models.CharField(
    max_length = 30, 
    choices = UNIDAD_PROD, 
    verbose_name="Unidad Producto"
  )
  class Meta:
    verbose_name = ("Producto Pedido SSU")
    verbose_name_plural = ("Productos Pedidos SSU")
  def __int__(self):
    return self.id_pedido


