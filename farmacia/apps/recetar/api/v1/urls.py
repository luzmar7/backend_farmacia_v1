from django.urls import include, path
from rest_framework.urlpatterns import format_suffix_patterns

from .views import (
  Destudiantex,
  EstudianteDetalle,
  IngresarAfiliado,
  IngresarEstudiante,
  ListaAfiliado,
  ListaCarrera,
  ListaEstudiantes,
  ListaProvincia,
  ListaSangre,
  RegistroAfiliado,
  ListaCiudad,
  ListaAlergia,
  ListaRegistroAlergias,
  AlergiasAfiliado,
  DetalleAfiliado,

  EstCivil,
  EditaAfiliado,
  ListaAfiliadoDetalle,
  VerBajaAfiliado,
  SolicitarBaja,
  VerEditarBaja,
  TBajas,
  AfiliadodelDia,
  BajasdelDia,
  ConsultaLista,

  ConsultaGuardar,
  RecetaGuardar,
  MedicoLista,
  DetalleRecetaBusca,
  RecetaLista,
  MedicamentoRecetaGuardar,
  DetalleMedicamentoRecetaBusca,

  OtroMedicamentoLista,
  OtroMedicamentoBusca,
  RecetasPorEntregar,
  RecetasEntregadas,
  RecetasAnuladas,

  RecetaEstadoEdita,
  RecetaEstadoEdita2,
  BindCardReceta,

  BindCardRecetaBusca,
  Medicamento_RecetaEdita,

  InsumoRecetaGuardar,
  DetalleInsumoRecetaBusca,
  BindCardRecetaIBusca,
  Insumo_RecetaEdita,
  RecetaListaFecha,
  RecetaListaAfiliado,
  MedicoBusca,

  RecetasPorEntregarFecha,
  RecetasPorEntregarAfiliado,

  RecetasEntregadasFecha,
  RecetasEntregadasAfiliado,

  RecetasAnuladasFecha,
  RecetasAnuladasAfiliado,
  
  BindCardRecetaBuscaFecha,
  BindCardRecetaIBuscaFecha,

  RecetaMesSumMedicamento,
  RecetaTrimSumMedicamento,
  RecetaTrimSumTotalMedicamento,
  
  RecetaSemSumMedicamento,
  RecetaSemSumTotalMedicamento,

  RecetaAnualSumMedicamento,
  RecetaAnualSumTotalMedicamento,

  RecetaMesSumInsumo,
  RecetaTrimSumInsumo,
  RecetaTrimSumTotalInsumo,

  RecetaSemSumInsumo,
  RecetaSemSumTotalInsumo,
  RecetaAnualSumInsumo,
  RecetaAnualSumTotalInsumo,
  RecetaAnualSumTotalInsumo,
  MedicamentoRecetaBuscaNoEntregado,
  InsumoRecetaBuscaNoEntregado,
  RecetaListaMedico,
  RecetaListaMedicoFecha,
  RecetaListaMedicoAfiliado,
  RecetasPorEntregarMedico,
)
app_name = 'recetar'

urlpatterns = [
  path('detalleafiliados/', ListaAfiliadoDetalle.as_view(), name='ListaAfiliadoDetalle'),
  path('detalleafiliado/<str:pk>/', DetalleAfiliado.as_view(), name='DetalleAfiliado'),
  path('carrera/', ListaCarrera.as_view(), name='ListaCarreras'),
  path('provincia/', ListaProvincia.as_view(), name='ListaProvincias'),
  path('tsangre/', ListaSangre.as_view(), name='ListaTiposSangre'),
  path('ciudad/', ListaCiudad.as_view(), name = 'ListaCiudad'),
  path('alergias/', ListaAlergia.as_view(), name = 'ListaAlergia'),
  path('estcivil/', EstCivil.as_view(), name = 'EstCivilr'),
  path('estudiante/', ListaEstudiantes.as_view(), name='ListaEstudiantes'),
  path('afiliado/', ListaAfiliado.as_view(), name='ListaAfiliado'),
  path('ingresarafiliado/', IngresarAfiliado.as_view(), name='ingresarafiliado'),
  path('ingresarEst/', IngresarEstudiante.as_view(), name="IngresarEst"),
  path('actualizaEst/<str:pk>/', EstudianteDetalle.as_view(), name=" EstudianteDetalle "),
  path('actualizaAfiliado/<str:pk>/', EditaAfiliado.as_view(), name=" EditaAfiliado"),
  path('verEst/<str:pk>/', Destudiantex.as_view(), name=" Destudiantex "),
  path('Registroalergias', ListaRegistroAlergias.as_view(), name = 'ListaRegistroAlergias'),
  path('AlergiasAfiliado/<str:pk>/', AlergiasAfiliado.as_view(), name = 'AlergiasAfiliado'),
  path('listaBajas/', SolicitarBaja.as_view(), name='SolicitarBaja'),
  path('bajaafiliado/<str:pk>/', VerBajaAfiliado.as_view(), name='VerBajaAfiliado'),
  path('motivobaja/', TBajas.as_view(), name = 'TBajas'),
  path('AfiliadodelDia/<str:fecha>/<str:est>/', AfiliadodelDia.as_view(), name = 'AfiliadodelDia'),
  path('BajasdelDia/<str:fecha>/', BajasdelDia.as_view(), name = 'BajasdelDia'),

######################### CONSULTA #######################
  path('ListaConsulta/', ConsultaLista.as_view(), name="ListaConsulta"),
  path('ConsultaGuardar/', ConsultaGuardar.as_view(), name="ConsultaGuardar"),
  path('MedicoLista/', MedicoLista.as_view(), name="MedicoLista"),
  path('MedicoBusca/<str:busca>/', MedicoBusca.as_view(), name="MedicoBusca"),

######################### RECETA #######################
  path('RecetaGuardar/', RecetaGuardar.as_view(), name="RecetaGuardar"),
  path('detalleRecetaBusca/<int:busca>/',DetalleRecetaBusca.as_view(), name="detalleRecetaBusca"),
  path('RecetaLista/', RecetaLista.as_view(), name="RecetaLista"),
  
  path('RecetaListaFecha/<str:busca>/', RecetaListaFecha.as_view(), name="RecetaListaFecha"),
  path('RecetaListaAfiliado/<str:busca>/', RecetaListaAfiliado.as_view(), name="RecetaListaAfiliado"),

  path('RecetasPorEntregar/', RecetasPorEntregar.as_view(), name="RecetasPorEntregar"),
  path('RecetasPorEntregarFecha/<str:busca>/', RecetasPorEntregarFecha.as_view(), name="RecetasPorEntregarFecha"),
  path('RecetasPorEntregarAfiliado/<str:busca>/', RecetasPorEntregarAfiliado.as_view(), name="RecetasPorEntregarAfiliado"),
  
  path('RecetasEntregadas/', RecetasEntregadas.as_view(), name="RecetasEntregadas"),
  path('RecetasEntregadasFecha/<str:busca>/', RecetasEntregadasFecha.as_view(), name="RecetasEntregadasFecha"),
  path('RecetasEntregadasAfiliado/<str:busca>/', RecetasEntregadasAfiliado.as_view(), name="RecetasEntregadasAfiliado"),

  path('RecetasAnuladas/', RecetasAnuladas.as_view(), name="RecetasAnuladas"),
  path('RecetasAnuladasFecha/<str:busca>/', RecetasAnuladasFecha.as_view(), name="RecetasAnuladasFecha"),
  path('RecetasAnuladasAfiliado/<str:busca>/', RecetasAnuladasAfiliado.as_view(), name="RecetasAnuladasAfiliado"),

  path('RecetaEstadoEdita/<int:pk>/', RecetaEstadoEdita.as_view(), name="RecetaEstadoEdita"),
  path('RecetaEstadoEdita2/<int:pk>/', RecetaEstadoEdita2.as_view(), name="RecetaEstadoEdita2"),
#--------- POR MEDICO ---------
  path('RecetaListaMedico/<int:medico>/', RecetaListaMedico.as_view(), name="RecetaListaMedico"),
  path('RecetaListaMedicoFecha/<int:medico>/<str:busca>/', RecetaListaMedicoFecha.as_view(), name="RecetaListaMedicoFecha"),
  path('RecetaListaMedicoAfiliado/<int:medico>/<str:busca>/', RecetaListaMedicoAfiliado.as_view(), name="RecetaListaMedicoAfiliado"),
  path('RecetasPorEntregarMedico/<int:medico>/', RecetasPorEntregarMedico.as_view(), name="RecetasPorEntregarMedico"),
  
#################### MEDICAMENTO - RECETA ##############
  path('ListaMedicamentoRecetaGuardar/',MedicamentoRecetaGuardar.as_view(), name="ListaMedicamentoRecetaGuardar"),
  path('detalleMedicamentoRecetaBusca/<int:busca>/',DetalleMedicamentoRecetaBusca.as_view(), name="detalleMedicamentoRecetaBusca"),  
  path('MedicamentoRecetaBuscaNoEntregado/<int:busca>/',MedicamentoRecetaBuscaNoEntregado.as_view(), name="MedicamentoRecetaBuscaNoEntregado"),  

  path('Medicamento_RecetaEdita/<int:pk>/',Medicamento_RecetaEdita.as_view(), name="Medicamento_RecetaEdita"),

#################### INSUMO - RECETA ##############
  path('InsumoRecetaGuardar/',InsumoRecetaGuardar.as_view(), name="InsumoRecetaGuardar"),
  path('DetalleInsumoRecetaBusca/<int:busca>/',DetalleInsumoRecetaBusca.as_view(), name="DetalleInsumoRecetaBusca"),
  path('InsumoRecetaBuscaNoEntregado/<int:busca>/',InsumoRecetaBuscaNoEntregado.as_view(), name="InsumoRecetaBuscaNoEntregado"),

  path('Insumo_RecetaEdita/<int:pk>/',Insumo_RecetaEdita.as_view(), name="Insumo_RecetaEdita"),

#################### OTRO PRODUCTO #########################
  path('OtroMedicamentoLista/',OtroMedicamentoLista.as_view(), name="OtroMedicamentoLista"),
  path('OtroMedicamentoBusca/<int:busca>/',OtroMedicamentoBusca.as_view(), name="OtroMedicamentoBusca"),


################### BIND CARD #######################
  path('BindCardReceta/',BindCardReceta.as_view(), name="BindCardReceta"),
  path('BindCardRecetaBusca/<str:busca>/',BindCardRecetaBusca.as_view(), name="BindCardRecetaBusca"),
  path('BindCardRecetaBuscaFecha/<str:codigo>/<str:fecha>/',BindCardRecetaBuscaFecha.as_view(), name="BindCardRecetaBuscaFecha"),

  path('BindCardRecetaIBusca/<str:busca>/',BindCardRecetaIBusca.as_view(), name="BindCardRecetaIBusca"),
  path('BindCardRecetaIBuscaFecha/<str:codigo>/<str:fecha>/',BindCardRecetaIBuscaFecha.as_view(), name="BindCardRecetaIBuscaFecha"),

##################### REPORTES ######################
  path('RecetaMesSumMedicamento/<str:mes>/<str:anio>/<str:codigo>/',RecetaMesSumMedicamento.as_view(), name="RecetaMesSumMedicamento"),
  path('RecetaTrimSumMedicamento/<str:mes1>/<str:mes2>/<str:mes3>/<str:anio>/<str:codigo>/',RecetaTrimSumMedicamento.as_view(), name="RecetaTrimSumMedicamento"),
  path('RecetaTrimSumTotalMedicamento/<str:mes1>/<str:mes2>/<str:mes3>/<str:anio>/',RecetaTrimSumTotalMedicamento.as_view(), name="RecetaTrimSumTotalMedicamento"),

  path('RecetaSemSumMedicamento/<str:mes1>/<str:mes2>/<str:mes3>/<str:mes4>/<str:mes5>/<str:mes6>/<str:anio>/<str:codigo>/',RecetaSemSumMedicamento.as_view(), name="RecetaSemSumMedicamento"),
  path('RecetaSemSumTotalMedicamento/<str:mes1>/<str:mes2>/<str:mes3>/<str:mes4>/<str:mes5>/<str:mes6>/<str:anio>/',RecetaSemSumTotalMedicamento.as_view(), name="RecetaSemSumTotalMedicamento"),

  path('RecetaAnualSumMedicamento/<str:anio>/<str:codigo>/',RecetaAnualSumMedicamento.as_view(), name="RecetaAnualSumMedicamento"),
  path('RecetaAnualSumTotalMedicamento/<str:anio>/',RecetaAnualSumTotalMedicamento.as_view(), name="RecetaAnualSumTotalMedicamento"),

  path('RecetaMesSumInsumo/<str:mes>/<str:anio>/<str:codigo>/',RecetaMesSumInsumo.as_view(), name="RecetaMesSumInsumo"),
  path('RecetaTrimSumInsumo/<str:mes1>/<str:mes2>/<str:mes3>/<str:anio>/<str:codigo>/',RecetaTrimSumInsumo.as_view(), name="RecetaTrimSumInsumo"),
  path('RecetaTrimSumTotalInsumo/<str:mes1>/<str:mes2>/<str:mes3>/<str:anio>/',RecetaTrimSumTotalInsumo.as_view(), name="RecetaTrimSumTotalInsumo"),

  path('RecetaSemSumInsumo/<str:mes1>/<str:mes2>/<str:mes3>/<str:mes4>/<str:mes5>/<str:mes6>/<str:anio>/<str:codigo>/',RecetaSemSumInsumo.as_view(), name="RecetaSemSumInsumo"),
  path('RecetaSemSumTotalInsumo/<str:mes1>/<str:mes2>/<str:mes3>/<str:mes4>/<str:mes5>/<str:mes6>/<str:anio>/',RecetaSemSumTotalInsumo.as_view(), name="RecetaSemSumTotalInsumo"),

  path('RecetaAnualSumInsumo/<str:anio>/<str:codigo>/', RecetaAnualSumInsumo.as_view(), name="RecetaAnualSumInsumo"),
  path('RecetaAnualSumTotalInsumo/<str:anio>/',RecetaAnualSumTotalInsumo.as_view(), name="RecetaAnualSumTotalInsumo"),

]
