from django.contrib.auth.models import User
from rest_framework import serializers
from ....almacena.api.v1.serializers import MedicamentoSerializer, InsumoHospitalarioSerializer
from ....pedir.api.v1.serializers import PersonalSerializer2, FarmaceuticaSerializer, Personal_PROMESSerializer
from ....recetar.models import (
  Afiliado,
  Carrera,
  Estudiante,
  Provincia,
  T_Alergia_Medicamento,
  T_Ciudad,
  T_Estado_Civil,
  T_Tipo_Sangre,
  T_Ciudad,
  Registro_aler,
  T_Baja,
  SolicitaBaja,
  Consulta,
  Medico,
  Receta,
  medicamento_receta,
  otro_producto, 
  insumo_receta
)

class SangreSerializer(serializers.ModelSerializer):
  class Meta:
    model = T_Tipo_Sangre
    fields = '__all__'

class CarreraSerializer(serializers.ModelSerializer):
  class Meta:
    model = Carrera
    fields = '__all__'

class ProvinciaSerializer(serializers.ModelSerializer):
  class Meta:
    model = Provincia
    fields = '__all__'

class CiudadSerializer(serializers.ModelSerializer):
  class Meta:
    model = T_Ciudad
    fields = '__all__'


class EstudiantesSerializer(serializers.ModelSerializer):
  class Meta:
    model = Estudiante
    fields = '__all__'

class AlergiasSerializer(serializers.ModelSerializer):
  class Meta:
    model = T_Alergia_Medicamento
    fields = '__all__'

class ECivilSerializer(serializers.ModelSerializer):
  class Meta:
    model = T_Estado_Civil
    fields = '__all__'

class EstudianteSerializer(serializers.ModelSerializer):
  est_civil = ECivilSerializer(read_only = True)
  Carrera = CarreraSerializer(read_only = True)
  class Meta:
    model = Estudiante
    fields = '__all__'

class AfiliadoSerializer(serializers.ModelSerializer):
  ci = EstudianteSerializer(read_only = True)
  ciudad = CiudadSerializer(read_only = True)
  tipo_sangre = SangreSerializer(read_only = True)
  class Meta:
    model = Afiliado
    fields = '__all__'

class DAfiliadoSerializer(serializers.ModelSerializer):
  class Meta:
    model = Afiliado
    fields = '__all__'

class RegistroAlergiasSerializer(serializers.ModelSerializer):
  class Meta:
    model = Registro_aler
    fields = '__all__'

class TBajaSerializer(serializers.ModelSerializer):
  class Meta:
    model = T_Baja
    fields = '__all__'

class SolicitaBajaSerializer(serializers.ModelSerializer):
  class Meta:
    model = SolicitaBaja
    fields = '__all__'

class VerSolicitaBajaSerializer(serializers.ModelSerializer):
  ci = AfiliadoSerializer(read_only = True)
  id_baja = TBajaSerializer(read_only = True)
  class Meta:
      model = SolicitaBaja
      fields = '__all__'

class MedicoSerializer(serializers.ModelSerializer):
  class Meta:
    model = Medico
    fields = '__all__'

class ConsultaSerializer(serializers.ModelSerializer):
  afiliado = AfiliadoSerializer(read_only=True)
  medico = Personal_PROMESSerializer(read_only=True)
  class Meta:
    model = Consulta
    fields = '__all__'

class ConsultaGuardarSerializer(serializers.ModelSerializer):    
  class Meta:
    model = Consulta
    fields = '__all__'

###################### RECETA #############################################
class RecetaGuardarSerializer(serializers.ModelSerializer):
  class Meta:
    model = Receta
    fields = '__all__'
        
class RecetaSerializer(serializers.ModelSerializer):
  consulta=ConsultaSerializer(read_only=True)
  farmaceutica=Personal_PROMESSerializer(read_only=True)
  class Meta:
    model = Receta
    fields = '__all__'

class RecetaEstadoSerializer(serializers.ModelSerializer):
  class Meta:
    model = Receta
    fields = (
      'estado',
      'farmaceutica',
      'fecha_entrega',
      'hora_entrega',
    )

class RecetaEstadoSerializer2(serializers.ModelSerializer):
  class Meta:
    model = Receta
    fields = (
      'estado',
      'observacion',
    )    
##################### MEDICAMENTO - RECETA  #################################

class MedicamentoRecetaSerializer(serializers.ModelSerializer):
  id_receta=RecetaSerializer(read_only=True)
  codigo=MedicamentoSerializer(read_only=True)
  class Meta:
    model = medicamento_receta
    fields = '__all__'

class MedicamentoRecetaSerializer2(serializers.ModelSerializer):
  id_receta=RecetaGuardarSerializer(read_only=True)
  codigo=MedicamentoSerializer(read_only=True)
  class Meta:
    model = medicamento_receta
    fields = '__all__'


class MedicamentoRecetaSerializerGuardar(serializers.ModelSerializer):
  class Meta:
    model = medicamento_receta
    fields = '__all__'

class MedicamentoRecetaSerializer3(serializers.ModelSerializer):
  class Meta:
    model = medicamento_receta
    fields = (
      'lote_salida',
      'fecha_expiracion',
      'entregado'
    )
################### INSUMO - RECETA #########################################
class InsumoRecetaSerializer(serializers.ModelSerializer):
  id_receta=RecetaSerializer(read_only=True)
  codigo= InsumoHospitalarioSerializer(read_only=True)
  class Meta:
    model = insumo_receta
    fields = '__all__'

class InsumoRecetaSerializer2(serializers.ModelSerializer):
  id_receta=RecetaGuardarSerializer(read_only=True)
  codigo= InsumoHospitalarioSerializer(read_only=True)
  class Meta:
    model = insumo_receta
    fields = '__all__'


class InsumoRecetaSerializerGuardar(serializers.ModelSerializer):
  class Meta:
    model = insumo_receta
    fields = '__all__'

class InsumoRecetaSerializer3(serializers.ModelSerializer):
  class Meta:
    model = insumo_receta
    fields = (
      'lote_salida',
      'fecha_expiracion',
      'entregado',
    )
##################### OTRO PRODUCTO #########################################
class OtroProductoSerializer(serializers.ModelSerializer):
  class Meta:
    model = otro_producto
    fields = '__all__'

############################### REPORTES ################################
#----------------------------- Reportes Medicamentos -------------------
class RecetaMedicamentoMesSumSerializer(serializers.ModelSerializer):
  sum = serializers.IntegerField()
  class Meta:
    model = medicamento_receta
    fields = ('codigo_id','sum')

#----------------------------- Reportes Medicamentos aaaaaaa -------------------
class RecetaMedicamentoMesSumSerializer2(serializers.ModelSerializer):
  sum = serializers.IntegerField()
  class Meta:
    model = medicamento_receta
    fields = ('sum')

#----------------------------- Reportes Insumos -------------------
class RecetaInsumoMesSumSerializer(serializers.ModelSerializer):
  sum = serializers.IntegerField()
  class Meta:
    model = insumo_receta
    fields = ('codigo_id','sum')
