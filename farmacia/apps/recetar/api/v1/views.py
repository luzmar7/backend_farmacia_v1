# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import get_object_or_404, render
from rest_framework import status, viewsets
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from django.http import HttpResponse
from django.http import Http404 
# Create your views here.
from rest_framework.views import APIView
from ....recetar.models import (

  Afiliado,
  Carrera,
  Estudiante,
  Provincia,
  T_Ciudad,
  T_Tipo_Sangre,
  T_Alergia_Medicamento,
  Registro_aler,
  T_Estado_Civil,
  SolicitaBaja,
  T_Baja,
  Consulta,
  Medico,
  Receta,
  medicamento_receta,
  otro_producto,
  insumo_receta,
  )
from .serializers import (
  AfiliadoSerializer,
  CarreraSerializer,
  CiudadSerializer,
  DAfiliadoSerializer,
  EstudiantesSerializer,
  EstudianteSerializer,

  ProvinciaSerializer,
  SangreSerializer,
  AlergiasSerializer,
  RegistroAlergiasSerializer,

  ECivilSerializer,
  TBajaSerializer,
  VerSolicitaBajaSerializer,
  SolicitaBajaSerializer,
  ConsultaSerializer,
  ConsultaGuardarSerializer,
  MedicoSerializer,
  RecetaGuardarSerializer,
  RecetaSerializer,

  MedicamentoRecetaSerializerGuardar,
  MedicamentoRecetaSerializer,
  OtroProductoSerializer,
  RecetaEstadoSerializer,
  RecetaEstadoSerializer2,

  MedicamentoRecetaSerializer2,
  MedicamentoRecetaSerializer3,

  InsumoRecetaSerializerGuardar,
  InsumoRecetaSerializer,
  InsumoRecetaSerializer2,
  InsumoRecetaSerializer3,
  RecetaMedicamentoMesSumSerializer,
  RecetaMedicamentoMesSumSerializer2,
  RecetaInsumoMesSumSerializer,
)
from django.db.models import Q
from django.db.models import Avg, Count, Min, Sum

class ListaCarrera(APIView):
  def get(self, request):
    carrera = Carrera.objects.all()[:20]
    data = CarreraSerializer(carrera, many=True).data
    return Response(data)

class ListaProvincia(APIView):
  def get(self, request):
    provincia = Provincia.objects.all()[:20]
    data = ProvinciaSerializer(provincia, many=True).data
    return Response(data)

class ListaSangre(APIView):
  def get(self, request, format = None):
    snippets = T_Tipo_Sangre .objects.all()
    serializer = SangreSerializer (snippets, many = True)
    return Response(serializer.data)
  def post(self, request, format=None):
    serializer = SangreSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ListaEstudiantes(APIView):
  def get(self, request):
    estudiante = Estudiante.objects.all()[:20]
    data = EstudiantesSerializer(estudiante, many = True).data
    return Response(data)

class ListaCiudad(APIView):
  def get(self, request, format = None):
    snippets = T_Ciudad .objects.all()
    serializer = CiudadSerializer (snippets, many = True)
    return Response(serializer.data)
  def post(self, request, format=None):
    serializer = CiudadSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

"datos de Estado Civil"
class EstCivil(APIView):
  def get(self, request, format=None):
    snippets = T_Estado_Civil.objects.all()
    serializer = ECivilSerializer(snippets, many = True)
    return Response(serializer.data)
  def post(self, request, format = None):
    serializer = ECivilSerializer (data = request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status= status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ListaAlergia(APIView):
  def get(self, request, format = None):
    snippets = T_Alergia_Medicamento .objects.all()
    serializer = AlergiasSerializer(snippets, many = True)
    return Response(serializer.data)
  def post(self, request, format=None):
    serializer = AlergiasSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class EditaAfiliado(APIView):
  def get_object(self, pk):
    try:
      return Afiliado.objects.get(codAfiliado=pk)
    except Estudiante.DoesNotExist:
      raise Http404
  def get(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = DAfiliadoSerializer(snippet)
    return Response(serializer.data)
  def put(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = DAfiliadoSerializer(snippet, data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#--------------------AFILIADO ---------------------------
class ListaAfiliadoDetalle(APIView):
  def get(self, request, format = None):
    snippets = Afiliado.objects.all()
    serializer = AfiliadoSerializer (snippets, many = True)
    return Response(serializer.data)

class ListaAfiliado(APIView):
  def get(self, request, format = None):
    snippets = Afiliado.objects.all()
    serializer = DAfiliadoSerializer (snippets, many = True)
    return Response(serializer.data)
  def post(self, request, format=None):
    serializer = DAfiliadoSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class DetalleAfiliado(APIView):
  def get(self, request, pk, format=None):
    snippets = Afiliado.objects.filter(codAfiliado = pk)
    serializer = AfiliadoSerializer(snippets, many = True)
    return Response(serializer.data)

class AfiliadodelDia(APIView):
  def get(self, request,fecha, est,format=None):
    snippets = Afiliado.objects.filter(estado = est, fecha = fecha)
    serializer = AfiliadoSerializer(snippets, many = True)
    return Response(serializer.data)

class IngresarEstudiante(APIView):
  def post(self, request):
    ci = request.data.get("ci")
    apellido_p = request.data.get("apellido_p")
    apellido_m = request.data.get("apellido_m")
    nombres = request.data.get("nombres")
    sexo = request.data.get("sexo")
    fecha_nac = request.data.get("fecha_nac")
    telefono = request.data.get("telefono")
    celular = request.data.get("celular")
    direccion = request.data.get("direccion")
    est_civil = request.data.get("est_civil")
    Carrera = request.data.get("Carrera")
    matricula = request.data.get("matricula")
    data = {'ci': ci, 'nombres': nombres, 'apellido_p': apellido_p, 'apellido_m':apellido_m,'sexo':sexo, 'fecha_nac': fecha_nac, 'telefono': telefono, 'celular': celular, 'direccion': direccion, 'est_civil': est_civil, 'Carrera': Carrera, 'matricula': matricula}
    serializer = EstudiantesSerializer(data=data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class IngresarAfiliado(APIView):
  def post(self, request):
    ci = request.data.get("ci")
    codAfiliado = request.data.get("codAfiliado")
    apellidoEsposo = request.data.get("apellidoEsposo")
    nombre_Referencia= request.data.get("nombre_Referencia")
    telefono_Referencia= request.data.get("telefono_Referencia")
    ciudad= request.data.get("ciudad")
    estado= request.data.get("estado")

    alergia= request.data.get("alergia")
    tipo_sangre= request.data.get("tipo_sangre")
    fecha= request.data.get("fecha")

    data = {'ci': ci, 'codAfiliado': codAfiliado, 'apellidoEsposo': apellidoEsposo, 'nombre_Referencia': nombre_Referencia, 'telefono_Referencia':telefono_Referencia,'ciudad':ciudad, 'estado': estado,'alergia': alergia, 'tipo_sangre': tipo_sangre, 'fecha':fecha }
    serializer = AfiliadoSerializer(data=data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class Destudiantex(APIView):
  def get(self, request, pk, format=None):
    snippets = Estudiante.objects.filter(ci = pk)
    serializer = EstudianteSerializer(snippets, many = True)
    return Response(serializer.data)

class EstudianteDetalle(APIView):
  def get_object(self, pk):
    try:
      return Estudiante.objects.get(ci=pk)
    except Estudiante.DoesNotExist:
      raise Http404
  def get(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = EstudiantesSerializer(snippet)
    return Response(serializer.data)
  def put(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = EstudiantesSerializer(snippet, data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
  """BORRA UN REGISTRO"""
  def delete(self, request, pk, format=None):
    snippet = self.get_object(pk)
    snippet.delete()
    return Response(status=status.HTTP_204_NO_CONTENT)

class RegistroAfiliado(APIView):
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = FORM_DOC.objects.all()
    serializer = FORM_DOCSerializer(snippets, many = True)
    return Response(serializer.data)
  #CREA UN NUEVO REGISTRO
  def post(self, request, format=None):
    serializer = EstudianteAfiSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

"REGISTROS  DE  ALERGIAS"
class ListaRegistroAlergias(APIView):
#Realiza un listado de los datos
  def get(self, request,format=None):
    snippet = Registro_aler.objects.all()
    serializador = RegistroAlergiasSerializer(snippet, many=True)
    return Response(serializador.data)
#Crea un nuevo registro
  def post(self, request, format=None):
    serializer = RegistroAlergiasSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class AlergiasAfiliado(APIView):
  #Realiza un listado de los datos que sean igual a algo WHERE
  def get(self, request, pk,format=None):
    snippet = Registro_aler.objects.filter(afiliado=pk)
    serializador = RegistraAlergiaJoinSerializer(snippet, many=True)
    return Response(serializador.data)

class TBajas(APIView):
  def get(self, request,format=None):
    snippet = T_Baja.objects.all()
    serializador = TBajaSerializer(snippet, many=True)
    return Response(serializador.data)
#Crea un nuevo registro
  def post(self, request, format=None):
    serializer = TBajaSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

  #Baja Detallada DE un Afiliado
class VerBajaAfiliado(APIView):
  def get(self, request, pk,format=None):
    snippet = SolicitaBaja.objects.filter(ci=pk)
    serializador = VerSolicitaBajaSerializer(snippet, many=True)
    return Response(serializador.data)
## obtener y crear un nuevo registro de Baja deafiliado
class SolicitarBaja(APIView):
  def get(self, request,format=None):
    snippet = SolicitaBaja.objects.all()
    serializador = SolicitaBajaSerializer(snippet, many=True)
    return Response(serializador.data)
#Crea un nuevo registro
  def post(self, request, format=None):
    serializer = SolicitaBajaSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class BajasdelDia(APIView):
  def get(self, request,fecha,format=None):
    snippets = SolicitaBaja.objects.filter( fecha = fecha)
    serializer = VerSolicitaBajaSerializer(snippets, many = True)
    return Response(serializer.data)

## obtiene, edita y elimina un registro de baja de afiliado
class VerEditarBaja(APIView):
  def get_object(self, pk):
    try:
      return SolicitaBaja.objects.get(ci=pk)
    except Estudiante.DoesNotExist:
      raise Http404
  def get(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = SolicitaBajaSerializer(snippet)
    return Response(serializer.data)
  def put(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = SolicitaBajaSerializer(snippet, data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
  """BORRA UN REGISTRO"""
  def delete(self, request, pk, format=None):
    snippet = self.get_object(pk)
    snippet.delete()
    return Response(status=status.HTTP_204_NO_CONTENT)

##---------------------CONSULTA----------------------------
class ConsultaLista(APIView):
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Consulta.objects.all()
    serializer = ConsultaSerializer(snippets, many=True)
    return Response(serializer.data)
  #CREA UN NUEVO REGISTRO
  def post(self, request, format=None):
    serializer = ConsultaSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

##--------------------CONSULTA GUARDAR--------
class ConsultaGuardar(APIView):
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Consulta.objects.all()
    serializer = ConsultaGuardarSerializer(snippets, many=True)
    return Response(serializer.data)
  #CREA UN NUEVO REGISTRO
  def post(self, request, format=None):
    serializer = ConsultaGuardarSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

##------------------- MEDICO ---
class MedicoLista(APIView):
  def get(self, request, format=None):
    snippets= Medico.objects.all()
    serializer = MedicoSerializer(snippets, many=True)
    return Response(serializer.data)

class MedicoBusca(APIView):
  def get(self, request, busca, format=None):
    snippets= Medico.objects.all().filter(cedulaIdentidad=busca)
    serializer = MedicoSerializer(snippets, many=True)
    return Response(serializer.data)


####################### RECETA ##################################
#---------------- RECETA GUARDAR ----------------------
class RecetaGuardar(APIView):
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Receta.objects.all()
    serializer = RecetaGuardarSerializer(snippets, many=True)
    return Response(serializer.data)
  #CREA UN NUEVO REGISTRO
  def post(self, request, format=None):
    serializer = RecetaGuardarSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#----------- RECETA BUSCA -------------
class DetalleRecetaBusca(APIView):
  def get(self, request, busca, format=None):
    snippets= Receta.objects.filter(pk=busca) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = RecetaSerializer(snippets, many=True)
    return Response(serializer.data)

#------------ DETALLE RECETA --------------
class RecetaLista(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Receta.objects.all().order_by('-id_receta')
    serializer = RecetaSerializer(snippets, many=True)
    return Response(serializer.data)

#------------ DETALLE RECETA (MEDICO) --------------
class RecetaListaMedico(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, medico, format=None):
    snippets = Receta.objects.all().order_by('-id_receta').filter(consulta__medico__ci=medico)
    serializer = RecetaSerializer(snippets, many=True)
    return Response(serializer.data)

#------------ DETALLE RECETA (FECHA) --------------
class RecetaListaFecha(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets = Receta.objects.all().order_by('-id_receta').filter(fecha_emision=busca)
    serializer = RecetaSerializer(snippets, many=True)
    return Response(serializer.data)

#------------ DETALLE RECETA (FECHA, UNIDAD) --------------
class RecetaListaMedicoFecha(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, medico , busca, format=None):
    snippets = Receta.objects.all().order_by('-id_receta').filter(consulta__medico__ci=medico).filter(fecha_emision=busca)
    serializer = RecetaSerializer(snippets, many=True)
    return Response(serializer.data)

#------------ DETALLE RECETA (AFILIADO) --------------
class RecetaListaAfiliado(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets = Receta.objects.all().order_by('-id_receta').filter(consulta__afiliado__codAfiliado=busca)
    serializer = RecetaSerializer(snippets, many=True)
    return Response(serializer.data)

#------------ DETALLE RECETA (AFILIADO) --------------
class RecetaListaMedicoAfiliado(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, medico, busca, format=None):
    snippets = Receta.objects.all().order_by('-id_receta').filter(consulta__medico__ci=medico).filter(consulta__afiliado__codAfiliado=busca)
    serializer = RecetaSerializer(snippets, many=True)
    return Response(serializer.data)

#------------ DETALLE RECETA (POR ENTREGAR) --------------
class RecetasPorEntregar(APIView):
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Receta.objects.all().order_by('-id_receta').filter(estado ='Por Entregar')
    serializer = RecetaSerializer(snippets, many=True)
    return Response(serializer.data)

#------------ DETALLE RECETA (POR ENTREGAR, POR MEDICO ) --------------
class RecetasPorEntregarMedico(APIView):
  #LISTA TODOS LOS REGISTROS
  def get(self, request, medico, format=None):
    snippets = Receta.objects.all().order_by('-id_receta').filter(estado ='Por Entregar').filter(consulta__medico__ci=medico)
    serializer = RecetaSerializer(snippets, many=True)
    return Response(serializer.data)

#------------ DETALLE RECETA (POR ENTREGAR, FECHA) --------------
class RecetasPorEntregarFecha(APIView):
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets = Receta.objects.all().order_by('-id_receta').filter(estado ='Por Entregar').filter(fecha_emision=busca)
    serializer = RecetaSerializer(snippets, many=True)
    return Response(serializer.data)

#------------ DETALLE RECETA (POR ENTREGAR, AFILIADO) --------------
class RecetasPorEntregarAfiliado(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets = Receta.objects.all().order_by('-id_receta').filter(estado ='Por Entregar').filter(consulta__afiliado__codAfiliado=busca)
    serializer = RecetaSerializer(snippets, many=True)
    return Response(serializer.data)

#------------ DETALLE RECETA (ENTREGADAS) --------------
class RecetasEntregadas(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Receta.objects.all().order_by('-id_receta').filter(estado ='Entregado')
    serializer = RecetaSerializer(snippets, many=True)
    return Response(serializer.data)

#------------ DETALLE RECETA (ENTREGADAS, FECHA) --------------
class RecetasEntregadasFecha(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets = Receta.objects.all().order_by('-id_receta').filter(estado ='Entregado').filter(fecha_entrega=busca)
    serializer = RecetaSerializer(snippets, many=True)
    return Response(serializer.data)

#------------ DETALLE RECETA (POR ENTREGAR, AFILIADO) --------------
class RecetasEntregadasAfiliado(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets = Receta.objects.all().order_by('-id_receta').filter(estado ='Entregado').filter(consulta__afiliado__codAfiliado=busca)
    serializer = RecetaSerializer(snippets, many=True)
    return Response(serializer.data)





#------------ DETALLE RECETA (ANULADAS) --------------
class RecetasAnuladas(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Receta.objects.all().order_by('-id_receta').filter(estado ='Anulada')
    serializer = RecetaSerializer(snippets, many=True)
    return Response(serializer.data)

#------------ DETALLE RECETA (ENTREGADAS, FECHA) --------------
class RecetasAnuladasFecha(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets = Receta.objects.all().order_by('-id_receta').filter(estado ='Anulada').filter(fecha_emision=busca)
    serializer = RecetaSerializer(snippets, many=True)
    return Response(serializer.data)

#------------ DETALLE RECETA (POR ENTREGAR, AFILIADO) --------------
class RecetasAnuladasAfiliado(APIView):    
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets = Receta.objects.all().order_by('-id_receta').filter(estado ='Anulada').filter(consulta__afiliado__codAfiliado=busca)
    serializer = RecetaSerializer(snippets, many=True)
    return Response(serializer.data)




#---------- ACTUALIZA ESTADO DE RECETA --------------
class RecetaEstadoEdita(APIView):
  def get_object(self, pk):
    try:
      return Receta.objects.get(id_receta=pk)
    except Receta.DoesNotExist:
      raise Http404
  def get(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = RecetaEstadoSerializer(snippet)
    return Response(serializer.data)

  def put(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = RecetaEstadoSerializer(snippet, data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#---------- ACTUALIZA ESTADO DE RECETA --------------
class RecetaEstadoEdita2(APIView):
  def get_object(self, pk):
    try:
      return Receta.objects.get(id_receta=pk)
    except Receta.DoesNotExist:
      raise Http404
  def get(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = RecetaEstadoSerializer2(snippet)
    return Response(serializer.data)

  def put(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = RecetaEstadoSerializer2(snippet, data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

######################### MEDICAMENTO RECETA #########################################
#---------- MEDICAMENTO RECETA --------------
class MedicamentoRecetaGuardar(APIView):
  def get(self, request, format=None):
    snippets = medicamento_receta.objects.all()
    serializer = MedicamentoRecetaSerializerGuardar(snippets, many=True)
    return Response(serializer.data)
  
  def post(self, request, format=None):
    serializer = MedicamentoRecetaSerializerGuardar(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



#--------------------- BUSCA MEDICAMENTO-RECETA -------------------        
class DetalleMedicamentoRecetaBusca(APIView):
  def get(self, request, busca, format=None):
    snippets= medicamento_receta.objects.filter(id_receta=busca) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = MedicamentoRecetaSerializer(snippets, many=True)
    return Response(serializer.data)

#--------------------- BUSCA MEDICAMENTO-RECETA (No entregado)-------------------        
class MedicamentoRecetaBuscaNoEntregado(APIView):
  def get(self, request, busca, format=None):
    snippets= medicamento_receta.objects.filter(id_receta=busca).filter(entregado=False) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = MedicamentoRecetaSerializer(snippets, many=True)
    return Response(serializer.data)

###--- ACTUALIZA MEDICAMENTO-RECETA------------------------------
class Medicamento_RecetaEdita(APIView):
  def get_object(self, pk):
    try:
      return medicamento_receta.objects.get(id=pk)
    except medicamento_receta.DoesNotExist:
      raise Http404


  def get(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = MedicamentoRecetaSerializer3(snippet)
    return Response(serializer.data)

  def put(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = MedicamentoRecetaSerializer3(snippet, data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


######################### INSUMO RECETA #########################################
#---------- INSUMO RECETA --------------
class InsumoRecetaGuardar(APIView):
  def get(self, request, format=None):
    snippets = insumo_receta.objects.all()
    serializer = InsumoRecetaSerializerGuardar(snippets, many=True)
    return Response(serializer.data)
  
  def post(self, request, format=None):
    serializer = InsumoRecetaSerializerGuardar(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#--------------------- BUSCA INSUMO-RECETA -------------------        
class DetalleInsumoRecetaBusca(APIView):
  def get(self, request, busca, format=None):
    snippets= insumo_receta.objects.filter(id_receta=busca) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = InsumoRecetaSerializer(snippets, many=True)
    return Response(serializer.data)

#--------------------- BUSCA INSUMO-RECETA (No entregado)------------------- 
      
class InsumoRecetaBuscaNoEntregado(APIView):
  def get(self, request, busca, format=None):
    snippets= insumo_receta.objects.filter(id_receta=busca).filter(entregado=False) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = InsumoRecetaSerializer(snippets, many=True)
    return Response(serializer.data)

###--- ACTUALIZA INSUMO-RECETA------------------------------
class Insumo_RecetaEdita(APIView):
  def get_object(self, pk):
    try:
      return insumo_receta.objects.get(id=pk)
    except insumo_receta.DoesNotExist:
      raise Http404

  def get(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = InsumoRecetaSerializer3(snippet)
    return Response(serializer.data)

  def put(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = InsumoRecetaSerializer3(snippet, data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

##################### OTRO PRODUCTO RECETAR ##########################################
#--------------- OTRO MEDICAMENTO------------------
class OtroMedicamentoLista(APIView):  
  #LISTA TODOS LOS REGISTROS MEDICAMENTO-RECETA
  def get(self, request, format=None):
    snippets = otro_producto.objects.all()
    serializer = OtroProductoSerializer(snippets, many=True)
    return Response(serializer.data)

  #CREA UN NUEVO REGISTRO OTRO
  def post(self, request, format=None):
    serializer = OtroProductoSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
      
#--------------- OTRO MEDICAMENTO BUSCA -------------------
class OtroMedicamentoBusca(APIView):  
  #BUSCA ID-RECETA
  def get(self, request,busca, format=None):
    snippets = otro_producto.objects.all().filter(id_receta=busca)
    serializer = OtroProductoSerializer(snippets, many=True)
    return Response(serializer.data)

#################### BIND CARD RECETA ##################################
#--------------------- BINDCARD RECETA EN FARMACIA-------------------        
class BindCardReceta(APIView):
  def get(self, request, format=None):
    snippets= medicamento_receta.objects.filter(id_receta__estado="Entregado") #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = MedicamentoRecetaSerializer2(snippets, many=True)
    return Response(serializer.data)

class BindCardRecetaBusca(APIView):
  def get(self, request,busca, format=None):
    snippets= medicamento_receta.objects.filter(id_receta__estado="Entregado").filter(codigo=busca).order_by('-id') #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = MedicamentoRecetaSerializer2(snippets, many=True)
    return Response(serializer.data)

class BindCardRecetaBuscaFecha(APIView):
  def get(self, request, codigo, fecha, format=None):
    snippets= medicamento_receta.objects.filter(id_receta__estado="Entregado").filter(codigo=codigo).filter(id_receta__fecha_entrega=fecha).order_by('-id') #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = MedicamentoRecetaSerializer2(snippets, many=True)
    return Response(serializer.data)

class BindCardRecetaIBusca(APIView):
  def get(self, request,busca, format=None):
    snippets= insumo_receta.objects.filter(id_receta__estado="Entregado").filter(codigo=busca).order_by('-id')  #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = InsumoRecetaSerializer2(snippets, many=True)
    return Response(serializer.data)

class BindCardRecetaIBuscaFecha(APIView):
  def get(self, request, codigo, fecha, format=None):
    snippets= insumo_receta.objects.filter(id_receta__estado="Entregado").filter(codigo=codigo).filter(id_receta__fecha_entrega=fecha).order_by('-id')  #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = InsumoRecetaSerializer2(snippets, many=True)
    return Response(serializer.data)

###################### REPORTES ###########################
########---------------------- REPORTES INSUMOS ---------------------##############

#----------------------- RECETAS SUM POR CODIGO SALIDAS MEDICAMENTOS---------------
class RecetaMesSumMedicamento(APIView):
  def get(self, request, codigo, mes, anio, format=None):
    snippets= medicamento_receta.objects.values('codigo_id').filter(id_receta__fecha_entrega__month=mes).filter(id_receta__fecha_entrega__year=anio).filter(codigo=codigo).annotate(sum=Sum('cantidad'))
    serializer = RecetaMedicamentoMesSumSerializer(snippets, many=True)  
    return Response(serializer.data)

#----------------------- RECETAS SUM TRIMESTRAL SALIDAS MEDICAMENTOS---------------
class RecetaTrimSumMedicamento(APIView):
  def get(self, request, codigo, mes1, mes2, mes3, anio, format=None):
    snippets= medicamento_receta.objects.values('codigo_id').filter(Q(id_receta__fecha_entrega__month=mes1) | Q(id_receta__fecha_entrega__month=mes2) | Q(id_receta__fecha_entrega__month=mes3)).filter(id_receta__fecha_entrega__year=anio).filter(codigo=codigo).annotate(sum=Sum('cantidad'))
    serializer = RecetaMedicamentoMesSumSerializer(snippets, many=True)   
    return Response(serializer.data)
#----------------------- RECETAS SUM TRIMESTRAL TOTAL SALIDAS MEDICAMENTOS---------------
class RecetaTrimSumTotalMedicamento(APIView):
  def get(self, request, mes1, mes2, mes3, anio, format=None):
    snippets= medicamento_receta.objects.all().filter(Q(id_receta__fecha_entrega__month=mes1) | Q(id_receta__fecha_entrega__month=mes2) | Q(id_receta__fecha_entrega__month=mes3)).filter(id_receta__fecha_entrega__year=anio).filter(codigo__producto_farmaceutico__habilitado=True).aggregate(sum=Sum('cantidad')) 
    return Response(snippets)

class RecetaSemSumMedicamento(APIView):
  def get(self, request, codigo, mes1, mes2, mes3, mes4, mes5, mes6, anio, format=None):
    snippets= medicamento_receta.objects.values('codigo_id').filter(Q(id_receta__fecha_entrega__month=mes1) | Q(id_receta__fecha_entrega__month=mes2) | Q(id_receta__fecha_entrega__month=mes3) | Q(id_receta__fecha_entrega__month=mes4) | Q(id_receta__fecha_entrega__month=mes5) | Q(id_receta__fecha_entrega__month=mes6)).filter(id_receta__fecha_entrega__year=anio).filter(codigo=codigo).annotate(sum=Sum('cantidad'))
    serializer = RecetaMedicamentoMesSumSerializer(snippets, many=True)   
    return Response(serializer.data)

# #----------------------- RECETAS SUM TRIMESTRAL SALIDAS---------------
# class RecetaTrimSumTotalMedicamentoA(APIView):
#   def get(self, request, mes1, mes2, mes3, anio, format=None):
#     snippets= medicamento_receta.objects.filter(Q(id_receta__fecha_entrega__month=mes1) | Q(id_receta__fecha_entrega__month=mes2) | Q(id_receta__fecha_entrega__month=mes3)).filter(id_receta__fecha_entrega__year=anio).annotate(sum=Avg(Sum('cantidad')))
#     serializer = RecetaMedicamentoMesSumSerializer(snippets, many=True)   
#     return Response(serializer.data)
 
 #----------------------- RECETAS SUM TRIMESTRAL SALIDAS---------------
class RecetaSemSumTotalMedicamento(APIView):
  def get(self, request, mes1, mes2, mes3, mes4, mes5, mes6, anio, format=None):
    snippets= medicamento_receta.objects.all().filter(Q(id_receta__fecha_entrega__month=mes1) | Q(id_receta__fecha_entrega__month=mes2) | Q(id_receta__fecha_entrega__month=mes3) | Q(id_receta__fecha_entrega__month=mes4) | Q(id_receta__fecha_entrega__month=mes5) | Q(id_receta__fecha_entrega__month=mes6)).filter(id_receta__fecha_entrega__year=anio).filter(codigo__producto_farmaceutico__habilitado=True).aggregate(sum=Sum('cantidad')) 
    return Response(snippets)

class RecetaAnualSumMedicamento(APIView):
  def get(self, request, codigo, anio, format=None):
    snippets= medicamento_receta.objects.values('codigo_id').filter(id_receta__estado="Entregado").filter(id_receta__fecha_entrega__year=anio).filter(codigo=codigo).annotate(sum=Sum('cantidad'))
    serializer = RecetaMedicamentoMesSumSerializer(snippets, many=True)   
    return Response(serializer.data)

class RecetaAnualSumTotalMedicamento(APIView):
  def get(self, request, anio, format=None):
    snippets= medicamento_receta.objects.all().filter(id_receta__estado="Entregado").filter(id_receta__fecha_entrega__year=anio).filter(codigo__producto_farmaceutico__habilitado=True).aggregate(sum=Sum('cantidad')) 
    return Response(snippets)

########---------------------- REPORTES INSUMOS ---------------------##############
#----------------------- RECETAS SUM POR CODIGO SALIDAS INSUMOS---------------
class RecetaMesSumInsumo(APIView):
  def get(self, request, codigo, mes, anio, format=None):
    snippets= insumo_receta.objects.values('codigo_id').filter(id_receta__fecha_entrega__month=mes).filter(id_receta__fecha_entrega__year=anio).filter(codigo=codigo).annotate(sum=Sum('cantidad'))
    serializer = RecetaInsumoMesSumSerializer(snippets, many=True)  
    return Response(serializer.data)

#----------------------- RECETAS SUM TRIMESTRAL SALIDAS INSUMOS---------------
class RecetaTrimSumInsumo(APIView):
  def get(self, request, codigo, mes1, mes2, mes3, anio, format=None):
    snippets= insumo_receta.objects.values('codigo_id').filter(Q(id_receta__fecha_entrega__month=mes1) | Q(id_receta__fecha_entrega__month=mes2) | Q(id_receta__fecha_entrega__month=mes3)).filter(id_receta__fecha_entrega__year=anio).filter(codigo=codigo).annotate(sum=Sum('cantidad'))
    serializer = RecetaInsumoMesSumSerializer(snippets, many=True)   
    return Response(serializer.data)

#----------------------- RECETAS SUM TRIMESTRAL TOTAL SALIDAS INSUMOS---------------
class RecetaTrimSumTotalInsumo(APIView):
  def get(self, request, mes1, mes2, mes3, anio, format=None):
    snippets= insumo_receta.objects.all().filter(Q(id_receta__fecha_entrega__month=mes1) | Q(id_receta__fecha_entrega__month=mes2) | Q(id_receta__fecha_entrega__month=mes3)).filter(id_receta__fecha_entrega__year=anio).filter(codigo__producto_farmaceutico__habilitado=True).aggregate(sum=Sum('cantidad')) 
    return Response(snippets)

#----------------------- RECETAS SUM SEMESTRAL SALIDAS INSUMOS---------------
class RecetaSemSumInsumo(APIView):
  def get(self, request, codigo, mes1, mes2, mes3, mes4, mes5, mes6, anio, format=None):
    snippets= insumo_receta.objects.values('codigo_id').filter(Q(id_receta__fecha_entrega__month=mes1) | Q(id_receta__fecha_entrega__month=mes2) | Q(id_receta__fecha_entrega__month=mes3) | Q(id_receta__fecha_entrega__month=mes4) | Q(id_receta__fecha_entrega__month=mes5) | Q(id_receta__fecha_entrega__month=mes6)).filter(id_receta__fecha_entrega__year=anio).filter(codigo=codigo).annotate(sum=Sum('cantidad'))
    serializer = RecetaInsumoMesSumSerializer(snippets, many=True)   
    return Response(serializer.data)

#----------------------- RECETAS SUM SEMESTRAL TOTAL SALIDAS INSUMOS---------------
class RecetaSemSumTotalInsumo(APIView):
  def get(self, request, mes1, mes2, mes3, mes4, mes5, mes6, anio, format=None):
    snippets= insumo_receta.objects.all().filter(Q(id_receta__fecha_entrega__month=mes1) | Q(id_receta__fecha_entrega__month=mes2) | Q(id_receta__fecha_entrega__month=mes3) | Q(id_receta__fecha_entrega__month=mes4) | Q(id_receta__fecha_entrega__month=mes5) | Q(id_receta__fecha_entrega__month=mes6)).filter(id_receta__fecha_entrega__year=anio).filter(codigo__producto_farmaceutico__habilitado=True).aggregate(sum=Sum('cantidad')) 
    return Response(snippets)

class RecetaAnualSumInsumo(APIView):
  def get(self, request, codigo, anio, format=None):
    snippets= insumo_receta.objects.values('codigo_id').filter(id_receta__estado="Entregado").filter(id_receta__fecha_entrega__year=anio).filter(codigo=codigo).annotate(sum=Sum('cantidad'))
    serializer = RecetaInsumoMesSumSerializer(snippets, many=True)   
    return Response(serializer.data)

class RecetaAnualSumTotalInsumo(APIView):
  def get(self, request, anio, format=None):
    snippets= insumo_receta.objects.all().filter(id_receta__estado="Entregado").filter(id_receta__fecha_entrega__year=anio).filter(codigo__producto_farmaceutico__habilitado=True).aggregate(sum=Sum('cantidad')) 
    return Response(snippets)