from django.contrib import admin

from apps.recetar.models import (
  Estudiante, 
  T_Estado_Civil, 
  Afiliado, 
  T_Localidad,
  T_Alergia_Medicamento,
  T_Ciudad, 
  T_Baja, 
  SolicitaBaja, 
  T_Tipo_Sangre, 
  Provincia,
  Carrera, 
  Registro_aler,
  Medico, 
  Consulta, 
  Receta, 
 
  medicamento_receta, 
  otro_producto,
  insumo_receta,
)

@admin.register(Estudiante)
class EstudianteAdmin(admin.ModelAdmin):
  list_display = ["ci", "nombres", "primer_apellido", "segundo_apellido","sexo","fecha_nac","telefono","celular","direccion","est_civil","Carrera", "matricula"]

@admin.register(Afiliado)
class AfiliadoAdmin(admin.ModelAdmin):
  list_display = ["ci", "codAfiliado", "apellidoEsposo", "nombre_Referencia","telefono_Referencia","ciudad","estado","tipo_sangre","fecha"]

@admin.register(Medico)
class MedicoAdmin(admin.ModelAdmin):
  list_display = ["cedulaIdentidad", "apellidoPaterno", "apellidoMaterno", "nombres","fechaNacimiento","especialidad","sexo"]

@admin.register(Consulta)
class ConsultaAdmin(admin.ModelAdmin):
  list_display = ["id_consulta", "afiliado", "medico"]

@admin.register(Receta)
class RecetaAdmin(admin.ModelAdmin):
  list_display = ["id_receta", "fecha_emision", "fecha_entrega", "hora_emision", "hora_entrega" ,"consulta", "farmaceutica", "estado", "observacion"]

@admin.register(medicamento_receta)
class medicamento_recetaAdmin(admin.ModelAdmin):
  list_display = ["id", "id_receta", "codigo", "cantidad","lote_salida", "fecha_expiracion", "prescripcion", "entregado"]
  

@admin.register(insumo_receta)
class insumo_recetaAdmin(admin.ModelAdmin):
  list_display = ["id", "id_receta", "codigo", "cantidad","lote_salida", "fecha_expiracion", "prescripcion", "entregado"]

@admin.register(otro_producto)
class otro_productoAdmin(admin.ModelAdmin):
  list_display = ["id_receta", "nombre", "forma_farmaceutica", "cantidad", "prescripcion"]

@admin.register(T_Estado_Civil)
class estado_civilAdmin(admin.ModelAdmin):
  list_display = ["id_estado", "estado"]

@admin.register(Carrera)
class carreraAdmin(admin.ModelAdmin):
  list_display = ["id_carrera", "carrera"]

@admin.register(T_Ciudad)
class t_ciudadAdmin(admin.ModelAdmin):
  list_display = ["id_ciudad", "des_ciudad"]

@admin.register(T_Tipo_Sangre)
class t_tipo_sangre(admin.ModelAdmin):
  list_display = ["id_Sangre", "sangre"]