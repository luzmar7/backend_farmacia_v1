from django.db import models
import datetime
from ..almacena.models import Medicamento, Insumo_hospitalario
from ..pedir.models import Personal, Farmaceutica, Personal_PROMES

class Provincia(models.Model):
  id_prov= models.AutoField(primary_key = True)
  dprovincia = models.CharField(max_length = 30)
  def __str__(self):
    return self.dprovincia
  class Meta:
    verbose_name_plural = ("Provincia")

class Carrera(models.Model):
  id_carrera = models.AutoField(primary_key  = True)
  carrera  = models.CharField(max_length = 30)
  def __str__(self):
    return self.carrera
  class Meta:
    verbose_name_plural = ("Carrera")

class T_Estado_Civil(models.Model):
  id_estado = models.AutoField(primary_key=True)
  estado = models.CharField(max_length = 15)
  def __str__(self):
    return self.estado
  class Meta:
    verbose_name_plural = ("T_Estado_Civil")

class Estudiante (models.Model):
  Sexo= (('F','Femenino'), ('M','Masculino'))
  ci = models.CharField(primary_key= True, max_length = 15, )
  nombres = models.CharField(max_length = 30)
  primer_apellido = models.CharField(max_length = 30)
  segundo_apellido = models.CharField(max_length = 30)
  sexo = models.CharField(max_length = 1, choices = Sexo)
  fecha_nac = models.DateField()
  telefono = models.IntegerField(null = True)
  celular = models.IntegerField()
  direccion = models.CharField(max_length = 50)
  email = models.CharField(max_length = 25, default = '')
  est_civil = models.ForeignKey(T_Estado_Civil, on_delete = models.CASCADE)
  Carrera = models.ForeignKey(Carrera, on_delete= models.CASCADE )
  matricula = models.PositiveIntegerField()
  def __str__(self):
    return str(self.ci)
  def __unicode__(self):
    return self.ci
  class Meta:
    verbose_name_plural = ("Estudiantes")

class T_Ciudad(models.Model):
  id_ciudad= models.AutoField(primary_key = True)
  des_ciudad = models.CharField(max_length=30)
  def __str__(self):
    return self.des_ciudad
  class Meta:
    verbose_name_plural = ("T_Ciudad")

class T_Alergia_Medicamento(models.Model):
  id_alergia = models.AutoField(primary_key = True)
  alergia_med = models.CharField(max_length=30)
  def __str__(self):
    return self.alergia_med
  class Meta:
    verbose_name_plural = ("T_Alergia_Medicamento")

class T_Tipo_Sangre(models.Model):
  id_Sangre = models.AutoField(primary_key = True)
  sangre = models.CharField(max_length = 10)
  def __str__(self):
    return self.sangre
  class Meta:
    verbose_name_plural = ("T_Tipo_Sangre")

class T_Respaldo(models.Model):
  id_respaldo = models.AutoField(primary_key = True)
  respaldo = models.CharField(max_length = 20)
  def __str__(self):
    return self.respaldo
  class Meta:
    verbose_name_plural = ("Respaldos")

class T_Parentesco(models.Model):
  id_parentesco = models.AutoField(primary_key = True)
  parentesco = models.CharField(max_length = 20)
  def __str__(self):
    return self.parentesco
  class Meta:
    verbose_name_plural = ("Parentesco")

class Afiliado(models.Model):
  ESTADO = (
    ('Activo','Activo'),
    ('Inactivo','Inactivo'),
    ('Espera','Espera')
  )
  ci = models.OneToOneField(
    Estudiante,
    verbose_name ="Carnet identidad",
    on_delete = models.CASCADE, 
    unique = True
  )
  codAfiliado = models.CharField(
    max_length = 20, 
    primary_key = True,
    verbose_name="Codigo Afiliado" 
  )
  apellidoEsposo = models.CharField(
    max_length = 20, 
    blank = True,
    verbose_name="Apellido Esposo"
  )
  nombre_Referencia = models.CharField(
    max_length = 50,
    verbose_name="Nombre Referencia"
  )
  telefono_Referencia = models.PositiveIntegerField(
    verbose_name="Telefono de referencia"
  )
  parentesco = models.ForeignKey(T_Parentesco, blank = True, on_delete = models.SET_NULL, null = True)
  ciudad = models.ForeignKey(
    T_Ciudad, 
    on_delete = True,
    verbose_name="Ciudad"
  )
  estado = models.CharField(
    max_length = 10, 
    choices = ESTADO, 
    default = 'Inactivo',
    verbose_name="Estado"
  )
  provnac= models.ForeignKey(Provincia, on_delete = models.SET_NULL, blank = True, null = True)
  foto = models.ImageField(upload_to = 'afiliados', blank = True, null = True)  
  tipo_sangre = models.ForeignKey(
    T_Tipo_Sangre, 
    on_delete = models.CASCADE,
    verbose_name="Tipo Sangre"
  )
  respaldo = models.ForeignKey(T_Respaldo, on_delete = models.SET_NULL, blank = True, null = True)
  fecha = models.DateField(
    verbose_name="Fecha"
  )
  gestion = models.IntegerField(default = 2019)

  class Meta:
    verbose_name = ("Afiliado")
    verbose_name_plural = ("Afiliados")
  def __str__(self):
      return self.codAfiliado +" - " + self.ci.nombres



class Registro_aler(models.Model):
  afiliado = models.ForeignKey(Afiliado, on_delete = models.CASCADE)
  alergia = models.ForeignKey(T_Alergia_Medicamento, on_delete = models.CASCADE)
  personal = models.IntegerField()
  class Meta:
    verbose_name_plural = ("Registro_alergias")

class T_Localidad(models.Model):
  id_localidad = models.AutoField(primary_key = True)
  des_localidad = models.CharField(max_length=30)
  def __int__(self):
    return self.id_localidad
  class Meta:
    verbose_name_plural = ("T_Localidad")

class T_Baja(models.Model):
  id_baja = models.AutoField(primary_key = True)
  baja = models.CharField(max_length = 30)
  def __str__(self):
    return self.baja
  class Meta:
    verbose_name_plural = ("T_Baja")

class SolicitaBaja(models.Model):
  ci = models.ForeignKey(Afiliado, on_delete=models.CASCADE)
  id_baja = models.ForeignKey(T_Baja,on_delete=models.CASCADE)
  observacion = models.CharField(max_length = 30,blank = True )
  fecha = models.DateField()
  class Meta:
    verbose_name_plural = ("SolicitudBaja")


class Medico(models.Model):
  SEXO = (
      ('F','Femenino'),
      ('M','Masculino')
  )
  cedulaIdentidad = models.CharField(
    max_length=15, 
    primary_key=True,
    verbose_name="Celula Identidad"
  )
  apellidoPaterno = models.CharField(
    max_length=30,
    verbose_name="Apellido Paterno"
  )
  apellidoMaterno = models.CharField(
    max_length=30,
    verbose_name="Apellido Materno"
  )
  nombres = models.CharField(
    max_length=60,
    verbose_name="Nombres"
  )
  fechaNacimiento = models.DateField(
    verbose_name="Fecha Nacimiento"
  )
  especialidad = models.CharField(
    max_length=40,
    verbose_name="Epeecialidad"
  )
  sexo = models.CharField(
    max_length=1, 
    choices=SEXO,
    verbose_name="Sexo"
  ) 
  class Meta:
    verbose_name = ("Medico")
    verbose_name_plural = ("Medicos")
  def __str__(self):
      return self.cedulaIdentidad + " - "+self.nombres

class Consulta(models.Model):
  id_consulta = models.AutoField(
    primary_key=True,
    verbose_name="Id Consulta"
  )
  afiliado = models.ForeignKey(
    Afiliado,
    on_delete=models.CASCADE,
    verbose_name="Afiliado",
  )
  medico = models.ForeignKey(
    Personal,
    on_delete=models.CASCADE,
    verbose_name="Medico"
  )
  class Meta:
    verbose_name = ("Consulta")
    verbose_name_plural = ("Consultas")
  def __int__(self):
    return self.id_consulta 

class Receta(models.Model):
  ESTADO = (
    ('Por Entregar','Por Entregar'),
    ('Entregado','Entregado'),
    ('No Entregado','No Entregado'),
    ('Anulada','Anulada')
  )
  id_receta = models.AutoField(
    primary_key=True,
    verbose_name="Id Receta"
  )
  fecha_emision= models.DateField(
    default=datetime.date.today,
    verbose_name="Fecha de Emision",
  )
  fecha_entrega = models.DateField(
    verbose_name="Fecha de Entrega",
    null=True,
    blank = True,
  )
  hora_emision = models.TimeField(
    verbose_name= "Hora Emision",
  )
  hora_entrega = models.TimeField(
    verbose_name="Hora Entrega",
    null=True, 
    blank=True
  )
  consulta = models.ForeignKey(
    Consulta,
    on_delete=models.CASCADE,
    verbose_name="Consulta"
  )
  farmaceutica = models.ForeignKey(
    Personal,
    on_delete=models.CASCADE,
    verbose_name="entregado por",   
  )
  estado = models.CharField(
    max_length = 30, 
    choices = ESTADO, 
    default = 'Por Entregar',
    verbose_name="Estado"
  )
  observacion = models.CharField(
    verbose_name="observacion",
    max_length=50,
    null=True,
    blank = True,
  )

  class Meta:
    verbose_name = ("Receta")
    verbose_name_plural = ("Recetas")
  def __int__(self):
    return self.id_receta

class medicamento_receta(models.Model):
  id_receta = models.ForeignKey(
    Receta,
    on_delete=models.CASCADE,
    verbose_name = "Id Receta"
  )
  codigo = models.ForeignKey(
    Medicamento,
    on_delete=models.CASCADE,
    verbose_name="Codigo"
  )
  cantidad = models.PositiveIntegerField(
    verbose_name="Cantidad"
  )
  lote_salida = models.CharField(
    verbose_name="lote salida",
    max_length=20,
    null=True,
    blank = True,
  ) 
  fecha_expiracion = models.DateField(
    verbose_name="Fecha de expiración",
    null=True,
    blank = True,
  )
  prescripcion = models.CharField(
    max_length=100,
    verbose_name="Prescripcion"
  )
  entregado=models.BooleanField(
    default=False,
    verbose_name="Entregado"
  )
  class Meta:
    verbose_name = ("Medicamento Receta")
    verbose_name_plural = ("Medicamentos Recetas")
  def __int__(self):
    return self.id_receta

class insumo_receta(models.Model):
  id_receta = models.ForeignKey(
    Receta,
    on_delete=models.CASCADE,
    verbose_name = "Id Receta"
  )
  codigo = models.ForeignKey(
    Insumo_hospitalario,
    on_delete=models.CASCADE,
    verbose_name="Codigo"
  )
  cantidad = models.PositiveIntegerField(
    verbose_name="Cantidad"
  )
  lote_salida = models.CharField(
    verbose_name="lote salida",
    max_length=20,
    null=True,
    blank = True,
  )
  fecha_expiracion = models.DateField(
    verbose_name="Fecha de expiración",
    null=True,
    blank = True,
  ) 
  prescripcion = models.CharField(
    max_length=100,
    verbose_name="Prescripcion"
  )
  entregado=models.BooleanField(
    default=False,
    verbose_name="Entregado"
  )
  class Meta:
    verbose_name = ("Insumo Receta")
    verbose_name_plural = ("Insumos Recetas")
  def __int__(self):
    return self.id_receta

class otro_producto(models.Model):
  id_receta = models.ForeignKey(
    Receta,
    on_delete=models.CASCADE,
    verbose_name="Id Receta"
  )
  nombre = models.CharField(
    max_length=50,
    verbose_name="Nombre del Producto"
  )
  forma_farmaceutica = models.CharField(
    max_length=50,
    verbose_name="Forma Farmaceutica"
  )
  cantidad = models.PositiveIntegerField()
  prescripcion = models.CharField(
    max_length=100,
    verbose_name="prescripcion"
  )
  class Meta:
    verbose_name_plural = ("otro producto")
    verbose_name_plural = ("otros productos")

  def __int__(self):
    return self.id_receta



