from django.urls import path
from .views import current_user, UserList,ListaUsuarios, ListaUsuarios2

urlpatterns = [
  path('current_user/', current_user),
  path('users/', UserList.as_view()),
  path('ListaUsuarios/<str:usr>/', ListaUsuarios.as_view(),name="ListaUsuarios"),
  path('ListaUsuarios2/', ListaUsuarios2.as_view(),name="ListaUsuarios2"),
]
