from django.contrib import admin
from django.urls import path, include

app_name = 'almacena'
urlpatterns = [
    path('v1/',include("apps.almacena.api.v1.urls", namespace="v1")),
]
