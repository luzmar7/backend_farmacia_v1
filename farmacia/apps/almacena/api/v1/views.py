from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import HttpResponse
from django.http import Http404  
from django.shortcuts import get_object_or_404
from rest_framework import status
from ....almacena.models import (
  Producto_farmaceutico,
  T_forma_farmaceutica,
  T_presentacion,
  Medicamento,
  Reactivo,
  Insumo_hospitalario,
  Lote_producto,
  Almacen,
  Esta_Ubicado,
  Lote,
  T_unidad,
  Deposito
)
from .serializers import(
  ProductoSerializer,
  IngresaSerializer,
  FormaFarmaceuticaSerializer,
  UnidadSerializer,
  MedicamentoSerializer,
  ReactivoSerializer,
  InsumoHospitalarioSerializer,
  PresentacionSerializer,
  Esta_UbicadoSerializer,
  Lote_ProductoSerializer,
  LoteSerializer,
  Esta_UbicadoSumSerializer,
  Lote_ProductoCantidadSerializer,
  Esta_UbicadoCantidadSerializer,
  ProductoCantidadSerializer,
  AlmacenSerializer,
  Esta_UbicadoSerializer2,

  MedicamentoCodigoSerializer,
  Lote_ProductoSerializer2,
  MedicamentoSerializer2,
  ProductoCodigoSerializer,
  ProductoHabilitadoSerializer,
  InsumoHospitalarioSerializer2,
  ReactivoSerializer2,
  DepositoSerializer,
  LoteSerializer2,
  Esta_UbicadoSumSerializer2,
)

from django.db.models import Q
from django.db.models import Avg, Count, Min, Sum

################## LOTE ########################

#----------------------- LOTE ---------------------
class LoteGuardar(APIView): 
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Lote.objects.all()
    serializer = LoteSerializer(snippets, many=True)
    return Response(serializer.data)

    #CREA UN NUEVO REGISTRO
  def post(self, request, format=None):
    serializer = LoteSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#------------------- DEVUELVE LA FECHA DE EXPIRACION DE UN LOTE------------
class LoteBusca(APIView):
  def get(self, request, buscaL, format=None):
    snippets = Lote.objects.all().order_by('id_lote').filter(id_lote=buscaL)
    serializer = LoteSerializer(snippets, many=True)
    return Response(serializer.data)

#--------------------- LOTE ELIMINA --------------------
class EliminarLote(APIView):
  def get_object(self, pk):
    try:
      return Lote.objects.get(id_lote=pk)
    except Lote.DoesNotExist:
      raise Http404
  def get(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = LoteSerializer2(snippet)
    return Response(serializer.data)
  def put(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer =LoteSerializer2(snippet, data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
  """BORRA UN REGISTRO"""
  def delete(self, request, pk, format=None):
    snippet = self.get_object(pk)
    snippet.delete()
    return Response(status=status.HTTP_204_NO_CONTENT)


#################### LOTE PRODUCTO  #####################################

#----------------- LOTE PRODUCTO GUARDAR --------------------------------
class Lote_ProductoGuardar(APIView): 
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Lote_producto.objects.all()
    serializer = Lote_ProductoSerializer2(snippets, many=True)
    return Response(serializer.data)

    #CREA UN NUEVO REGISTRO
  def post(self, request, format=None):
    serializer = Lote_ProductoSerializer2(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


#----------------- LOTE PRODUCTO BUSCA --------------------------------
class Lote_ProductoBuscaL(APIView): 
  #LISTA TODOS LOS REGISTROS
  def get(self, request, busca, format=None):
    snippets = Lote_producto.objects.all().filter(lote=busca)
    serializer = Lote_ProductoSerializer2(snippets, many=True)
    return Response(serializer.data)

##################### PRODUCTOS FARMACEUTICOS ##########################
class ListaProductos(APIView):
  """LISTA TODOS LOS PRODUCTOS FARMACEUTICOS DE FARMACIA"""
  def get(self, request):
    producto = Producto_farmaceutico.objects.all().order_by('nombre').filter(habilitado=True)
    data = ProductoSerializer(producto, many=True).data
    return Response(data)

##################### PRODUCTOS FARMACEUTICOS NOMBRES ##########################
class ListaProductosNombres(APIView):
  """LISTA TODOS LOS PRODUCTOS FARMACEUTICOS DE FARMACIA"""
  def get(self, request):
    producto = Producto_farmaceutico.objects.all().order_by('nombre').filter(habilitado=True).distinct('nombre')
    data = ProductoSerializer(producto, many=True).data
    return Response(data)

class IngresarProd(APIView):
  def post(self, request):
    codigo = request.data.get("codigo")
    nombre = request.data.get("nombre")
    stockGeneral = request.data.get("stockGeneral")
    data = {'codigo': codigo, 'nombre': nombre, 'stockGeneral':stockGeneral}
    serializer =IngresaSerializer(data=data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


##- ----- - PRODUCTO GUARDAR ---------------------
class ProductoGuardar(APIView): 
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Producto_farmaceutico.objects.all()
    serializer = ProductoSerializer(snippets, many=True)
    return Response(serializer.data)
    #CREA UN NUEVO REGISTRO
  def post(self, request, format=None):
    serializer = ProductoSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

##-LISTA DETALLE DE UN PRODUCTO (CODIGO)
class ProductoBusca2(APIView):
  def get(self, request,buscaP, format=None):
    snippets = Producto_farmaceutico.objects.all().order_by('nombre').filter(codigo=buscaP)
    serializer = ProductoSerializer(snippets, many=True)
    return Response(serializer.data)


##-LISTA DETALLE DE UN PRODUCTO (NOMBRE) 
class ProductoBuscaNombre(APIView):
  def get(self, request,buscaP, format=None):
    snippets = Producto_farmaceutico.objects.all().order_by('nombre').filter(nombre=buscaP)
    serializer = ProductoSerializer(snippets, many=True)
    return Response(serializer.data)

#--------------------- ELIMA PRODUCTO --------------------
class EliminaProducto(APIView):
  def get_object(self, pk):
    try:
      return Producto_farmaceutico.objects.get(codigo=pk)
    except Producto_farmaceutico.DoesNotExist:
      raise Http404
  def get(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = ProductoCodigoSerializer(snippet)
    return Response(serializer.data)
  def put(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer =ProductoCodigoSerializer(snippet, data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
  """BORRA UN REGISTRO"""
  def delete(self, request, pk, format=None):
    snippet = self.get_object(pk)
    snippet.delete()
    return Response(status=status.HTTP_204_NO_CONTENT)

#--------------------- EDITA PRODUCTO --------------------
class EditaProducto(APIView):
  def get_object(self, pk):
    try:
      return Producto_farmaceutico.objects.get(codigo=pk)
    except Producto_farmaceutico.DoesNotExist:
      raise Http404
  def get(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = ProductoHabilitadoSerializer(snippet)
    return Response(serializer.data)
  def put(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = ProductoHabilitadoSerializer(snippet, data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
  """BORRA UN REGISTRO"""
  def delete(self, request, pk, format=None):
    snippet = self.get_object(pk)
    snippet.delete()
    return Response(status=status.HTTP_204_NO_CONTENT)

#######################$$$ UNIDAD DE MANEJO $$$$######################
#--------------------- LISTA UNIDAD DE MANEJO -------------------------
class UnidadLista(APIView):     
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = T_unidad.objects.all().order_by('descripcion')
    serializer = UnidadSerializer(snippets, many=True)
    return Response(serializer.data)

#--------------------- UNIDAD DE MANEJO  GUARDAR --------------------------------
class UnidadGuardar(APIView): 
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = T_unidad.objects.all().order_by('descripcion')
    serializer = UnidadSerializer(snippets, many=True)
    return Response(serializer.data)
    #CREA UN NUEVO REGISTRO
  def post(self, request, format=None):
    serializer = UnidadSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#######################$$$$ FORMA FARMACEUTICA $$$$#####################
#--------------------- LISTA FORMA FARMACEUTICA -------------------------
class FormaFarmaceuticaLista(APIView):     
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = T_forma_farmaceutica.objects.all().order_by('descripcion')
    serializer = FormaFarmaceuticaSerializer(snippets, many=True)
    return Response(serializer.data)

#----------------- R FORMA FARMACEUTICA GUARDAR --------------------------------
class FormaFarmaceuticaGuardar(APIView): 
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = T_forma_farmaceutica.objects.all()
    serializer = FormaFarmaceuticaSerializer(snippets, many=True)
    return Response(serializer.data)

    #CREA UN NUEVO REGISTRO
  def post(self, request, format=None):
    serializer = FormaFarmaceuticaSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

##-LISTA DETALLE FORMA FARMACEUTICA(DESCRIPCION)
class FormaFarmaceuticaBuscar(APIView):
  def get(self, request,busca, format=None):
    snippets = T_forma_farmaceutica.objects.all().order_by('descripcion').filter(descripcion=busca)
    serializer = FormaFarmaceuticaSerializer(snippets, many=True)
    return Response(serializer.data)
#-------------------- PRESENTACION  ------------------------------

class PresentacionLista(APIView):  
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = T_presentacion.objects.all().order_by('descripcion')
    serializer = PresentacionSerializer(snippets, many=True)
    return Response(serializer.data)


#-----------------PRESENTACION GUARDAR --------------------------------
class PresentacionGuardar(APIView): 
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = T_presentacion.objects.all().order_by('descripcion')
    serializer = PresentacionSerializer(snippets, many=True)
    return Response(serializer.data)
    #CREA UN NUEVO REGISTRO
  def post(self, request, format=None):
    serializer = PresentacionSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

##-LISTA DETALLE FORMA FARMACEUTICA(DESCRIPCION)
class PresentacionBuscar(APIView):
  def get(self, request,busca, format=None):
    snippets = T_presentacion.objects.all().order_by('descripcion').filter(descripcion=busca)
    serializer = PresentacionSerializer(snippets, many=True)
    return Response(serializer.data)


#------------------------MEDICAMENTOS HABILITADOS---------------------
class MedicamentosLista(APIView): 
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Medicamento.objects.all().order_by('producto_farmaceutico__nombre').filter(producto_farmaceutico__habilitado=True)
    serializer = MedicamentoSerializer(snippets, many=True)
    return Response(serializer.data)

    #CREA UN NUEVO REGISTRO
  def post(self, request, format=None):
    serializer = MedicamentoSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#------------------------MEDICAMENTOS INABILITADOS---------------------
class MedicamentosInhabilitados(APIView): 
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Medicamento.objects.all().order_by('producto_farmaceutico__nombre').filter(producto_farmaceutico__habilitado=False)
    serializer = MedicamentoSerializer(snippets, many=True)
    return Response(serializer.data)

    #CREA UN NUEVO REGISTRO
  def post(self, request, format=None):
    serializer = MedicamentoSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#------------------------MEDICAMENTOS HABILITADOS Y NO HABILITADOS---------------------
class MedicamentosTodo(APIView): 
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Medicamento.objects.all().order_by('producto_farmaceutico__nombre')
    serializer = MedicamentoSerializer(snippets, many=True)
    return Response(serializer.data)

    #CREA UN NUEVO REGISTRO
  def post(self, request, format=None):
    serializer = MedicamentoSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


#--------------------- ELIMA MEDICAMENTO --------------------
class EliminaMedicamento(APIView):
  def get_object(self, pk):
    try:
      return Medicamento.objects.get(producto_farmaceutico=pk)
    except Medicamento.DoesNotExist:
      raise Http404
  def get(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = MedicamentoSerializer2(snippet)
    return Response(serializer.data)
  def put(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = MedicamentoSerializer2(snippet, data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
  """BORRA UN REGISTRO"""
  def delete(self, request, pk, format=None):
    snippet = self.get_object(pk)
    snippet.delete()
    return Response(status=status.HTTP_204_NO_CONTENT)
  
    #DETALLE DE UN REGISTRO
class MedicamentoDetalle(APIView):
  def get_object(self, pk):
    try:
      return Medicamento.objects.get(pk=pk)
    except Medicamento.DoesNotExist:
      raise Http404
          
  #DEVUELVE UN REGISTRO
  def get(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = MedicamentoSerializer(snippet)
    return Response(serializer.data)

  #ACTUALIZA UN REGISTRO
  def put(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = MedicamentoSerializer(snippet, data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    #BORRA UN REGISTRO
  def delete(self, request, pk, format=None):
    snippet = self.get_object(pk)
    snippet.delete()
    return Response(status=status.HTTP_204_NO_CONTENT)

    #BUSCA MEDICAMENTO        
class DetalleMedicamentoBusca(APIView):
  def get(self, request, busca, format=None):
    snippets=Medicamento.objects.filter(pk=busca) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = MedicamentoSerializer(snippets, many=True)
    return Response(serializer.data)

class DetalleMedicamentoBuscaNombre(APIView):
  def get(self, request, busca, format=None):
    snippets=Medicamento.objects.filter(producto_farmaceutico__nombre=busca) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = MedicamentoSerializer(snippets, many=True)
    return Response(serializer.data)
    
#------------------------GUARDA MEDICAMENTO---------------------
class MedicamentoGuardar(APIView): 
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Medicamento.objects.all()
    serializer = MedicamentoSerializer2(snippets, many=True)
    return Response(serializer.data)

    #CREA UN NUEVO REGISTRO
  def post(self, request, format=None):
    serializer = MedicamentoSerializer2(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
  
class MedicamentosCodigo(APIView): 
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Medicamento.objects.all()
    serializer = MedicamentoCodigoSerializer(snippets, many=True)
    return Response(serializer.data)

#####################$$$$ REACTIVO $$$####################################
#----------------------REACTIVOS HABILITADOS Y NO HABILITADOS---------------------
class ReactivosTodo(APIView): 
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Reactivo.objects.all().order_by('producto_farmaceutico__nombre')
    serializer = ReactivoSerializer(snippets, many=True)
    return Response(serializer.data)

#------------------- REACTIVOS C ---------------------------
class ReactivoLista(APIView):  
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Reactivo.objects.all().order_by('producto_farmaceutico__nombre').filter(producto_farmaceutico__habilitado=True)
    serializer = ReactivoSerializer(snippets, many=True)
    return Response(serializer.data)

#------------------------REACTIVOS INABILITADOS---------------------
class ReactivosInhabilitados(APIView): 
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Reactivo.objects.all().order_by('producto_farmaceutico__nombre').filter(producto_farmaceutico__habilitado=False)
    serializer = ReactivoSerializer(snippets, many=True)
    return Response(serializer.data)

#------------------------GUARDA MEDICAMENTO---------------------
class ReactivoGuardar(APIView): 
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Reactivo.objects.all()
    serializer = ReactivoSerializer2(snippets, many=True)
    return Response(serializer.data)

    #CREA UN NUEVO REGISTRO
  def post(self, request, format=None):
    serializer = ReactivoSerializer2(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#------------------------BUSCA MEDICAMENTO---------------------        
class DetalleReactivoBusca(APIView):
  def get(self, request, busca, format=None):
    snippets= Reactivo.objects.filter(pk=busca) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = ReactivoSerializer2(snippets, many=True)
    return Response(serializer.data)


#####################$$$$ INSUMO HOSPITALARIO F $$$$######################
#------------------------INSUMOS HABILITADOS Y NO HABILITADOS---------------------
class InsumosTodo(APIView): 
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Insumo_hospitalario.objects.all().order_by('producto_farmaceutico__nombre')
    serializer = InsumoHospitalarioSerializer(snippets, many=True)
    return Response(serializer.data)

#------------------------INSUMOS INABILITADOS---------------------
class InsumosInhabilitados(APIView): 
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Insumo_hospitalario.objects.all().order_by('producto_farmaceutico__nombre').filter(producto_farmaceutico__habilitado=False)
    serializer = InsumoHospitalarioSerializer(snippets, many=True)
    return Response(serializer.data)

class InsumoHospitalarioLista(APIView):   
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Insumo_hospitalario.objects.all().order_by('producto_farmaceutico__nombre').filter(producto_farmaceutico__habilitado=True)
    serializer = InsumoHospitalarioSerializer(snippets, many=True)
    return Response(serializer.data)

    #BUSCA INSUMO
class DetalleInsumoBusca(APIView):
  def get(self, request, busca, format=None):
    snippets= Insumo_hospitalario.objects.filter(pk=busca) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = InsumoHospitalarioSerializer(snippets, many=True)
    return Response(serializer.data)
    
    #BUSCA INSUMO
class DetalleInsumoBuscaNombre(APIView):
  def get(self, request, busca, format=None):
    snippets= Insumo_hospitalario.objects.filter(producto_farmaceutico__nombre=busca) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = InsumoHospitalarioSerializer(snippets, many=True)
    return Response(serializer.data)

#------------------------GUARDA INSUMO---------------------
class InsumoGuardar(APIView): 
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Insumo_hospitalario.objects.all()
    serializer = InsumoHospitalarioSerializer2(snippets, many=True)
    return Response(serializer.data)

    #CREA UN NUEVO REGISTRO
  def post(self, request, format=None):
    serializer = InsumoHospitalarioSerializer2(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
  
    
#----------------------- ESTA UBICADO .-----------------------
  #LISTA TODOS LOS REGISTROS
class EstaUbicadoLista(APIView):   
  def get(self, request, format=None):
    snippets = Esta_Ubicado.objects.all().order_by('producto_farmaceutico__nombre')
    serializer = Esta_UbicadoSerializer(snippets, many=True)
    return Response(serializer.data)



## CREA UN NUEVO REGISTRO ESTA UBICADO 
class EstaUbicadoRegistra(APIView): 
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Esta_Ubicado.objects.all()
    serializer = Esta_UbicadoSerializer2(snippets, many=True)
    return Response(serializer.data)

    #CREA UN NUEVO REGISTRO
  def post(self, request, format=None):
    serializer = Esta_UbicadoSerializer2(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


  #LISTA TODOS LOS REGISTROS BUSCA COD
class EstaUbicadoListaBuscaCod(APIView):
  def get(self, request, busca, format=None):
    snippets = Esta_Ubicado.objects.all().order_by('producto_farmaceutico__nombre').filter(producto_farmaceutico__codigo=busca)
    serializer = Esta_UbicadoSerializer(snippets, many=True)
    return Response(serializer.data)


##--LISTA ESTA UBICADO PRODUCTOS
class EstaUbicadoBusca(APIView):
  def get(self, request,busca, format=None):
    snippets= Esta_Ubicado.objects.all().order_by('producto_farmaceutico__nombre').filter(almacen=busca) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = Esta_UbicadoSerializer(snippets, many=True)
    return Response(serializer.data)

##--LISTA ESTA UBICADO PRODUCTOS BUSCA ALMACEN
class UbicadoAlmacenBuscaCodP(APIView):
  def get(self, request,busca, format=None):
    snippets= Esta_Ubicado.objects.all().order_by('lote__fecha_expiracion').filter(almacen=2).filter(producto_farmaceutico__codigo=busca) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = Esta_UbicadoSerializer(snippets, many=True)
    return Response(serializer.data)

##--LISTA ESTA UBICADO PRODUCTOS BUSCA FARMACIA
class UbicadoFarmaciaBuscaCodP(APIView):
  def get(self, request,busca, format=None):
    snippets= Esta_Ubicado.objects.all().order_by('lote__fecha_expiracion').filter(almacen=1).filter(producto_farmaceutico__codigo=busca) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = Esta_UbicadoSerializer(snippets, many=True)
    return Response(serializer.data)


##-LISTA ESTA UBICADO PRODUCTOS BUSCA (MEDICAMENTO CON LOTE)
class UbicadoFarmaciaBusca2(APIView):
  def get(self, request,buscaP, buscaL, format=None):
    snippets = Esta_Ubicado.objects.all().order_by('producto_farmaceutico__nombre').filter(almacen=1).filter(producto_farmaceutico__codigo=buscaP).filter(lote=buscaL)
    serializer = Esta_UbicadoSerializer(snippets, many=True)
    return Response(serializer.data)

##-LISTA ESTA UBICADO PRODUCTOS BUSCA (MEDICAMENTO CON LOTE)
class UbicadoAlmacenBusca2(APIView):
  def get(self, request,buscaP, buscaL, format=None):
    snippets = Esta_Ubicado.objects.all().order_by('producto_farmaceutico__nombre').filter(almacen=2).filter(producto_farmaceutico__codigo=buscaP).filter(lote=buscaL)
    serializer = Esta_UbicadoSerializer(snippets, many=True)
    return Response(serializer.data)

##--LISTA ESTA UBICADO MEDICAMENTOS
class EstaUbicadoBuscaMedicamentos(APIView):
  def get(self, request,busca, format=None):
    result = Medicamento.objects.all()
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets= Esta_Ubicado.objects.all().order_by('producto_farmaceutico__nombre').filter(almacen=busca).filter(producto_farmaceutico__in=lista) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = Esta_UbicadoSerializer(snippets, many=True)
    return Response(serializer.data)

##--LISTA ESTA UBICADO MEDICAMENTOS BUSCA ALMACEN
class UbicadoAlmacenBuscaCodM(APIView):
  def get(self, request,busca, format=None):
    result = Medicamento.objects.all().filter(producto_farmaceutico__codigo=busca)
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets= Esta_Ubicado.objects.all().order_by('producto_farmaceutico__nombre').filter(almacen=2).filter(producto_farmaceutico__in=lista) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = Esta_UbicadoSerializer(snippets, many=True)
    return Response(serializer.data)

##--LISTA ESTA UBICADO MEDICAMENTOS BUSCA FARMACIA
class UbicadoFarmaciaBuscaCodM(APIView):
  def get(self, request,busca, format=None):
    result = Medicamento.objects.all().filter(producto_farmaceutico__codigo=busca)
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets= Esta_Ubicado.objects.all().order_by('producto_farmaceutico__nombre').filter(almacen=1).filter(producto_farmaceutico__in=lista) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = Esta_UbicadoSerializer(snippets, many=True)
    return Response(serializer.data)

##---- MUESTRA UN REGISTRO DE ESTA UBICADO MEDICAMENTOS BUSCA FARMACIA CANTIDAD SUMADA


class UbicadoFarmaciaBuscaCodMsuma(APIView):
  def get(self, request, busca, format=None):
    snippets= Esta_Ubicado.objects.values('producto_farmaceutico_id').filter(almacen=1).annotate(sum=Sum('stockAlmacen')).filter(producto_farmaceutico_id=busca) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = Esta_UbicadoSumSerializer(snippets, many=True)
    return Response(serializer.data)

##---- MUESTRA UN REGISTRO DE ESTA UBICADO MEDICAMENTOS BUSCA LOCALIZACION(X) CANTIDAD SUMADA
class LocalizacionBuscaCodsuma(APIView):
  def get(self, request, buscaL,buscaCod, format=None):
    snippets= Esta_Ubicado.objects.values('producto_farmaceutico_id').filter(almacen=buscaL).annotate(sum=Sum('stockAlmacen')).filter(producto_farmaceutico_id=buscaCod) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = Esta_UbicadoSumSerializer(snippets, many=True)
    return Response(serializer.data)
    
##--LISTA ESTA UBICADO INSUMOS
class EstaUbicadoBuscaInsumos(APIView):
  def get(self, request,busca, format=None):
    result = Insumo_hospitalario.objects.all()
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets= Esta_Ubicado.objects.all().order_by('producto_farmaceutico__nombre').filter(almacen=busca).filter(producto_farmaceutico__in=lista) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = Esta_UbicadoSerializer(snippets, many=True)
    return Response(serializer.data)

##--LISTA ESTA UBICADO INSUMOS BUSCA ALMACEN
class UbicadoAlmacenBuscaCodI(APIView):
  def get(self, request,busca, format=None):
    result = Insumo_hospitalario.objects.all().filter(producto_farmaceutico__codigo=busca)
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets= Esta_Ubicado.objects.all().order_by('producto_farmaceutico__nombre').filter(almacen=2).filter(producto_farmaceutico__in=lista) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = Esta_UbicadoSerializer(snippets, many=True)
    return Response(serializer.data)

##--LISTA ESTA UBICADO INSUMOS BUSCA FARMACIA
class UbicadoFarmaciaBuscaCodI(APIView):
  def get(self, request,busca, format=None):
    result = Insumo_hospitalario.objects.all().filter(producto_farmaceutico__codigo=busca)
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets= Esta_Ubicado.objects.all().order_by('producto_farmaceutico__nombre').filter(almacen=1).filter(producto_farmaceutico__in=lista) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = Esta_UbicadoSerializer(snippets, many=True)
    return Response(serializer.data)


##--LISTA ESTA UBICADO REACTIVOS
class EstaUbicadoBuscaReactivos(APIView):
  def get(self, request,busca, format=None):
    result = Reactivo.objects.all()
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets= Esta_Ubicado.objects.all().order_by('producto_farmaceutico__nombre').filter(almacen=busca).filter(producto_farmaceutico__in=lista) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = Esta_UbicadoSerializer(snippets, many=True)
    return Response(serializer.data)

##--LISTA ESTA UBICADO REACTIVOS BUSCA ALMACEN
class UbicadoAlmacenBuscaCodR(APIView):
  def get(self, request,busca, format=None):
    result = Reactivo.objects.all().filter(producto_farmaceutico__codigo=busca)
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets= Esta_Ubicado.objects.all().order_by('producto_farmaceutico__nombre').filter(almacen=2).filter(producto_farmaceutico__in=lista) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = Esta_UbicadoSerializer(snippets, many=True)
    return Response(serializer.data)

##--LISTA ESTA UBICADO REACTIVOS BUSCA FARMACIA
class UbicadoFarmaciaBuscaCodR(APIView):
  def get(self, request,busca, format=None):
    result = Reactivo.objects.all().filter(producto_farmaceutico__codigo=busca)
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets= Esta_Ubicado.objects.all().order_by('producto_farmaceutico__nombre').filter(almacen=1).filter(producto_farmaceutico__in=lista) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = Esta_UbicadoSerializer(snippets, many=True)
    return Response(serializer.data)

#------------------- LOTE SERIALIZERS ------------------------
##-LISTA LOTE PRODUCTOS
class Lote_ProductoLista(APIView):
  def get(self, request, format=None):
    snippets = Lote_producto.objects.all().order_by('producto_farmaceutico__nombre')
    serializer = Lote_ProductoSerializer(snippets, many=True)
    return Response(serializer.data)

##-LISTA LOTE PRODUCTOS BUSCA
class Lote_ProductoBusca(APIView):
  def get(self, request,busca, format=None):
    snippets = Lote_producto.objects.all().order_by('producto_farmaceutico__nombre').filter(producto_farmaceutico__codigo=busca).filter(producto_farmaceutico__habilitado=True)
    serializer = Lote_ProductoSerializer(snippets, many=True)
    return Response(serializer.data)

##-LISTA LOTE PRODUCTOS BUSCA (MEDICAMENTO CON LOTE)
class Lote_ProductoBusca2(APIView):
  def get(self, request,buscaP, buscaL, format=None):
    snippets = Lote_producto.objects.all().order_by('producto_farmaceutico__nombre').filter(producto_farmaceutico__codigo=buscaP).filter(lote=buscaL)
    serializer = Lote_ProductoSerializer(snippets, many=True)      
    return Response(serializer.data)

##-LISTA LOTE MEDICAMENTOS
class Lote_ProductoListaMedicamentos(APIView):
  def get(self, request, format=None):
    result = Medicamento.objects.all()
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets = Lote_producto.objects.all().order_by('producto_farmaceutico__nombre').filter(producto_farmaceutico__in=lista).filter(producto_farmaceutico__habilitado=True)
    serializer = Lote_ProductoSerializer(snippets, many=True)
    return Response(serializer.data)
  

##-LISTA LOTE MEDICAMENTOS BUSCA
class Lote_ProductoMedicamentoBusca(APIView):
  def get(self, request,busca,format=None):
    result = Medicamento.objects.all().filter(producto_farmaceutico__codigo=busca)
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets = Lote_producto.objects.all().order_by('producto_farmaceutico__nombre').filter(producto_farmaceutico__in=lista).filter(producto_farmaceutico__habilitado=True)
    serializer = Lote_ProductoSerializer(snippets, many=True)
    return Response(serializer.data)

##-LISTA LOTE MEDICAMENTOS BUSCA POR LOTE 
class Lote_ProductoMedicamentoBuscaLote(APIView):
  def get(self, request,busca,format=None):
    result = Medicamento.objects.all()
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets = Lote_producto.objects.all().order_by('producto_farmaceutico__nombre').filter(producto_farmaceutico__in=lista).filter(producto_farmaceutico__habilitado=True).filter(lote__id_lote=busca)
    serializer = Lote_ProductoSerializer(snippets, many=True)
    return Response(serializer.data)


##-LISTA LOTE INSUMOS
class Lote_ProductoListaInsumos(APIView):
  def get(self, request, format=None):
    result = Insumo_hospitalario.objects.all()
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets = Lote_producto.objects.all().order_by('producto_farmaceutico__nombre').filter(producto_farmaceutico__in=lista).filter(producto_farmaceutico__habilitado=True)
    serializer = Lote_ProductoSerializer(snippets, many=True)
    return Response(serializer.data)

##-LISTA LOTE INSUMOS BUSCA
class Lote_ProductoInsumosBusca(APIView):
  def get(self, request, busca, format=None):
    result = Insumo_hospitalario.objects.all().filter(producto_farmaceutico__codigo=busca)
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets = Lote_producto.objects.all().order_by('producto_farmaceutico__nombre').filter(producto_farmaceutico__in=lista).filter(producto_farmaceutico__habilitado=True)
    serializer = Lote_ProductoSerializer(snippets, many=True)
    return Response(serializer.data)

##-LISTA LOTE INSUMOS BUSCA POR LOTE

class Lote_ProductoInsumosBuscaLote(APIView):
  def get(self, request, busca, format=None):
    result = Insumo_hospitalario.objects.all()
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets = Lote_producto.objects.all().order_by('producto_farmaceutico__nombre').filter(producto_farmaceutico__in=lista).filter(producto_farmaceutico__habilitado=True).filter(lote__id_lote=busca)
    serializer = Lote_ProductoSerializer(snippets, many=True)
    return Response(serializer.data)

##-LISTA LOTE REACTIVOS
class Lote_ProductoListaReactivos(APIView):
  def get(self, request, format=None):
    result = Reactivo.objects.all()
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets = Lote_producto.objects.all().order_by('producto_farmaceutico__nombre').filter(producto_farmaceutico__in=lista).filter(producto_farmaceutico__habilitado=True)
    serializer = Lote_ProductoSerializer(snippets, many=True)
    return Response(serializer.data)

##-LISTA LOTE REACTIVOS BUSCA
class Lote_ProductoReactivosBusca(APIView):
  def get(self, request, busca,format=None):
    result = Reactivo.objects.all().filter(producto_farmaceutico__codigo=busca)
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets = Lote_producto.objects.all().order_by('producto_farmaceutico__nombre').filter(producto_farmaceutico__in=lista).filter(producto_farmaceutico__habilitado=True)
    serializer = Lote_ProductoSerializer(snippets, many=True)
    return Response(serializer.data)

##-LISTA LOTE REACTIVOS BUSCA POR LOTE

class Lote_ProductoReactivosBuscaLote(APIView):
  def get(self, request, busca,format=None):
    result = Reactivo.objects.all()
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets = Lote_producto.objects.all().order_by('producto_farmaceutico__nombre').filter(producto_farmaceutico__in=lista).filter(producto_farmaceutico__habilitado=True).filter(lote__id_lote=busca)
    serializer = Lote_ProductoSerializer(snippets, many=True)
    return Response(serializer.data)

###--- ACTUALIZA LOTE-ṔRODUCTO FARMACIA 
class LoteCantidadEdita(APIView):
  def get_object(self, pk):
    try:
      return Lote_producto.objects.get(lote__id_lote=pk)
    except Lote_producto.DoesNotExist:
      raise Http404

  def get(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = Lote_ProductoCantidadSerializer(snippet)
    return Response(serializer.data)

  def put(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = Lote_ProductoCantidadSerializer(snippet, data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

###--- ACTUALIZA ESTA UBICADO  FARMACIA (CANTIDAD) 
class EstaFarmaciaEdita(APIView):
  def get_object(self, pk):
    try:
      return Esta_Ubicado.objects.filter(almacen=1).get(lote__id_lote=pk)
    except Esta_Ubicado.DoesNotExist:
      raise Http404

  def get(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = Esta_UbicadoCantidadSerializer(snippet)
    return Response(serializer.data)

  def put(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = Esta_UbicadoCantidadSerializer(snippet, data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
###--- ACTUALIZA ESTA UBICADO  ALMACEN (CANTIDAD) 
class EstaAlmacenEdita(APIView):
  def get_object(self, pk):
    try:
      return Esta_Ubicado.objects.filter(almacen=2).get(lote__id_lote=pk)
    except Esta_Ubicado.DoesNotExist:
      raise Http404

  def get(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = Esta_UbicadoCantidadSerializer(snippet)
    return Response(serializer.data)

  def put(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = Esta_UbicadoCantidadSerializer(snippet, data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)




###--- ACTUALIZA PRODUCTO (CANTIDAD) 
class ProductoEdita(APIView):
  def get_object(self, pk):
    try:
      return Producto_farmaceutico.objects.get(codigo=pk)
    except Esta_Ubicado.DoesNotExist:
      raise Http404

  def get(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = ProductoCantidadSerializer(snippet)
    return Response(serializer.data)

  def put(self, request, pk, format=None):
    snippet = self.get_object(pk)
    serializer = ProductoCantidadSerializer(snippet, data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#-------------- LISTA ALMACEN ------------------------
class ListaAlmacen(APIView):
  """LISTA LOS ALMACENES"""
  def get(self, request):
    almacen = Almacen.objects.all().order_by('descripcion')
    data = AlmacenSerializer(almacen, many=True).data
    return Response(data)

############" BUSCA MEDICAMENTOS CODIGO "#####################
class MedicamentosBusca(APIView):
  def get(self, request, format=None):
    result = Medicamento.objects.all()
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets = Producto_farmaceutico.objects.all().order_by('nombre').filter(codigo__in=lista).filter(habilitado=True)
    serializer = ProductoSerializer(snippets, many=True)
    return Response(serializer.data)

############" BUSCA MEDICAMENTOS NOMBRE"#####################
class MedicamentosBuscaNombre(APIView):
  def get(self, request, format=None):
    result = Medicamento.objects.all()
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets = Producto_farmaceutico.objects.all().order_by('nombre').filter(codigo__in=lista).filter(habilitado=True).distinct('nombre')
    serializer = ProductoSerializer(snippets, many=True)
    return Response(serializer.data)

############" BUSCA INSUMOS "#####################
class InsumosBusca(APIView):
  def get(self, request, format=None):
    result = Insumo_hospitalario.objects.all()
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets = Producto_farmaceutico.objects.all().order_by('nombre').filter(codigo__in=lista).filter(habilitado=True)
    serializer = ProductoSerializer(snippets, many=True)
    return Response(serializer.data)

############" BUSCA INSUMOS NOMBRE"#####################
class InsumosBuscaNombre(APIView):
  def get(self, request, format=None):
    result = Insumo_hospitalario.objects.all()
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets = Producto_farmaceutico.objects.all().order_by('nombre').filter(codigo__in=lista).filter(habilitado=True).distinct('nombre')
    serializer = ProductoSerializer(snippets, many=True)
    return Response(serializer.data)

############" BUSCA Reactivo"#####################
class ReactivosBusca(APIView):
  def get(self, request, format=None):
    result = Reactivo.objects.all()
    lista = result.values_list('producto_farmaceutico', flat=True)
    snippets = Producto_farmaceutico.objects.all().order_by('nombre').filter(codigo__in=lista).filter(habilitado=True)
    serializer = ProductoSerializer(snippets, many=True)
    return Response(serializer.data)

##############$$$ DEPOSITO  $$$##############
#---------------------- DEPOSITO GUARDAR --------------------------
class DepositoGuardar(APIView): 
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Deposito.objects.all()
    serializer = DepositoSerializer(snippets, many=True)
    return Response(serializer.data)
    #CREA UN NUEVO REGISTRO
  def post(self, request, format=None):
    serializer = DepositoSerializer(data=request.data)
    if serializer.is_valid():
      serializer.save()
      return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#---------------------- DEPOSITO TODOS --------------------------
class DepositoTodos(APIView): 
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Deposito.objects.all().order_by('-id')
    serializer = DepositoSerializer(snippets, many=True)
    return Response(serializer.data)
#---------------------- DEPOSITO MEDICAMENTOS --------------------------
class DepositoMedicamentos(APIView): 
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Deposito.objects.all().filter(tipo_producto='medicamento').order_by('-id')
    serializer = DepositoSerializer(snippets, many=True)
    return Response(serializer.data)

#---------------------- DEPOSITO INSUMOS --------------------------
class DepositoInsumos(APIView): 
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Deposito.objects.all().filter(tipo_producto='insumo').order_by('-id')
    serializer = DepositoSerializer(snippets, many=True)
    return Response(serializer.data)

#---------------------- DEPOSITO REACTIVOS --------------------------
class DepositoReactivos(APIView): 
  #LISTA TODOS LOS REGISTROS
  def get(self, request, format=None):
    snippets = Deposito.objects.all().filter(tipo_producto='reactivo').order_by('-id')
    serializer = DepositoSerializer(snippets, many=True)
    return Response(serializer.data)


class UbicadoFarmaciaBuscaCodMsuma2(APIView):
  def get(self, request, busca, format=None):
    snippets= Esta_Ubicado.objects.all().filter(almacen=1).filter(producto_farmaceutico_id=busca).aggregate(sum=Sum('stockAlmacen')) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    return Response(snippets)

########################################


class UbicadoFarmaciaBuscaCodMsumaHola(APIView):
  def get(self, request, busca, format=None):
    snippets= Esta_Ubicado.objects.values('producto_farmaceutico_id').filter(almacen=1).filter(producto_farmaceutico__nombre=busca).annotate(sum=Sum('stockAlmacen')) #| Q(ci__iexact=busca) | Q(registro_universitario__iexact=busca) | Q(cod_promes__iexact=busca) )
    serializer = Esta_UbicadoSumSerializer2(snippets, many=True)
    return Response(serializer.data)
