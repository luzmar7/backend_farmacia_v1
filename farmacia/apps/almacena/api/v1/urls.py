from django.urls import path
from .views import (
  ListaProductos,
  IngresarProd, 
  FormaFarmaceuticaLista, 
  MedicamentosLista, 
  DetalleMedicamentoBusca,
  MedicamentoDetalle,
  ReactivoLista, 
  InsumoHospitalarioLista, 
  PresentacionLista,
  EstaUbicadoLista, 
  EstaUbicadoBusca, 
  Lote_ProductoLista,
  Lote_ProductoListaMedicamentos,
  Lote_ProductoListaInsumos, 
  Lote_ProductoListaReactivos, 
  EstaUbicadoBuscaMedicamentos,
  EstaUbicadoBuscaInsumos, 
  EstaUbicadoBuscaReactivos,
  Lote_ProductoMedicamentoBusca, 
  Lote_ProductoInsumosBusca, 
  Lote_ProductoReactivosBusca,
  Lote_ProductoBusca, 
  UbicadoAlmacenBuscaCodI, 
  UbicadoAlmacenBuscaCodM,
  UbicadoAlmacenBuscaCodR, 
  UbicadoAlmacenBuscaCodP, 
  UbicadoFarmaciaBuscaCodI,
  UbicadoFarmaciaBuscaCodR, 
  UbicadoFarmaciaBuscaCodM, 
  UbicadoFarmaciaBuscaCodP,
  EstaUbicadoListaBuscaCod,
  UbicadoFarmaciaBuscaCodMsuma,
  DetalleMedicamentoBuscaNombre, 
  Lote_ProductoBusca2, 
  LoteCantidadEdita,
  EstaFarmaciaEdita, 
  UbicadoFarmaciaBusca2,
  ProductoBusca2, 
  ProductoEdita,
  ListaAlmacen,
  LocalizacionBuscaCodsuma,
  ProductoBuscaNombre,
  UbicadoAlmacenBusca2,
  UbicadoAlmacenBuscaCodP,

  EstaAlmacenEdita,
  EstaUbicadoRegistra,
  DetalleInsumoBusca,

  MedicamentosCodigo,
  MedicamentosBusca,
  InsumosBusca,
  DetalleInsumoBuscaNombre,

  LoteGuardar,
  Lote_ProductoGuardar,
  ReactivosBusca,
  LoteBusca,
  ProductoGuardar,
  FormaFarmaceuticaGuardar,
  FormaFarmaceuticaBuscar,
  MedicamentoGuardar,
  EliminaMedicamento,
  EliminaProducto,
  EditaProducto,

  MedicamentosTodo,
  MedicamentosInhabilitados, 

  PresentacionGuardar,
  PresentacionBuscar,
  InsumoGuardar,
  UnidadLista,
  UnidadGuardar,
  ReactivoGuardar,
  DetalleReactivoBusca,
  InsumosTodo,
  InsumosInhabilitados,
  ReactivosTodo,
  ReactivosInhabilitados,
  DepositoGuardar,
  Lote_ProductoMedicamentoBuscaLote,
  Lote_ProductoInsumosBuscaLote,
  Lote_ProductoReactivosBuscaLote,
  
  DepositoMedicamentos,
  DepositoInsumos,
  DepositoReactivos,
  DepositoTodos,
  EliminarLote,
  
  UbicadoFarmaciaBuscaCodMsuma2,
  MedicamentosBuscaNombre,
  UbicadoFarmaciaBuscaCodMsumaHola,
  InsumosBuscaNombre,
  ListaProductosNombres,
  Lote_ProductoBuscaL,
)

app_name = 'almacena'
urlpatterns = [
  ## PRODUCTOS
  path('productos/', ListaProductos.as_view(), name="ListaProductos"),

  path('ListaProductosNombres/', ListaProductosNombres.as_view(), name="ListaProductosNombres"),
  path('ProductoGuardar/', ProductoGuardar.as_view(), name="ProductoGuardar"),

  path('ingresar/', IngresarProd.as_view(), name="IngresarProd"),
  path('ProductoBusca2/<str:buscaP>/', ProductoBusca2.as_view(), name="ProductoBusca2"),
  path('ProductoBuscaNombre/<str:buscaP>/', ProductoBuscaNombre.as_view(), name="ProductoBuscaNombre"),

  ## EDITA PRODUCTO CANTIDAD (busca codigo producto)
  path('ProductoEdita/<str:pk>/',ProductoEdita.as_view(), name="ProductoEdita"),

  path('EliminaProducto/<str:pk>/',EliminaProducto.as_view(), name="EliminaProducto"),

  path('EditaProducto/<str:pk>/', EditaProducto.as_view(), name="EditaProducto"),

  #MEDICAMENTOS
  path('FormaFarmaceuticaLista/',FormaFarmaceuticaLista.as_view(), name="FormaFarmaceuticaLista"),
  path('FormaFarmaceuticaGuardar/',FormaFarmaceuticaGuardar.as_view(), name="FormaFarmaceuticaGuardar"),

  path('FormaFarmaceuticaBuscar/<str:busca>/', FormaFarmaceuticaBuscar.as_view(), name="FormaFarmaceuticaBuscar"),

  path('Medicamentos/',MedicamentosLista.as_view(), name="Medicamentosb"),
  path('MedicamentosInhabilitados/',MedicamentosInhabilitados.as_view(), name="MedicamentosInhabilitados"),
  path('MedicamentosTodo/',MedicamentosTodo.as_view(), name="MedicamentosTodo"),

  path('MedicamentoGuardar/',MedicamentoGuardar.as_view(), name="MedicamentoGuardar"),

  path('detalleMedicamento/<str:pk>/',MedicamentoDetalle.as_view(), name="MedicamentobDetalle"),
  path('detalleMedicamentoBusca/<str:busca>/',DetalleMedicamentoBusca.as_view(), name="MedicamentobDetalle"),
  path('DetalleMedicamentobBuscaNombre/<str:busca>/',DetalleMedicamentoBuscaNombre.as_view(), name="DetalleMedicamentobBuscaNombre"),

  path('MedicamentosCodigo/',MedicamentosCodigo.as_view(), name="MedicamentosCodigo"),

  path('MedicamentosBusca/',MedicamentosBusca.as_view(), name="MedicamentosBusca"),

  path('MedicamentosBuscaNombre/',MedicamentosBuscaNombre.as_view(), name="MedicamentosBuscaNombre"),


  path('ReactivosBusca/',ReactivosBusca.as_view(), name="ReactivosBusca"),

  path('EliminaMedicamento/<str:pk>/',EliminaMedicamento.as_view(), name="EliminaMedicamento"),

  #REACTIVOS
  path('ReactivosTodo/',ReactivosTodo.as_view(), name="ReactivosTodo"),
  path('Reactivos/',ReactivoLista.as_view(), name="Reactivos"),
  path('ReactivosInhabilitados/',ReactivosInhabilitados.as_view(), name="ReactivosInhabilitados"),
  path('ReactivoGuardar/',ReactivoGuardar.as_view(), name="ReactivoGuardar"),
  path('DetalleReactivoBusca/<str:busca>/',DetalleReactivoBusca.as_view(), name="DetalleReactivoBusca"),

  #INSUMOSInsumoHospitalario

  path('InsumosTodo/',InsumosTodo.as_view(), name="InsumosTodo"),
  
  path('InsumoHospitalario/',InsumoHospitalarioLista.as_view(), name="InsumoHospitalario"),
  path('InsumosInhabilitados/',InsumosInhabilitados.as_view(), name="InsumosInhabilitados"),

  path('PresentacionLista/',PresentacionLista.as_view(), name="PresentacionLista"),
  path('PresentacionGuardar/',PresentacionGuardar.as_view(), name="PresentacionGuardar"),
  path('PresentacionBuscar/<str:busca>/', PresentacionBuscar.as_view(), name="PresentacionBuscar"),

  path('UnidadLista/',UnidadLista.as_view(), name="UnidadLista"),
  path('UnidadGuardar/',UnidadGuardar.as_view(), name="UnidadGuardar"),

  path('InsumosBusca/',InsumosBusca.as_view(), name="InsumosBusca"),

  path('InsumosBuscaNombre/',InsumosBuscaNombre.as_view(), name="InsumosBuscaNombre"),
  path('InsumoGuardar/',InsumoGuardar.as_view(), name="InsumoGuardar"),

  path('DetalleInsumoBusca/<str:busca>/',DetalleInsumoBusca.as_view(), name="DetalleInsumoBusca"),

  path('DetalleInsumoBuscaNombre/<str:busca>/',DetalleInsumoBuscaNombre.as_view(), name="DetalleInsumoBuscaNombre"),


  #LOTE PRODUCTO 
  path('LoteProductoLista/',Lote_ProductoLista.as_view(), name="LoteProductoLista"),
  path('LoteMedicamentoLista/', Lote_ProductoListaMedicamentos.as_view(), name="LoteMedicamentoLista"),
  path('LoteInsumosLista/', Lote_ProductoListaInsumos.as_view(), name="LoteInsumosLista"),
  path('LoteReactivosLista/', Lote_ProductoListaReactivos.as_view(), name="LoteReactivosLista"),

  path('Lote_ProductoGuardar/', Lote_ProductoGuardar.as_view(), name="Lote_ProductoGuardar"),

  path('LoteBusca/<str:buscaL>/', LoteBusca.as_view(), name="LoteBusca"),
  path('Lote_ProductoBuscaL/<str:busca>/', Lote_ProductoBuscaL.as_view(), name="Lote_ProductoBuscaL"),

  #path('EstaUbicadoBusca/<int:busca>/',EstaUbicadoBusca.as_view(), name="EstaUbicadoBusca"),
  
  ##prueba LOTE PRODUCTO BUSCA
  path('LoteProductoBusca/<str:busca>/',Lote_ProductoBusca.as_view(), name="LoteProductoBusca"),
  path('LoteProductoBusca2/<str:buscaP>/<str:buscaL>/',Lote_ProductoBusca2.as_view(), name="LoteProductoBusca2"),
  path('LoteMedicamentoBusca/<str:busca>/', Lote_ProductoMedicamentoBusca.as_view(), name="LoteMedicamentoBusca"),
  path('LoteInsumoBusca/<str:busca>/', Lote_ProductoInsumosBusca.as_view(), name="LoteInsumoBusca"),
  path('LoteReactivoBusca/<str:busca>/', Lote_ProductoReactivosBusca.as_view(), name="LoteReactivoBusca"),
  path('Lote_ProductoMedicamentoBuscaLote/<str:busca>/', Lote_ProductoMedicamentoBuscaLote.as_view(), name="Lote_ProductoMedicamentoBuscaLote"),
  path('Lote_ProductoInsumosBuscaLote/<str:busca>/', Lote_ProductoInsumosBuscaLote.as_view(), name="Lote_ProductoInsumosBuscaLote"),
  path('Lote_ProductoReactivosBuscaLote/<str:busca>/', Lote_ProductoReactivosBuscaLote.as_view(), name="Lote_ProductoReactivosBuscaLote"),

  ## EDITA LOTE-PRODUCTO CANTIDAD
  path('LoteCantidadEdita/<str:pk>/',LoteCantidadEdita.as_view(), name="LoteCantidadEdita"),

  #ESTA UBICADO (Almacen)
  path('EstaUbicadoLista/',EstaUbicadoLista.as_view(), name="EstaUbicadoLista"),
  path('EstaUbicadoBusca/<int:busca>/',EstaUbicadoBusca.as_view(), name="EstaUbicadoBusca"),
  path('EstaUbicadoBuscaMedicamentos/<int:busca>/',EstaUbicadoBuscaMedicamentos.as_view(), name="EstaUbicadoBuscaMedicamentos"),
  path('EstaUbicadoBuscaInsumos/<int:busca>/',EstaUbicadoBuscaInsumos.as_view(), name="EstaUbicadoBuscaInsumos"),
  path('EstaUbicadoBuscaReactivos/<int:busca>/',EstaUbicadoBuscaReactivos.as_view(), name="EstaUbicadoBuscaReactivos"),
  
  ##ESTA UBICADO (Almacen) BUSCA CODIGO
  path('UbicadoAlmacenBuscaCodI/<str:busca>/',UbicadoAlmacenBuscaCodI.as_view(), name="UbicadoAlmacenBuscaCodI"),
  path('UbicadoAlmacenBuscaCodM/<str:busca>/', UbicadoAlmacenBuscaCodM.as_view(), name="UbicadoAlmacenBuscaCodM"),
  path('UbicadoAlmacenBuscaCodR/<str:busca>/', UbicadoAlmacenBuscaCodR.as_view(), name="UbicadoAlmacenBuscaCodR"),


  ##ESTA UBICADO (Farmacia) BUSCA CODIGO
  path('UbicadoFarmaciaBusca2/<str:buscaP>/<str:buscaL>/', UbicadoFarmaciaBusca2.as_view(), name="UbicadoFarmaciaBusca2"),

  path('UbicadoFarmaciaBuscaCodI/<str:busca>/', UbicadoFarmaciaBuscaCodI.as_view(), name="UbicadoFarmaciaBuscaCodI"),
  path('UbicadoFarmaciaBuscaCodR/<str:busca>/', UbicadoFarmaciaBuscaCodR.as_view(), name="UbicadoFarmaciaBuscaCodR"),
  path('UbicadoFarmaciaBuscaCodM/<str:busca>/', UbicadoFarmaciaBuscaCodM.as_view(), name="UbicadoFarmaciaBuscaCodM"),
  path('UbicadoFarmaciaBuscaCodP/<str:busca>/', UbicadoFarmaciaBuscaCodP.as_view(), name="UbicadoFarmaciaBuscaCodP"),
  path('UbicadoFarmaciaBuscaCodMsuma/<str:busca>/', UbicadoFarmaciaBuscaCodMsuma.as_view(), name="UbicadoFarmaciaBuscaCodMsuma"),
  
  ##ESTA UBICADO (Almacen) BUSCA CODIGO
  path('UbicadoAlmacenBusca2/<str:buscaP>/<str:buscaL>/', UbicadoAlmacenBusca2.as_view(), name="UbicadoAlmacenBusca2"),
  path('UbicadoAlmacenBuscaCodP/<str:busca>/', UbicadoAlmacenBuscaCodP.as_view(), name="UbicadoAlmacenBuscaCodP"),
  
  ##ESTA UBICADO REGISTRA (UNO NUEVO)
  path('EstaUbicadoRegistra/', EstaUbicadoRegistra.as_view(), name="EstaUbicadoRegistra"),


  ## EDITA ESTA UBICADO (Farmacia) CANTIDAD (busca nro lote)
  path('EstaFarmaciaEdita/<str:pk>/',EstaFarmaciaEdita.as_view(), name="EstaFarmaciaEdita"),
  path('EstaUbicadoListaBuscaCod/<str:busca>/', EstaUbicadoListaBuscaCod.as_view(), name="EstaUbicadoListaBuscaCod"),
  
  ## EDITA ESTA UBICADO (Almacen) CANTIDAD (busca nro lote)
  path('EstaAlmacenEdita/<str:pk>/',EstaAlmacenEdita.as_view(), name="EstaAlmacenEdita"),
  

  ## LISTA ALMACEN
  path('ListaAlmacen/', ListaAlmacen.as_view(), name="ListaAlmacen"),

  
  ##ESTA UBICADO LOCALIZACION(X) CODIGO(X) SUMA CANTIDAD
  path('LocalizacionBuscaCodsuma/<int:buscaL>/<str:buscaCod>/', LocalizacionBuscaCodsuma.as_view(), name="LocalizacionBuscaCodsuma"),

  ## LOTE
  path('LoteGuardar/', LoteGuardar.as_view(), name="LoteGuardar"),
  path('EliminarLote/<str:pk>/',EliminarLote.as_view(), name="EliminarLote"),

  ## DEPOSITO 
  
  path('DepositoGuardar/', DepositoGuardar.as_view(), name="DepositoGuardar"),
  path('DepositoTodos/', DepositoTodos.as_view(), name="DepositoTodos"),
  path('DepositoMedicamentos/', DepositoMedicamentos.as_view(), name="DepositoMedicamentos"),
  path('DepositoInsumos/', DepositoInsumos.as_view(), name="DepositoInsumos"),
  path('DepositoReactivos/', DepositoReactivos.as_view(), name="DepositoReactivos"),
  path('DepositoTodos/', DepositoTodos.as_view(), name="DepositoTodos"),

  #####-------------------------#############


  path('UbicadoFarmaciaBuscaCodMsuma2/<str:busca>/', UbicadoFarmaciaBuscaCodMsuma2.as_view(), name="UbicadoFarmaciaBuscaCodMsuma2"),

  path('UbicadoFarmaciaBuscaCodMsumaHola/<str:busca>/', UbicadoFarmaciaBuscaCodMsumaHola.as_view(), name="UbicadoFarmaciaBuscaCodMsumaHola"),


]

    