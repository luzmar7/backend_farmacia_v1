from rest_framework import serializers

from ....almacena.models import (
  Producto_farmaceutico,
  T_forma_farmaceutica,
  T_presentacion,
  Medicamento,
  Reactivo,
  Insumo_hospitalario,
  Lote_producto,
  Almacen,
  Esta_Ubicado,
  Lote,
  T_unidad,
  Deposito
)

class ProductoSerializer(serializers.ModelSerializer):
  class Meta:
    model = Producto_farmaceutico
    fields = '__all__'

class ProductoCodigoSerializer(serializers.ModelSerializer):
  class Meta:
    model = Producto_farmaceutico
    fields = (
      'codigo',
    )


class ProductoHabilitadoSerializer(serializers.ModelSerializer):
  class Meta:
    model = Producto_farmaceutico
    fields = (
      'habilitado',
    )


#------------- SERIALIZER PARA ACTUALIZAR  LA CANTIDAD PRODUCTO-FARMACEUTICO ---------------
class ProductoCantidadSerializer(serializers.ModelSerializer):
  class Meta:
    model = Producto_farmaceutico
    fields = (
      'stockGeneral',
    )


class IngresaSerializer(serializers.ModelSerializer):
  class Meta:
    model = Producto_farmaceutico
    fields = '__all__'

class FormaFarmaceuticaSerializer(serializers.ModelSerializer):
  class Meta:
    model = T_forma_farmaceutica
    fields = '__all__'

class PresentacionSerializer(serializers.ModelSerializer):
  class Meta:
    model = T_presentacion
    fields = '__all__'

class UnidadSerializer(serializers.ModelSerializer):
  class Meta:
    model = T_unidad
    fields = '__all__'

class MedicamentoSerializer(serializers.ModelSerializer):
  producto_farmaceutico = ProductoSerializer(read_only=True)
  t_forma_farmaceutica= FormaFarmaceuticaSerializer(read_only=True)
  class Meta:
    model = Medicamento
    fields = '__all__'

class MedicamentoSerializer2(serializers.ModelSerializer):
  class Meta:
    model = Medicamento
    fields = '__all__'

class MedicamentoCodigoSerializer(serializers.ModelSerializer):
  class Meta:
    model = Medicamento
    fields = (
      'producto_farmaceutico',
    )

class MedicamentoCodigoSerializer(serializers.ModelSerializer):
  class Meta:
    model = Medicamento
    fields = (
      'producto_farmaceutico',
    )

class ReactivoSerializer(serializers.ModelSerializer):
  producto_farmaceutico = ProductoSerializer(read_only=True)
  class Meta:
    model = Reactivo
    fields = '__all__'

class ReactivoSerializer2(serializers.ModelSerializer):
  class Meta:
    model = Reactivo
    fields = '__all__'

class InsumoHospitalarioSerializer(serializers.ModelSerializer):
  producto_farmaceutico = ProductoSerializer(read_only=True)
  t_presentacion= PresentacionSerializer(read_only=True)
  t_unidad = UnidadSerializer(read_only=True)
  class Meta:
    model = Insumo_hospitalario
    fields = '__all__'

class InsumoHospitalarioSerializer2(serializers.ModelSerializer):
  class Meta:
    model = Insumo_hospitalario
    fields = '__all__'

#------ SERIALIZER PARA ESTA_UBICADO ---------
class LoteSerializer(serializers.ModelSerializer):
  class Meta:
    model = Lote
    fields = '__all__'

class LoteSerializer2(serializers.ModelSerializer):
  class Meta:
    model = Lote
    fields = (
      'id_lote',
    )

class AlmacenSerializer(serializers.ModelSerializer):
  class Meta:
    model = Almacen
    fields = '__all__'


class Esta_UbicadoSerializer(serializers.ModelSerializer):
  producto_farmaceutico = ProductoSerializer(read_only=True)
  lote = LoteSerializer(read_only=True)
  almacen = AlmacenSerializer(read_only=True)
  class Meta:
    model = Esta_Ubicado
    fields = '__all__'

class Esta_UbicadoSerializer2(serializers.ModelSerializer):
  class Meta:
    model = Esta_Ubicado
    fields = '__all__'

#------------- SERIALIZER PARA ACTUALIZAR  LA CANTIDAD ESTA UBICADO ---------------
class Esta_UbicadoCantidadSerializer(serializers.ModelSerializer):
  class Meta:
    model = Esta_Ubicado
    fields = (
      'stockAlmacen',
    )

#------cantidad sumada esta ubicad

class Esta_UbicadoSumSerializer(serializers.ModelSerializer):
  sum = serializers.IntegerField()
  class Meta:
    model = Esta_Ubicado
    fields = ('producto_farmaceutico_id','sum')

#----------------- SERIALIZER LOTE PRODUCTO -------------------
class Lote_ProductoSerializer(serializers.ModelSerializer):
  producto_farmaceutico = ProductoSerializer(read_only=True)
  lote = LoteSerializer(read_only=True)
  class Meta:
    model = Lote_producto
    fields = '__all__'

#----------------- SERIALIZER LOTE PRODUCTO GUARDAR-------------------
class Lote_ProductoSerializer2(serializers.ModelSerializer):
  class Meta:
    model = Lote_producto
    fields = '__all__'


#------------- SERIALIZER PARA ACTUALIZAR LA CANTIDAD LOTE-PRODUCTO ---------------
class Lote_ProductoCantidadSerializer(serializers.ModelSerializer):
  class Meta:
    model = Lote_producto
    fields = (   
      'cantidad', 
    )

##################$$$$$ SERIALIZER DEPOSITO $$$###########################
class DepositoSerializer(serializers.ModelSerializer):
  class Meta:
    model = Deposito
    fields = '__all__'


################### asdlkjsahdsajhd "$################################"

class Esta_UbicadoSumSerializer2(serializers.ModelSerializer):
  sum = serializers.IntegerField()
  class Meta:
    model = Esta_Ubicado
    fields = ('producto_farmaceutico_id','sum')