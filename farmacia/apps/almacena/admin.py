from django.contrib import admin

from apps.almacena.models import (
  Almacen,
  Producto_farmaceutico,
  T_presentacion,
  T_forma_farmaceutica,
  T_unidad,
  Insumo_hospitalario,
  Medicamento,
  Reactivo,
  Lote,
  Lote_producto,
  Esta_Ubicado,
  Deposito
)

@admin.register(Producto_farmaceutico)
class Producto_farmaceuticoAdmin(admin.ModelAdmin):
  list_display = ["codigo", "nombre", "stockGeneral", "costo", "saldo_minimo", "cantidad_prescripcion", "habilitado"]

@admin.register(Almacen)
class AlmacenAdmin(admin.ModelAdmin):
  list_display = ["id", "descripcion"]

@admin.register(T_presentacion)
class T_presentacionAdmin(admin.ModelAdmin):
  list_display = ["id", "descripcion"]

@admin.register(T_forma_farmaceutica)
class T_forma_farmaceuticaAdmin(admin.ModelAdmin):
  list_display = ["id", "descripcion"]

@admin.register(T_unidad)
class T_forma_farmaceuticaAdmin(admin.ModelAdmin):
  list_display = ["id", "descripcion"]

@admin.register(Insumo_hospitalario)
class Insumo_hospitalarioAdmin(admin.ModelAdmin):
  list_display = ["producto_farmaceutico", "t_presentacion", "t_unidad"]

@admin.register(Medicamento)
class MedicamentoAdmin(admin.ModelAdmin):
  list_display = ["producto_farmaceutico", "t_forma_farmaceutica", "concentracion", "clasificacion"]

@admin.register(Reactivo)
class ReactivoAdmin(admin.ModelAdmin):
  list_display = ["producto_farmaceutico", "clasificacion"]

@admin.register(Lote)
class LoteAdmin(admin.ModelAdmin):
  list_display = ["id_lote", "fecha_expiracion"]

@admin.register(Lote_producto)
class Lote_productoAdmin(admin.ModelAdmin):
  list_display = ["producto_farmaceutico", "lote", "cantidad"]

@admin.register(Esta_Ubicado)
class Esta_UbicadoAdmin(admin.ModelAdmin):
  list_display = ["producto_farmaceutico", "lote", "almacen", "stockAlmacen"]

@admin.register(Deposito)
class DepositoAdmin(admin.ModelAdmin):
  list_display = ["id" ,"codigo", "nombre", "lote", "fecha_expiracion", "cantidad", "fecha_deposito", "hora_deposito", "tipo_producto", "observacion"]


# admin.site.register(T_presentacioni)
# admin.site.register(Insumo_hospitalarioi)
# admin.site.register(T_forma_farmaceutica)
# admin.site.register(Medicamentob)
# admin.site.register(Reactivoc)
# admin.site.register(Lote)
# admin.site.register(Lote_producto)
# admin.site.register(Esta_Ubicado)


# @admin.register(Producto_farmaceuticoB)
# class Producto_farmaceuticoBAdmin(admin.ModelAdmin):
#     list_display = ["codigo", "nombre", "costo"]






# Register your models here.
