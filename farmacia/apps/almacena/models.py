from django.db import models

# Create your models here.
from django.db import models
import datetime

class Almacen(models.Model):
  descripcion = models.CharField(
    max_length=30,
    verbose_name= "Localización"
  )
  class Meta:
    verbose_name =("Localización")
    verbose_name_plural =("Localizaciones")
  def __str__(self):
    return self.descripcion

class Producto_farmaceutico(models.Model):
  codigo = models.CharField(
    primary_key=True, 
    max_length=50, 
    null=False,
    verbose_name="Codigo del Producto"
  )
  nombre = models.CharField(
    max_length=100,
    verbose_name="Nombre del Producto"
  )
  stockGeneral = models.PositiveIntegerField(
    verbose_name="Stock General"
  )
  costo = models.PositiveIntegerField(
    verbose_name="Costo",
    null=True,
    blank=True,
  )
  saldo_minimo = models.PositiveIntegerField(
    verbose_name="Saldo Minimo",
    null=True,
    blank=True,
  )
  cantidad_prescripcion = models.PositiveIntegerField(
    verbose_name="Cantidad Maxima de Dispensación",
    null=True,
    blank=True,
  )
  habilitado=models.BooleanField(
    default=True,
    verbose_name="habilitado"
  )
  class Meta:
    verbose_name = ("Producto Farmaceutico")
    verbose_name_plural= ("Productos Farmaceuticos")
  def __str__(self):
    return self.codigo +" - "+ self.nombre



class T_unidad(models.Model):
  descripcion = models.CharField(
    max_length=30,
    verbose_name="Descripción"
  )
  class Meta:
    verbose_name =("Unidad de Manejo")
    verbose_name_plural =("Unidad de Manejo")
  def __str__(self):
    return self.descripcion

class T_forma_farmaceutica(models.Model):       
  descripcion = models.CharField(
    max_length=30,
    verbose_name="Descripcion"
  )
  class Meta:
    verbose_name =("Forma Farmaceutica")
    verbose_name_plural =("Formas Farmaceuticas")
  def __str__(self):
    return self.descripcion

class T_presentacion(models.Model):
  descripcion = models.CharField(
    max_length=30,
    verbose_name="Descripción"
  )
  class Meta:
    verbose_name =("Presentación")
    verbose_name_plural =("Presentaciones")
  def __str__(self):
    return self.descripcion


#poner 1 luego 2 para actualizar modelos
class Insumo_hospitalario(models.Model):
  producto_farmaceutico = models.OneToOneField(
    Producto_farmaceutico,
    verbose_name ="Insumo Hospitalario",
    on_delete = models.CASCADE, 
    primary_key =True, 
    unique = True 
  )
  t_presentacion = models.ForeignKey(
    T_presentacion ,
    verbose_name="Presentación",
    on_delete=models.CASCADE,
  )
  t_unidad = models.ForeignKey(
    T_unidad,
    verbose_name="Unidad de Manejo",
    on_delete=models.CASCADE,
  )
  class Meta:
    verbose_name =("Insumo Hospitalario")
    verbose_name_plural =("Insumos Hospitalarios")
  def __str__(self):
    return self.producto_farmaceutico_id

class Medicamento(models.Model):
  producto_farmaceutico = models.OneToOneField(
    Producto_farmaceutico,
    verbose_name ="Producto Farmaceutico",
    on_delete = models.CASCADE, 
    primary_key =True, 
    unique = True 
  )
  t_forma_farmaceutica = models.ForeignKey(
    T_forma_farmaceutica,
    verbose_name="Forma Farmaceutica", 
    on_delete=models.CASCADE
  )
  concentracion = models.CharField(max_length=30)
  clasificacion = models.CharField(max_length=30, null=True)
  class Meta:
    verbose_name =("Medicamento")
    verbose_name_plural =("Medicamentos")
  def __str__(self):
    return self.producto_farmaceutico_id

class Reactivo(models.Model):
  producto_farmaceutico = models.OneToOneField(
    Producto_farmaceutico,
    verbose_name="Producto Famaceutico",
    on_delete = models.CASCADE, 
    primary_key =True, 
    unique = True
  )
  clasificacion = models.CharField(max_length=30, null=True, blank=True)

  class Meta:
    verbose_name =("Reactivo")
    verbose_name_plural =("Reactivos")
  def __str__(self):
    return self.producto_farmaceutico_id

class Lote(models.Model):
  id_lote = models.CharField(
    primary_key=True,
    verbose_name="Nro de Lote", 
    max_length=20, 
    null=False
  )
  fecha_expiracion = models.DateField(
    verbose_name="Fecha de expiración",
    default=datetime.date.today
  )
  class Meta:
    verbose_name =("Lote")
    verbose_name_plural =("Lotes")
  def __str__(self):
    return self.id_lote

class Lote_producto(models.Model): 
  producto_farmaceutico = models.ForeignKey(
    Producto_farmaceutico,
    verbose_name="Producto Farmaceutico",
    on_delete=models.CASCADE
  )
  lote = models.ForeignKey(
    Lote,
    verbose_name="Nro de Lote",
    on_delete=models.CASCADE
  )
  cantidad = models.PositiveIntegerField(
    verbose_name= "Cantidad Lote"
  )
  class Meta:
    verbose_name =("Lote Producto")
    verbose_name_plural =("Lotes Productos")
  def __unicode__(self):
    return self.lote 

class Esta_Ubicado(models.Model):    
  producto_farmaceutico = models.ForeignKey(
    Producto_farmaceutico,
    verbose_name="Producto Farmaceutico",
    on_delete = models.CASCADE
  )
  lote = models.ForeignKey(
    Lote,
    verbose_name="Lote",
    on_delete = models.CASCADE
  )
  almacen = models.ForeignKey(
    Almacen,
    verbose_name="Localización",
    on_delete = models.CASCADE
  )
  stockAlmacen = models.PositiveIntegerField(
    verbose_name="Stock de Almacen"
  )
  class Meta:
    verbose_name =("Esta Ubicado")
    verbose_name_plural =("Esta Ubicado")
  def __unicode__(self):
    return self.lote 

class Deposito(models.Model):
  codigo = models.CharField(
    max_length=100,
    verbose_name= "Codigo"
  )
  nombre = models.CharField(
    max_length=100,
    verbose_name= "Nombre"
  )
  lote = models.CharField(
    max_length=100,
    verbose_name= "Lote"
  )
  fecha_expiracion = models.DateField(
    verbose_name="Fecha de expiración",
  )
  cantidad = models.PositiveIntegerField(
    verbose_name="Cantidad",
    null=True, 
    blank=True
  )
  fecha_deposito = models.DateField(
    verbose_name="Fecha deposito",
    default=datetime.date.today
  )
  hora_deposito = models.TimeField(
    verbose_name="Hora deposito",
    null=True, 
    blank=True
  )
  tipo_producto = models.CharField(
    max_length=30,
    verbose_name= "Tipo producto",
    null=True, 
    blank=True
  )
  observacion = models.CharField(
    max_length=100,
    verbose_name= "Observacion"
  )

  class Meta:
    verbose_name =("Deposito")
    verbose_name_plural =("Depositos")
  def __str__(self):
    return self.lote




# Create your models here.
# python manage.py migrate --fake (arreglar error)

